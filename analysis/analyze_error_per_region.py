from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np
import matplotlib.pyplot as plt
from pkg_resources import resource_filename
from sklearn import linear_model

from base.floor_plan_plot import FloorPlanPlotRec
from sklearn.metrics import mean_squared_error, r2_score
import seaborn as sns

from analysis.visualize_learning_progress import convert_from_grid


def main():

    pipe_c: Pipeline = Storable.load("../evaluation/lohan/evaluation/deep/output/BBOX_NEW")

    convert_from_grid(pipe_c)

    y_pred = np.concatenate(pipe_c.summary.y_pred, axis=0)[:, :2]
    y_pred_box_shape = np.concatenate(pipe_c.summary.y_pred, axis=0)[:, 2:]
    y_true = np.concatenate(pipe_c.summary.y_true_labels, axis=0)
    #y_pred_reg = np.concatenate(pipe_r.summary.y_pred, axis=0)

    error = np.linalg.norm(y_pred - y_true, axis=1)

    # x/y error
    x_y_error = np.abs(y_pred[:, 0] - y_true[:, 0]) / np.abs(y_pred[:, 1] - y_true[:, 1])
    x_y_error[x_y_error < 1] = -1
    x_y_error[x_y_error >= 1] = 1

    w_h_error = y_pred_box_shape[:, 0]/y_pred_box_shape[:, 1]
    w_h_error[w_h_error < 1] = -1
    w_h_error[w_h_error >= 1] = 1

    r = len(np.where(x_y_error == w_h_error)[0]) / len(x_y_error)

    circle_size = np.prod(np.concatenate(pipe_c.summary.y_pred, axis=0)[:, 2:], axis=1)

    #error = x_y_error
    #circle_size = w_h_error

    sub_mask = np.where(error < 200)[0]
    error = error[sub_mask]

    #circle_size = get_size_for_quantile(pipe_c)

    circle_size = circle_size[sub_mask]

    plt.scatter(error, circle_size)

    # Create linear regression object
    regr = linear_model.LinearRegression()

    # Train the model using the training sets
    regr.fit(error.reshape(-1, 1), circle_size.reshape(-1, 1))
    # Make predictions using the testing set
    y_val = regr.predict(error.reshape(-1, 1))
    plt.plot(error, y_val)

    # The coefficients
    print('Coefficients: \n', regr.coef_)
    # The mean squared error
    print('Mean squared error: %.2f'
          % mean_squared_error(circle_size, y_val))
    # The coefficient of determination: 1 is perfect prediction
    print('Coefficient of determination: %.2f'
          % r2_score(circle_size, y_val))

    plt.show()

    sns.kdeplot(error, circle_size, shade = True, cmap = "PuBu")
    plt.show()

    img = "../evaluation/gia/gia_floor_4.jpg"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img)

    fp.draw_error_scatter(y_true, 12, color="b") #np.full(circle_size.shape, 5.7)
    fp.draw_error_scatter(y_true, error,
                          color="r")  # np.full(circle_size.shape, 5.7)
    #fp.draw_error_scatter(y_true, error, color="r")


    fp.show_plot()


def get_size_for_quantile(pipe: Pipeline):
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
    max_x = np.max(y_pred[:, [0, 2]], axis=1)
    min_x = np.min(y_pred[:, [0, 2]], axis=1)
    max_y = np.max(y_pred[:, [1, 3]], axis=1)
    min_y = np.min(y_pred[:, [1, 3]], axis=1)

    diff_x = max_x - min_x
    diff_y = max_y - min_y

    size = np.multiply((max_y - min_y), (max_x - min_x))

    return size


if __name__ == "__main__":
    main()