from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np

from analysis.correlation_knn import evaluate_correlation
from analysis.visualize_learning_progress import \
    convert_from_2dim_overlapping_grid


def main():

    path_lohan = "/home/laskama/Dropbox/evaluation/lohan/BBOX_{}l_{}o{}_{}"
    path_uji = "/home/laskama/Dropbox/evaluation/UJIndoorLoc/b0/f0/final/output/BBOX_{}l_{}o{}_{}"

    for o in [3, 4, 5]:

        for num_layers in range(2):

            pred_pos = None
            true_pos = None
            size = None

            for sample in range(5):
                try:
                    f_path = path_lohan.format(num_layers+1, o, "_aug", sample + 1)
                    model: Pipeline = Storable.load(f_path)
                except:
                    continue

                convert_from_2dim_overlapping_grid(model, grid_size=40, quantile=False,
                                                   store=True,
                                                   average_samples=True)

                pred = np.concatenate(model.summary.y_pred, axis=0)
                true = np.concatenate(model.summary.y_true_labels, axis=0)

                p_pos = pred[:, :2]
                p_size = np.prod(pred[:, 2:], axis=1)

                if pred_pos is None:
                    pred_pos = p_pos
                    true_pos = true
                else:
                    pred_pos = np.concatenate((pred_pos, p_pos), axis=0)
                    true_pos = np.concatenate((true_pos, true), axis=0)

                if size is None:
                    size = p_size
                else:
                    size = np.concatenate((size, p_size), axis=0)

            summary = {"y": [], "beta": [], "p_val": [], "r_2": [], "f": [], "type": []}

            error = np.linalg.norm(pred_pos - true_pos, axis=1)

            evaluate_correlation(size, error, "error -> size", summary)

            print("num_layers: {}".format(num_layers +1))
            print("o: {}".format(o))
            print(summary)
            print("\n")


if __name__ == "__main__":
    main()
