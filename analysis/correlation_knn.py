from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np
import pandas as pd
#from mgwr.gwr import GWR, MGWR, GWRResults
#from mgwr.sel_bw import Sel_BW
from shapely.geometry import Point
from pkg_resources import resource_filename

from analysis.visualize_learning_progress import convert_from_2dim_grid, convert_from_2dim_2layer_grid, convert_from_2dim_3layer_grid, convert_from_2dim_overlapping_grid
from statsmodels.regression.linear_model import OLS
from statsmodels.tools import add_constant
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from sklearn import linear_model

from base.floor_plan_plot import FloorPlanPlotRec


def main(dir_p="../evaluation/lohan/evaluation/new_knn/output/", f_idx=0):
    f = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/UJIndoorLoc/b0/f0/final/output/BBOX_1l_20_aug_1"
    f = "/media/laskama/Daten/BBOX/UJIndoorLoc/b0/f1/test/output/BBOX_1l_20_aug_1"
    f = "/media/laskama/Daten/BBOX/lohan/test/output/BBOX_1l_10_aug_1"
    f = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/uji/cnn/output/CNN_BBOX_test_1"
    bbox: Pipeline = Storable.load(f)#dir_p + "BBOX_1l_3o_aug_1")

    convert_from_2dim_overlapping_grid(bbox, grid_size=bbox.data_provider.grid_size, quantile=False,
                                  store=True,
                                  average_samples=True)
    #convert_from_2dim_grid(bbox, grid_size=40, quantile=False, store=True,
    #                       average_samples=True)

    f= "/home/laskama/Dropbox/evaluation/UJIndoorLoc/b0/f1/final/output/REG_1"
    f = "/home/laskama/Dropbox/evaluation/lohan/REG_1"
    #reg: Pipeline = Storable.load(f)#dir_p + "REG_2l_1") # Storable.load("../evaluation/simulation/evaluation/big_2/output/" + "REG") #Storable.load("../evaluation/simulation/evaluation/big_2/output/REG")

    #knn: Pipeline = Storable.load(dir_p + "KNN")

    #bbox_m: Pipeline = Storable.load(
    #    "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/grid_test/output/BBOX_TEST_3_v5")
    #convert_from_2dim_grid(bbox_m, grid_size=40, quantile=False, store=True,
    #                       average_samples=False)

    # ground truth positions (of fold f_idx)
    y_true = bbox.summary.y_true_labels[f_idx]

    # predicted positions (of fold f_idx)
    y_pred_box = bbox.summary.y_pred[f_idx]
    #y_pred_reg = reg.summary.y_pred[f_idx]
    #y_pred_knn = knn.summary.y_pred[f_idx]

    #y_pred_bbox_m = bbox_m.summary.y_pred[f_idx]

    #
    # Compute non MCD metrics
    #

    # center statistics
    box_error = np.linalg.norm(y_true - y_pred_box[:, :2], axis=1)
    box_mse = np.mean(box_error)

    print("box_mse: {}".format(box_mse))

    #reg_error = np.linalg.norm(y_true - y_pred_reg, axis=1)
    #reg_mse = np.mean(reg_error)

    #print("reg_mse: {}".format(reg_mse))

    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    (train, test) = bbox.data_provider.splits_indices[f_idx]
    fp = FloorPlanPlotRec((200, 80), 2, floorplan_bg_img=img)
    fp.draw_points(bbox.data_provider.labels[train, 0],
                   bbox.data_provider.labels[train, 1], color='g', alpha=0.5)
    fp.draw_points(bbox.data_provider.labels[test, 0],
                 bbox.data_provider.labels[test, 1], color='r',
                 alpha=0.5)
    fp.show_plot()

    # for each prediction => find bbox of TD with close
    SIM_THRESHOLD = 5
    sim_size = np.zeros(len(test))
    for t_idx, t in enumerate(test):
        t_data = bbox.data_provider.data_tensor[t, :]
        #diff_to_t = np.linalg.norm(bbox.data_provider.data_tensor - t_data, axis=1)
        diff_to_t = np.linalg.norm(bbox.data_provider.labels - bbox.data_provider.labels[t, :], axis=1)
        sim_mask = np.where(diff_to_t < SIM_THRESHOLD)[0]
        sim_labels = bbox.data_provider.labels[sim_mask, :]
        max_l = np.max(sim_labels, axis=0)
        min_l = np.min(sim_labels, axis=0)
        #sim_size[t_idx] = (max_l[0] - min_l[0]) * (max_l[1] - min_l[1])
        sim_size[t_idx] = len(sim_labels)


    # reference error of kNN model (also component-wise)
    #knn_error = np.linalg.norm(y_true - y_pred_knn, axis=1)
    #knn_error_x = np.abs(y_true - y_pred_knn)[:, 0]
    #knn_error_y = np.abs(y_true - y_pred_knn)[:, 1]
    #knn_mse = np.mean(knn_error)

    knn_error = box_error
    knn_error_x = np.abs(y_true - y_pred_box[:, :2])[:, 0]
    knn_error_y = np.abs(y_true - y_pred_box[:, :2])[:, 1]

    # Bbox size (also component-wise)
    box_size = np.prod(y_pred_box[:, 2:], axis=1)
    size_x = y_pred_box[:, 2]
    size_y = y_pred_box[:, 3]

    #
    # Compute MCD related metrics
    #

    # compute std of error and size per observation
    #m_error = np.linalg.norm(y_pred_bbox_m[:, :2, :] - np.stack([y_true] * 100, axis=2), axis=1)
    #error_std = np.std(m_error, axis=1)

    #center_std = np.linalg.norm(np.std(y_pred_bbox_m[:, :2, :], axis=2), axis=1)

    #size_std = np.std(np.prod(y_pred_bbox_m[:, 2:, :], axis=1), axis=1)
    #avg_size = np.mean(np.prod(y_pred_bbox_m[:, 2:, :], axis=1), axis=1)

    summary = {"y": [], "beta": [], "p_val": [], "r_2": [], "f": [], "type": []}

    #
    # Correlation between Error of simple kNN model and box size of BBox model
    #

    evaluate_correlation(box_size, knn_error, "error -> size", summary)

    # component wise correlation

    evaluate_correlation(size_x, knn_error_x, "error_x -> size_x", summary)

    evaluate_correlation(size_y, knn_error_y, "error_y -> size_y", summary)

    evaluate_correlation(size_x, knn_error_y, "error_y -> size_x", summary)
    evaluate_correlation(size_y, knn_error_x, "error_x -> size_y", summary)

    evaluate_correlation(sim_size, y_true[:, 0], "num_around -> size", summary)

    evaluate_correlation(box_size, y_true[:, 0], "x_pos -> size", summary)
    evaluate_correlation(box_size, y_true[:, 1], "y_pos -> size", summary)
    evaluate_correlation(box_size, np.mean(y_true, axis=1), "pos -> size", summary)

    evaluate_correlation(box_size, np.stack((y_true[:, 0], y_true[:, 1], knn_error_x, knn_error_y), axis=1), "all -> size", summary)

    #plt.figure()
    #plt.scatter(knn_error, box_size)
    #plt.show()

    coords = [(l[0], l[1]) for l in y_true]
    geometry = [Point(xy) for xy in zip(coords)]
    df = pd.DataFrame(list(zip(y_pred_box[:,0], y_pred_box[:, 1], knn_error_x, knn_error_y, box_size)), columns=['x', 'y', 'error_x', 'error_y', 'box_size'])
    fig = plt.figure()
    ax = fig.add_subplot(111)
    s = ax.scatter(df.x, df.y, c = df.box_size)

    #df.plot.scatter(x='x', y='y', c='box_size', colormap='viridis')
    #ax = plt.gca()
    ax.set_aspect('equal')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb = plt.colorbar(s, cax=cax)
    cb.set_label('Box size [m²]')
    #plt.ylim(0, 80)

    df_2 = pd.DataFrame(list(
        zip(y_true[:, 0], y_true[:, 1], knn_error_x, knn_error_y,
            box_size)), columns=['x', 'y', 'error_x', 'error_y', 'box_size'])
    df_2.plot.scatter(x='x', y='y', c='box_size', colormap='viridis')
    #y = box_size.reshape(-1, 1)
    #X = np.stack((knn_error_x, knn_error_y), axis=1)
    #gwr(coords, y, X)
    plt.axis('equal')
    plt.show()

    regr = linear_model.LinearRegression()

    # Train the model using the training sets
    regr.fit(knn_error.reshape(-1, 1), box_size.reshape(-1, 1))
    # Make predictions using the testing set
    y_val = regr.predict(knn_error.reshape(-1, 1))

    plt.scatter(knn_error, box_size)
    plt.plot(knn_error, y_val)

    plt.show()


    #
    # Correlation between std of Monte-Carlo-Dropout Model (center, size, error)
    # with kNN model error
    #

    #evaluate_correlation(error_std, knn_error, "error -> std(error) [MCD]", summary)

    #evaluate_correlation(size_std, knn_error, "error -> std(size) [MCD]", summary)

    #evaluate_correlation(avg_size, knn_error, "error -> mean(size) [MCD]", summary)

    #evaluate_correlation(center_std, knn_error, "error -> std(center) [MCD]", summary)

    summary["f"] += [f_idx] * 10

    return summary


def evaluate_correlation(y, x, x_name, y_name, summary):
    if len(x.shape) == 1:
        x = x.reshape(-1, 1)
    regr = OLS(y,
               add_constant(x)).fit()
    summary["$\\beta_0$"].append(regr.params[0])
    summary["$\\beta_1$"].append(regr.params[1])
    summary["p-value"].append(regr.pvalues[1])
    summary["$R^2$"].append(regr.rsquared)
    summary["Y"].append(x_name)
    summary["X"].append(y_name)
    summary["Y_mean"].append(np.mean(y))
    summary["RSE"].append(np.sqrt(regr.scale))
    #summary["type"].append(type)


def generate_correlation_df():
    y_intercepts = []
    betas = []
    p_vals = []
    r_squared = []
    corr_type = []
    fold_idx = []

    for f_idx in range(1):
        summary = main("/home/laskama/Dropbox/evaluation/lohan/", f_idx=f_idx)

        y_intercepts += summary["y"]
        betas += summary["beta"]
        p_vals += summary["p_val"]
        r_squared += summary["r_2"]
        fold_idx += summary["f"]
        corr_type += summary["type"]

    df_dict = {"corr_type": corr_type, "y_intercept": y_intercepts,
               "beta": betas, "p_val": p_vals,
               "r2": r_squared, "fold": fold_idx}

    df = pd.DataFrame(data=df_dict)

    return df


# def gwr(g_coords, g_y, g_X):
#     gwr_selector = Sel_BW(g_coords, g_y, g_X)
#     gwr_bw = gwr_selector.search(bw_min=2)
#     print(gwr_bw)
#     gwr_results: GWRResults = GWR(g_coords, g_y, g_X, gwr_bw).fit()
#     print(gwr_results.localR2[0:10])
#     print(gwr_results.summary())
#
#     filter_t = gwr_results.filter_tvals()



if __name__ == "__main__":

    df = generate_correlation_df()
    print(df)#df[df["corr_type"] == "error -> size"])