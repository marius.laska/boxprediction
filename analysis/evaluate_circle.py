from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np
import math

from base.floor_plan_plot import FloorPlanPlotRec
from analysis.plot_analysis import find_radius_for_acc

PI = 3.14159265359


def main():

    pipe: Pipeline = Storable.load("evaluation/gia/cmp/output/CIRCLE")
    b_pipe: Pipeline = Storable.load("evaluation/UJIndoorLoc/circle/b0/f1/output/BBOX")
    p_reg: Pipeline = Storable.load("evaluation/UJIndoorLoc/circle/b0/f1/output/REG")

    pipe: Pipeline = Storable.load("evaluation/gia/cmp/output/CIRCLE")
    p_reg: Pipeline = Storable.load("evaluation/gia/cmp/output/REG")

    print("-------CIRCLE------- \n")
    # calc success rate
    success, c_dist = get_success_rate(pipe)
    print("ACC: {}".format(success))
    print("CENTER_DIST: {}".format(c_dist))
    # get avg size
    size = get_avg_circle_size(pipe)
    print("SIZE: {}".format(size))

    dist_out, dist_in = get_dist_from_circles(centers=np.concatenate(pipe.summary.y_pred, axis=0)[:, :2],
                          radius=np.concatenate(pipe.summary.y_pred, axis=0)[:, 2],
                          y_true=np.concatenate(pipe.summary.y_true_labels, axis=0))

    print("DEVIATION: out: {}, in: {}".format(dist_out, dist_in))
    #
    # print("--------BOX--------- \n")
    # y_true = np.concatenate(b_pipe.summary.y_true_labels, axis=0)
    # y_pred = np.concatenate(b_pipe.summary.y_pred, axis=0)
    #
    # acc, wrong_mask = calc_acc_c(y_true, y_pred)
    # print("acc: {}".format(acc))
    # print("center-dist: {}".format(center_diff(y_true, y_pred)))
    # print("avg_center: {}".format(np.mean(y_pred[:, :2], axis=0)))
    # print("avg_box_size: {}".format(get_avg_box_size(b_pipe)))
    #
    # dist = get_dist_from_box(b_pipe)
    # print("DEVIATION: {}".format(dist))

    print("--------REG--------- \n")
    # comparison to REG
    radius_c = find_radius_for_acc(p_reg, accuracy=0.9147)
    print("SIZE_REG (CIRCLE): {}".format(PI * math.pow(radius_c, 2)))
    radius_b = find_radius_for_acc(p_reg, accuracy=0.9)
    print("SIZE_REG (CIRCLE): {}".format(PI * math.pow(radius_b, 2)))

    dist_out, dist_in = get_dist_from_circles(
        centers=np.concatenate(p_reg.summary.y_pred, axis=0)[:, :2],
        radius=radius_c,
        y_true=np.concatenate(p_reg.summary.y_true_labels, axis=0))

    print("DEVIATION(circle): out: {}, in: {}".format(dist_out, dist_in))

    dist_out, dist_in = get_dist_from_circles(
        centers=np.concatenate(p_reg.summary.y_pred, axis=0)[:, :2],
        radius=radius_b,
        y_true=np.concatenate(p_reg.summary.y_true_labels, axis=0))

    print("DEVIATION(box): out: {}, in: {}".format(dist_out, dist_in))

    # visualize
    visualize(pipe, p_reg, b_pipe)


def get_dist_from_box(pipe: Pipeline):
    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    diff_x = np.abs(y_true[:, 0] - y_pred[:, 0]) - y_pred[:, 2]/2
    diff_y = np.abs(y_true[:, 1] - y_pred[:, 1]) - y_pred[:, 3]/2

    diff_x = np.maximum(np.zeros_like(diff_x), diff_x)
    diff_y = np.maximum(np.zeros_like(diff_y), diff_y)

    dist = np.sqrt(np.square(diff_x) + np.square(diff_y))

    return np.mean(dist)


def get_dist_from_circles(centers, radius, y_true):

    diff = np.linalg.norm(centers - y_true, axis=1) - radius
    dist_out = np.maximum(np.zeros_like(diff), diff)

    #dist_out = np.max(diff)

    dist_in = np.minimum(np.zeros_like(diff), diff) / radius

    return np.mean(dist_out), np.mean(dist_in)


def get_avg_circle_size(pipe: Pipeline):
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    size = np.mean(PI * np.power(y_pred[:, 2], 2))

    return size

def get_success_rate(pipe: Pipeline):

    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    diff = np.linalg.norm(y_pred[:, :2] - y_true, axis=1)

    correct_idx = np.where(diff <= y_pred[:, 2])[0]

    return len(correct_idx)/len(y_true), np.mean(diff)


def visualize(pipe: Pipeline, p_reg: Pipeline, p_box: Pipeline):

    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
    y_pred_reg = np.concatenate(p_reg.summary.y_pred, axis=0)
    y_pred_box = np.concatenate(p_box.summary.y_pred, axis=0)

    max_el = len(y_true)
    img = "evaluation/gia/gia_floor_4.jpg"
    # fp = FloorPlanPlotRec((83.32, 17.16), 20, floorplan_bg_img=img)
    #plot_data_heatmap(pipe, floor_plotter=fp)
    for idx in range(max_el):
        idx += 0
        fp = FloorPlanPlotRec((83.32, 17.16), 2, floorplan_bg_img=img)
        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='g', alpha=0.5)

        # box_size = y_pred[:]

        fp.draw_circles(centers=y_pred[idx, :2], radius=y_pred[idx, 2], color='r')
        fp.draw_circles(centers=y_pred_reg[idx, :2], radius=3.9849, color='b')
        fp.draw_rectangles_new(anchors=y_pred_box[idx, :])

        fp.show_plot()


if __name__ == "__main__":
    main()