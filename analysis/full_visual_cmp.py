from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np
import math
import matplotlib.pyplot as plt

from pkg_resources import resource_filename

from analysis.evaluate_circle import get_success_rate, get_avg_circle_size, \
    get_dist_from_circles, get_dist_from_box
from analysis.visualize_learning_progress import convert_from_grid, \
    convert_from_2dim_grid, convert_from_2dim_2layer_grid, convert_from_2dim_overlapping_grid
from base.floor_plan_plot import FloorPlanPlotRec
from main import calc_acc_c, center_diff, calc_acc_elipse
from analysis.plot_analysis import find_radius_for_acc, get_avg_box_size #, find_acc_of_area
from analysis.visualize_quantile import get_acc_and_size
from base.acs import bbox_acs, reg_acs


PI = 3.14159265359
lambda_const = 1.0


def main(p_reg_f=None, p_circle_f=None, p_quantile_f=None, p_box_f=None, vis_idx=(0,1000)):

    p_reg = None
    p_quantile = None
    p_circle = None
    p_box = None

    if p_reg_f is not None:
        p_reg: Pipeline = Storable.load(p_reg_f)

    if p_quantile_f is not None:
        p_quantile: Pipeline = Storable.load(p_quantile_f)

    if p_circle_f is not None:
        p_circle: Pipeline = Storable.load(p_circle_f)

    if p_box_f is not None:
        p_box: Pipeline = Storable.load(p_box_f)
        #g_s = p_box.data_provider.
        g_s = p_box.data_provider.grid_size

        pred_vals = np.concatenate(p_box.summary.y_pred, axis=0)
        sub = pred_vals[:, 4::5]
        chosen = np.argmax(sub, axis=1)

        convert_from_2dim_overlapping_grid(p_box, grid_size=g_s, quantile=False,
                                           store=True,
                                           average_samples=True)
        p_box.data_provider.decode_pipe_grid_labels(p_box)

        # determine correct classification rate of BBOX
        y_true_labels = np.concatenate(p_box.summary.y_true_labels, axis=0)

        #p_box.data_provider.transform_to_2dim_overlapping_grid_encoding(
        #    grid_size=g_s, labels=y_true_labels)

        #y_true_grid_labels = p_box.data_provider.grid_labels

        #true_mask = chosen == y_true_grid_labels[:, 2]

        #true_idx = np.where(true_mask)[0]
        #false_idx = np.where(~true_mask)[0]

        y_true = np.concatenate(p_box.summary.y_true_labels, axis=0)
        y_pred = np.concatenate(p_box.summary.y_pred, axis=0)

        #convert_from_2dim_grid(p_box, grid_size=g_s, quantile=False, store=True,
        #                       average_samples=True)
        #correct_box_error = center_diff(y_true[true_idx, :], y_pred[true_idx, :])
        #wrong_box_error = center_diff(y_true[false_idx, :], y_pred[false_idx, :])

        size = np.prod(y_pred[:, 2:4], axis=1)

        #correct_size = np.mean(size[true_idx])
        #wrong_size = np.mean(size[false_idx])

        size_median = np.median(size)

    # compute avg box size per fold
    avg_box_size = [np.mean(np.prod(pred[:, 2:4], axis=1)) for pred in p_box.summary.y_pred]
    print(avg_box_size)
    dp = p_box.data_provider

    #transform_quantile_to_bbox(p_box)
    #convert_from_grid(p_box)

    #p_box.summary.y_pred = [p_box.summary.y_pred[idx]]
    #p_box.summary.y_true_labels = [p_box.summary.y_true_labels[idx]]

    # for fold_idx in range(p_box.data_provider.num_splits):
    #     p_box.summary.y_true_labels[fold_idx][:, 0] *= fp_dims[0]
    #     p_box.summary.y_true_labels[fold_idx][:, 1] *= fp_dims[1]
    #
    #     p_box.summary.y_pred[fold_idx][:, [0, 2]] *= fp_dims[0]
    #     p_box.summary.y_pred[fold_idx][:, [1, 3]] *= fp_dims[1]

    # for fold_idx in range(p_box.data_provider.num_splits):
    #
    #     p_box.summary.y_true_labels[fold_idx][:] = p_box.summary.y_true_labels[fold_idx][:] + np.array([95.333, 29.612])
    #     p_box.summary.y_pred[fold_idx][:, :2] = p_box.summary.y_pred[
    #                                                    fold_idx][:, :2] + np.array(
    #         [95.333, 29.612])

        # p_box.summary.y_true_labels[fold_idx][:, 0] = 200 - \
        #                                               p_box.summary.y_true_labels[
        #                                                        fold_idx][:, 0]
        # p_box.summary.y_true_labels[fold_idx][:, 1] = 80 - \
        #                                               p_box.summary.y_true_labels[
        #                                                        fold_idx][:, 1]
        # p_box.summary.y_pred[fold_idx][:, 0] = 200 - \
        #                                        p_box.summary.y_pred[
        #                                                 fold_idx][:, 0]
        # p_box.summary.y_pred[fold_idx][:, 1] = 80 - \
        #                                        p_box.summary.y_pred[
        #                                                 fold_idx][:, 1]


    if p_circle is not None:

        print("\n -------CIRCLE------- \n")
        # calc success rate
        success, c_dist = get_success_rate(p_circle)
        print("ACC: {}".format(success))
        print("CENTER_DIST: {}".format(c_dist))
        # get avg size
        size = get_avg_circle_size(p_circle)
        print("SIZE: {}".format(size))

        dist_out, dist_in = get_dist_from_circles(
            centers=np.concatenate(p_circle.summary.y_pred, axis=0)[:, :2],
            radius=np.concatenate(p_circle.summary.y_pred, axis=0)[:, 2],
            y_true=np.concatenate(p_circle.summary.y_true_labels, axis=0))

        print("DEVIATION: out: {}, in: {}".format(dist_out, dist_in))

    if p_box is not None:
        print("\n --------BOX--------- \n")
        y_true = np.concatenate(p_box.summary.y_true_labels, axis=0)
        y_pred = np.concatenate(p_box.summary.y_pred, axis=0)

        acc_box, wrong_mask, correct_mask = calc_acc_c(y_true, y_pred)
        #acs = bbox_acs(p_box, lambda_const=lambda_const)

        #acc_box = calc_acc_elipse(y_true, y_pred)
        #b_size = np.mean(PI * np.prod(y_pred[:, 2:]/2.0, axis=1))
        b_size = get_avg_box_size(p_box)
        corr_size = np.mean(y_pred[correct_mask, 2] * y_pred[correct_mask, 3])
        wrong_size = np.mean(y_pred[wrong_mask, 2] * y_pred[wrong_mask, 3])

        print("acc: {}".format(acc_box))
        print("center-dist: {}".format(center_diff(y_true, y_pred)))
        print("avg_center: {}".format(np.mean(y_pred[:, :2], axis=0)))
        print("avg_box_size: {}".format(b_size))
        #print("ACS: {}".format(acs))
        print("avg_box_size (correct): {}".format(corr_size))
        print("avg_box_size (wrong): {}".format(wrong_size))

        dist = get_dist_from_box(p_box)
        print("DEVIATION: {}".format(dist))

    if p_quantile is not None:

        print("\n ------QUANTILE------- \n")
        y_true = np.concatenate(p_quantile.summary.y_true_labels, axis=0)
        y_pred = np.concatenate(p_quantile.summary.y_pred, axis=0)

        acc_quantile, size = get_acc_and_size(p_quantile)

        print("acc: {}".format(acc_quantile))
        #print("center-dist: {}".format(center_diff(y_true, y_pred)))
        #print("avg_center: {}".format(np.mean(y_pred[:, :2], axis=0)))
        print("avg_box_size: {}".format(size))

    radius_b = None
    if p_reg is not None:
        # move closer
        #move_pred_closer(p_reg, dist=0.6)

        print("\n --------REG--------- \n")
        y_true = np.concatenate(p_reg.summary.y_true_labels, axis=0)
        y_pred = np.concatenate(p_reg.summary.y_pred, axis=0)

        print("center-dist: {}".format(center_diff(y_true, y_pred)))
        # comparison to REG
        #radius_c = find_radius_for_acc(p_reg, accuracy=success)
        #print("SIZE_REG (CIRCLE): {}".format(PI * math.pow(radius_c, 2)))

        radius_b = find_radius_for_acc(p_reg, accuracy=acc_box)
        #acc_b = find_acc_of_area(p_reg, area=b_size)
        print("SIZE_REG (BOX): {}".format(PI * math.pow(radius_b, 2)))
        #print("ACC_REG (BOX): {}".format(acc_b))
        #radius_b = find_radius_for_acc(p_reg, accuracy=acc_quantile)
        #print("SIZE_REG (QUANTILE): {}".format(PI * math.pow(radius_b, 2)))

        #acs = reg_acs(p_reg, lambda_const=lambda_const, radius=radius_b)
        #print("ACS: {}".format(acs))

    plt.show()

    #radius_b = 10
    visualize(p_reg, p_circle, p_quantile, p_box, radius_b, vis_idx)


def visualize(p_reg: Pipeline=None, p_circle: Pipeline=None, p_quantile: Pipeline=None, p_box: Pipeline=None, radius=9.0, vis_idx=(0,20)):
    #img = "../evaluation/gia/gia_floor_4.jpg"
    #fp_dims = (83.32, 17.16)
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    #img = "/Users/mariuslaska/sciebo/SensorDatenGIA/Gebäude STL/transparent/2130_4.og.png"
    fp_dims = (200, 80) # (200, 80)
    #fp_dims = (83.32, 17.6) #50.5)

    #fp = FloorPlanPlotRec((83.32, 17.16), 20, floorplan_bg_img=img)
    # plot_data_heatmap(pipe, floor_plotter=fp)
    if p_reg is not None:
        y_pred_reg = np.concatenate(p_reg.summary.y_pred, axis=0)
        y_true = np.concatenate(p_reg.summary.y_true_labels, axis=0)
    if p_circle is not None:
        y_pred_circle = np.concatenate(p_circle.summary.y_pred, axis=0)
        y_true = np.concatenate(p_circle.summary.y_true_labels, axis=0)

    if p_quantile is not None:
        y_pred_quantile = np.concatenate(p_quantile.summary.y_pred, axis=0)
        y_true = np.concatenate(p_quantile.summary.y_true_labels, axis=0)

    if p_box is not None:
        y_pred_box = np.concatenate(p_box.summary.y_pred, axis=0)
        y_true = np.concatenate(p_box.summary.y_true_labels, axis=0)

        #mask = np.where(np.logical_and(y_pred_box[:, 0] >= y_pred_box[:, 0], y_pred_box[:, 2] < y_pred_box[:, 3]))[0]

        # y_pred_box[:, 2:] = np.abs(y_pred_box[:, 2:])
        # mask = np.where(np.logical_and(np.greater_equal(y_pred_box[:, 3], y_pred_box[:, 3]), y_true[:, 0] >= 150))[0]
        #y_true = y_true[mask]
        #y_pred_box = y_pred_box[mask]

    # for (train, test) in p_box.data_provider.splits_indices:
    #     fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
    #     fp.draw_points(p_box.data_provider.labels[train, 0], p_box.data_provider.labels[train, 1], color='g', alpha=0.5)
    #     fp.draw_points(p_box.data_provider.labels[test, 0],
    #                    p_box.data_provider.labels[test, 1], color='r',
    #                    alpha=0.5)
    #     fp.show_plot()

    #img = "/home/laskama/Dropbox/PhD/BboxPaper/images/blank_bg.png"
    #fp_dims = (125, fp_dims[1])
    fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)

    # plot training data
    x_train, y_train = p_box.data_provider.get_train_data(area_labels=False, split_idx=0)
    fp.draw_points(y_train[:, 0], y_train[:, 1], color="green", alpha=0.5)

    idx_len = vis_idx[1]-vis_idx[0]

    sub_sample_mask = np.random.choice(np.arange(vis_idx[0], vis_idx[1]), int(idx_len * 0.7), replace=False)

    for idx in range(idx_len):#len(y_true)):
        idx += vis_idx[0]
        #if idx not in sub_sample_mask:
        #    continue

        #if np.prod(y_pred_box[idx, 2:]) < 600:
        #    continue

        #fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)

        # (train, test) = p_box.data_provider.splits_indices[0]
        # fp.draw_points(p_box.data_provider.labels[train, 0],
        #                p_box.data_provider.labels[train, 1], color='g',
        #                alpha=0.5)
        # fp.draw_points(p_box.data_provider.labels[test, 0],
        #                p_box.data_provider.labels[test, 1], color='r',
        #                alpha=0.5)

        # # find points that are within box
        # lab = p_box.data_provider.labels
        #
        # x_test_full = None
        # for s_idx in range(p_box.data_provider.num_splits):
        #     x_test, y_test = p_box.data_provider.get_test_data(labels=p_box.data_provider.grid_labels, split_idx=s_idx)
        #     if x_test_full is not None:
        #         x_test_full = np.concatenate((x_test_full, x_test), axis=0)
        #     else:
        #         x_test_full = x_test
        #
        # sig_diff_threshold = 2.0
        # dat = p_box.data_provider.data_tensor
        # x_range = np.logical_and(lab[:, 0] < y_pred_box[idx, 0] + y_pred_box[idx, 2]/2.0,
        #                          lab[:, 0] > y_pred_box[idx, 0] - y_pred_box[idx, 2]/2.0)
        # y_range = np.logical_and(lab[:, 1] < y_pred_box[idx, 1] + y_pred_box[idx, 3]/2.0,
        #                          lab[:, 1] > y_pred_box[idx, 1] - y_pred_box[idx, 3]/2.0)
        #
        # inside_mask = np.where(np.logical_and(x_range, y_range))[0]
        # outside_mask = np.where(~np.logical_and(x_range, y_range))[0]
        #
        # sig_diff = np.linalg.norm(x_test_full - x_test_full[idx, :], axis=1)
        # # color with alpha depending on sig_diff
        # pred_colors = np.zeros((len(inside_mask), 4))
        # # for red the first column needs to be one
        # pred_colors[:, 0] = 1.0
        # pred_colors[:, 3] = 1 - sig_diff[inside_mask] / sig_diff_threshold
        #
        # fp.draw_points(lab[inside_mask, 0], lab[inside_mask, 1], color=pred_colors, alpha=None)#, alpha=0.5)
        #fp.draw_points(lab[outside_mask, 0], lab[outside_mask, 1], color='black',
        #               alpha=0.2)

        # box_size = y_pred[:]
        if p_circle is not None:
            fp.draw_circles(centers=y_pred_circle[idx, :2], radius=y_pred_circle[idx, 2], color='r')
        if p_reg is not None:
            fp.draw_circles(centers=y_pred_reg[idx, :2], radius=radius, color='b')
        if p_quantile is not None:
            fp.draw_points(y_pred_quantile[idx, 0], y_pred_quantile[idx, 1], color="r")
            fp.draw_points(y_pred_quantile[idx, 2], y_pred_quantile[idx, 3],
                           color="b")

            fp.draw_rectangles(anchors=y_pred_quantile[idx, :], color="g")
        if p_box is not None:
            fp.draw_rectangles_new(anchors=y_pred_box[idx, :], color='black')
            #fp.draw_ellipse(y_pred_box[idx, :2], y_pred_box[idx, 2], y_pred_box[idx, 3], color="r")

        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='red', alpha=0.5)

    fp.show_plot()


def transform_quantile_to_bbox(pipe: Pipeline):
    summary = []
    for fold in pipe.summary.y_pred:
        width = fold[:, 2] - fold[:, 0]
        height = fold[:, 3] - fold[:, 1]
        center_x = fold[:, 0] + width / 2
        center_y = fold[:, 1] + height / 2

        f = np.stack((center_x, center_y, width, height), axis=1)

        summary.append(f)

    pipe.summary.y_pred = summary


def move_pred_closer(pipe: Pipeline, dist=1.0):

    p = []

    for (y_pred, y_true) in zip(pipe.summary.y_pred, pipe.summary.y_true_labels):
        diff = y_pred - y_true

        arg_max = np.argmax(np.abs(diff), axis=1)

        left_minus = np.where(np.logical_and(arg_max == 0, diff[:, 0] < 0))[0]
        left_plus = np.where(np.logical_and(arg_max == 0, diff[:, 0] > 0))[0]

        right_minus = np.where(np.logical_and(arg_max == 1, diff[:, 1] < 0))[0]
        right_plus = np.where(np.logical_and(arg_max == 1, diff[:, 1] > 0))[0]

        y_pred[left_minus] += dist
        y_pred[left_plus] -= dist

        y_pred[right_minus, 1] += dist
        y_pred[right_plus, 1] -= dist

        p.append(y_pred)

    pipe.summary.y_pred = p


if __name__ == "__main__":
    #main(p_box_f="../evaluation/gia/evaluation/floor_classifier_v2/output/FLOOR_4_box_up_pred")#_up_pred")
        #p_reg_f="../evaluation/gia/evaluation/floor_classifier_v2/output/FLOOR_1_reg_2")
         #p_reg_f="../evaluation/simulation/evaluation/big/output/REG_2")
        #main(p_reg_f="../evaluation/lohan/evaluation/new_knn/output/REG_2",
        #     p_box_f="../evaluation/lohan/evaluation/new_knn/output/BBOX_2")

    f1 = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/new_knn/output/BBOX_2"
    f2 = "../evaluation/lohan/evaluation/grid_new_loss/output/BBOX_v2"
    reg1 = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/new_knn/output/REG_1"
    reg2 = "../evaluation/lohan/evaluation/grid_new_loss/output/REG_DEEP"

    f = "../evaluation/lohan/evaluation/2layer_grid/output/BBOX"

    f1 = "../evaluation/lohan/evaluation/overlap/output/{}".format("BBOX_aug3")
    f2 = "../evaluation/lohan/evaluation/grid_new_loss/output/REG_DEEP"

    f1 = "/home/laskama/Dropbox/evaluation/UJIndoorLoc/b0/f1/final/output/BBOX_1l_5o_aug_5"
    f2 = "/home/laskama/Dropbox/evaluation/UJIndoorLoc/b0/f0/final/output/REG_1"
    f1 = "/home/laskama/Dropbox/evaluation/lohan/BBOX_1l_3o_aug_1"
    f1 = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/UJIndoorLoc/b0/f0/final/output/BBOX_1l_20_aug_1"

    f1 = "/media/laskama/Daten/BBOX/UJIndoorLoc/b0/f1/test/output/BBOX_1l_20_aug_1"
    f1 = "/media/laskama/Daten/BBOX/lohan/test/output/BBOX_1l_10_aug_1"

    f1 = "/Users/mariuslaska/Dropbox/tmp/BBOX_1l_15_1"

    f1 = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/uji/cnn/output/CNN_BBOX_test_1"
    f2 = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/uji/cnn/output/CNNLoc_1"

    f2 = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/uji/cnn/hpc/CNNLoc_1"
    f1 = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/uji/cnn/hpc/DNN-DLB_1"
    f1 = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/uji/gpu/full_grid/scalable_dnn/output/BASE-DNN-DLB_1"

    main(p_box_f=f1,
         p_reg_f=None,
         vis_idx=(0,100))