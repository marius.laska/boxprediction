from il_pipeline.pipeline import Pipeline
from il_pipeline.pipeline import Storable
from pkg_resources import resource_filename
import numpy as np

from base.data_provider_base_grid import DataProviderGridBase
from base.floor_plan_plot import FloorPlanPlotRec


def main():

    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    fp_dims = (200, 80)

    walls_h = np.load("hor_walls.npy")
    #walls_h_cor = walls_h[np.where(np.arange(len(walls_h)) != 84)[0]]
    #np.save("hor_walls.npy", walls_h_cor)
    fp = FloorPlanPlotRec(fp_dims, 5, floorplan_bg_img=img, walls_file="ver_walls.npy", add_walls=True)

    fp.show_plot()
    # plot_data_heatmap(pipe, floor_plotter=fp)


def encode():
    walls_h = np.load("hor_walls.npy")
    walls_v = np.load("ver_walls.npy")

    dp = DataProviderGridBase()
    dp.encode_walls_to_2dim_overlapping_grid_encoding(walls_h, horizontal=True,
                                                      grid_size=40, width=200,
                                                      height=80)
    #dp.encode_walls_to_2dim_overlapping_grid_encoding(walls_v, horizontal=False,
    #                                                  grid_size=40, width=200,
    #                                                  height=80)
    print("test")

if __name__ == '__main__':
    #encode()
    main()
