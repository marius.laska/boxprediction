from il_pipeline.pipeline import Pipeline
from il_pipeline.pipeline import Storable
from pkg_resources import resource_filename

from base.floor_plan_plot import FloorPlanPlotRec

# p: Pipeline = Storable.load("/media/laskama/Daten/BBOX/lohan/test/output/BBOX_1l_10_aug_1")
p: Pipeline = Storable.load("/media/laskama/Daten/BBOX/UJIndoorLoc/b0/f1/test/output/BBOX_1l_20_aug_1")

img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
#img = "/Users/mariuslaska/sciebo/SensorDatenGIA/Gebäude STL/transparent/2130_4.og.png"
fp_dims = (125, 150) # (200, 80)
#fp_dims = (83.32, 17.6) #50.5)
img = "/home/laskama/Dropbox/PhD/BboxPaper/images/blank_bg.png"
#fp_dims = (125, fp_dims[1])

fp = FloorPlanPlotRec(fp_dims, 5, floorplan_bg_img=img)
fp.draw_points(p.data_provider.labels[:, 0], p.data_provider.labels[:, 1], color="grey", alpha=0.5)
fp.draw_measurement_distribution(p.data_provider.labels, alpha=0.5)

fp.show_plot()
# plot_data_heatmap(pipe, floor_plotter=fp)