import math
import os

from il_pipeline.summary.summary import KFoldClassSummary
from il_pipeline.utility.storable import Storable

from base.BboxModel import BboxModel
from base.bbox_pipeline import BboxPipeline


def load_pipeline_params(model_file, weights_file):
    spit_type = "train"

    pipe: BboxPipeline = Storable.load(model_file)
    params = pipe.model_params
    dp = pipe.data_provider
    m_type = params['type']
    summary = KFoldClassSummary([], [], [])
    model = BboxModel(params['type'], summary, pipe.data_provider, params,
                      pipe.config.output_dir, pipe.filename)
    model.setup_model(setup_params=True)


    num_train, num_val, num_test = dp.get_train_val_test_num(
        area_labels=model.type != "regression")
    train_bs = min(num_train, params['batch_size'])
    test_bs = min(num_test, train_bs)

    for fold in range(dp.num_splits):
        dp.current_split_idx = fold
        evaluate_model(model, train_bs, spit_type)

    # change output dir for correct storage
    pipe.config.output_dir = "../" + pipe.config.output_dir

    pipe = BboxPipeline(dp, pipe.clusterer,
                        pipe.config,
                        pipe.model_params,
                        pipe.filename + "_pred")
    pipe.summary = summary

    pipe.store()

def evaluate_model(model, test_bs, split_type="test"):
    """
    Evaluates the model on separate test data (supplied by data generator)
    :param test_bs: batch size of test data
    """

    data_provider = model.data_provider
    summary = model.summary
    classifier = model.classifier

    path = "../" + model.output_dir + "{}_f{}.hdf5".format(model.filename, data_provider.current_split_idx)
    if os.path.exists(path):
        classifier.load_weights(path)

    # calculate the amount of steps on the test data before one batch
    # is completed (required by generator)
    num_train, num_val, num_test = data_provider.get_train_val_test_num(area_labels=model.type != "regression")
    steps_per_epoch = math.ceil(num_train / test_bs)

    area_labels = model.type == "classification" or model.type == "cnn"

    #y_pred_list = []
    #for idx in range(100):

    y_pred = classifier.predict_generator(
        data_provider.in_mem_data_generator(
            mode=split_type, model_type=model.type,
            area_labels=area_labels, batch_size=test_bs),
        steps=steps_per_epoch,
        verbose=0)

    #y_pred_list.append(y_pred)

    #if self.type != "regression":
    if data_provider.area_labels is not None:
        _, y_true = data_provider.get_test_data()
        summary.y_true.append(y_true)

    if split_type == "test":
        _, y_true_labels = data_provider.get_test_data(area_labels=False)
    elif split_type == "val":
        _, y_true_labels = data_provider.get_val_data(area_labels=False)
    elif split_type == "train":
        _, y_true_labels = data_provider.get_train_data(area_labels=False)

    summary.y_pred.append(y_pred)
    summary.y_true_labels.append(y_true_labels)

    summary.num_folds += 1


if __name__ == "__main__":

    m_f = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/gia/evaluation/floor_classifier_v2/output/FLOOR_4_box_up"
    w_f = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/gia/evaluation/floor_classifier_v2/output/FLOOR_4_box_up_f0.hdf5"

    load_pipeline_params(m_f, w_f)

