from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from pkg_resources import resource_filename
import numpy as np
import matplotlib.pyplot as plt

from typing import List
from analysis.visualize_box_overlap import normalize, add_colorbar
from analysis.visualize_learning_progress import convert_from_2dim_grid
from base.floor_plan_plot import FloorPlanPlotRec
import math

PI = math.pi

def main(mcd_box_f=None, mcd_point_f=None, point_f=None, vis_idx=(0,2)):
    file = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/grid_test/output/BBOX_TEST_3_v5_stat"

    if mcd_box_f is not None:
        pipe: Pipeline = Storable.load(mcd_box_f)
        # calc avg box size
        #y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
        #box_size_len = np.sqrt(np.mean(np.prod(y_pred[:, 2:, :], axis=1)))

        convert_from_2dim_grid(pipe, grid_size=40, quantile=False, store=True, average_samples=False)

    if mcd_point_f is not None:
        pipe_reg: Pipeline = Storable.load(mcd_point_f)
        pipe_reg = convert_reg_to_box(pipe_reg, box_size_len=10)

    pipe_reg_deep: Pipeline = Storable.load("/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/grid_test/output/REG_TEST_3_stat")
    #pipe_reg_deep = convert_reg_to_box(pipe_reg_deep, box_size_len=10)

    if point_f is not None:
        pipe_point: Pipeline = Storable.load(point_f)

    pipe_list = [pipe_point, pipe, pipe_reg] #[pipe_point, pipe, pipe_reg, pipe_reg_deep]

    visualize(p_box_list=pipe_list, vis_idx=vis_idx)

    print("test")


def convert_reg_to_box(p_reg: Pipeline=None, box_size_len=10):
    # convert p_reg to box with constant box size
    y_pred_post = []
    for y_pred, y_true in zip(p_reg.summary.y_pred, p_reg.summary.y_true_labels):
        y_pred = np.stack(y_pred, axis=2)
        box_dims = np.ones_like(y_pred) * box_size_len

        y_pred = np.concatenate((y_pred, box_dims), axis=1)
        y_pred_post.append(y_pred)

    p_reg.summary.y_pred = y_pred_post

    return p_reg


def visualize(p_box: Pipeline=None, p_box_list: List[Pipeline]=None, vis_idx=(0,2)):

    model_stats = [{"grid_prob": [], "correct": [], "num_cells": [],
                    "avg_cell_size": [], "draw_map": [], "draw_norm": [],
                    "prob_map": []} for _ in p_box_list]

    plot_num = 1400

    # get avg box size per train/test split
    # for m_idx, model in enumerate(p_box_list):
    #     for fold in model.summary.y_pred:
    #         avg_cell_size = np.mean(np.prod(fold[:, 2:, :], axis=1))
    #         model_stats[m_idx]["avg_cell_size"].append(avg_cell_size)

    # img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    # fp_dims = (200, 80)
    # p_box = p_box_list[0]
    # for (train, test) in p_box.data_provider.splits_indices:
    #     fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
    #     fp.draw_points(p_box.data_provider.labels[train, 0], p_box.data_provider.labels[train, 1], color='g', alpha=0.5)
    #     fp.draw_points(p_box.data_provider.labels[test, 0],
    #                    p_box.data_provider.labels[test, 1], color='r',
    #                    alpha=0.5)
    # fp.show_plot()

    for idx in range(vis_idx[1]-vis_idx[0]):#len(y_true)):
        print(idx/plot_num)
        idx+=vis_idx[0]
        for m_idx, model in enumerate(p_box_list):
            y_pred_box = np.concatenate(model.summary.y_pred, axis=0)
            y_true = np.concatenate(model.summary.y_true_labels, axis=0)

            if len(np.shape(y_pred_box)) == 3:
                multi_box = np.transpose(y_pred_box[idx, :, :])
                m_type = "box"
            else:
                multi_box = y_pred_box[idx, :]
                m_type = "point"

            if not hasattr(model, 'model_stats'):
                grid_prob, num_cells, draw_map, draw_norm, prob_map = \
                    plot_box_overlap_per_grid(
                        model, multi_box, y_true[idx], plot=True, model=m_type)

                model_stats[m_idx]["draw_map"].append(draw_map)
                model_stats[m_idx]["draw_norm"].append(draw_norm)
                model_stats[m_idx]["prob_map"].append(prob_map)

                model_stats[m_idx]["correct"].append(grid_prob > 0)
                model_stats[m_idx]["grid_prob"].append(grid_prob)
                model_stats[m_idx]["num_cells"].append(num_cells)
            else:
                pass
                #plot_box_existing_stats(model, y_true[idx], idx)

        plt.show()

    # for m, stat in zip(p_box_list, model_stats):
    #     m.model_stats = stat
    #     m.config.output_dir = "../" + m.config.output_dir
    #     m.store(m.filename + "_stat")

    model_stats = [box.model_stats for box in p_box_list]
    #
    for m_idx, m_stat in enumerate(model_stats):
        mask = np.where(~np.isnan(m_stat["grid_prob"]))[0]

        print("M_{}: ACC: {}, AVG_GRID_PROB: {}, NUM_CELLS: {}, AVG_CELL_SIZE: {}".format(
            m_idx,
            len(np.where(np.array(m_stat["correct"]))[0])/plot_num,
            np.mean(np.array(m_stat["grid_prob"])[mask]),
            np.mean(np.array(m_stat["num_cells"])),
            m_stat["avg_cell_size"]))


def plot_box_existing_stats(pipe: Pipeline, y_true, o_idx):
    # img = "../evaluation/gia/gia_floor_4.jpg"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    # fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    dp = pipe.data_provider
    fp_height = dp.floorplan_height
    fp_width = dp.floorplan_width
    g_size = 2

    # colormap
    cmap = plt.cm.jet  # define the colormap
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # force the first color entry to be grey
    cmaplist[0] = (.5, .5, .5, 0.1)

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))

    # Set alpha
    my_cmap[:, -1] = 0.8
    my_cmap[0] = (.75, .75, .75, 0.8)

    num_g_x = int(fp_width / g_size)
    num_g_y = int(fp_height / g_size)

    fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img,
                          title="Overlap")
    plots = [fp]

    norms = [pipe.model_stats["draw_norm"][o_idx]]
    overlap_count = pipe.model_stats["draw_map"][o_idx]
    oc = pipe.model_stats["prob_map"][o_idx]

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            count = overlap_count[num_g_y - y_idx - 1, x_idx]
            c_val = oc[num_g_y - y_idx - 1, x_idx]

            if count > 0:  # and c_val > 0.0001:
                fp.draw_rectangles(np.concatenate((lower_left, upper_right)),
                                   color=my_cmap[int(count)], fill=True)

    fp.draw_points(y_true[0], y_true[1], color='g', alpha=1)

    for p, n in zip(plots, norms):
        add_colorbar(p, cmap, n)

def plot_box_overlap_per_grid(pipe: Pipeline, multi_box, y_true, plot=True, model="box"):

    dp = pipe.data_provider
    fp_height = dp.floorplan_height
    fp_width = dp.floorplan_width
    g_size = 2

    num_g_x = int(fp_width / g_size)
    num_g_y = int(fp_height / g_size)

    overlap_count = np.full((num_g_y, num_g_x), np.nan)
    #kernel_vals = np.full((num_g_y, num_g_x), np.nan)

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            # get average error per grid
            if model == "point":
                kernel_val = kernel_value(multi_box, lower_left, upper_right)
                overlap_count[num_g_y - y_idx - 1, x_idx] = kernel_val

            elif model == "box":
                overlap = overlap_grid(multi_box, lower_left, upper_right)
                overlap_count[num_g_y - y_idx - 1, x_idx] = overlap


    overlap_count, norm_overlap_count = normalize(overlap_count, min=1, max=255)

    # normalize, such that sum = 1 (for PDF)
    oc = overlap_count / np.sum(overlap_count)
    #overlap_count /= np.sum(overlap_count)

    # colormap
    cmap = plt.cm.jet  # define the colormap
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # force the first color entry to be grey
    cmaplist[0] = (.5, .5, .5, 0.1)

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))

    # Set alpha
    my_cmap[:, -1] = 0.8
    my_cmap[0] = (.75, .75, .75, 0.8)

    # img = "../evaluation/gia/gia_floor_4.jpg"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    # fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    if plot:
        fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="Overlap")
        plots = [fp]

    norms = [norm_overlap_count]

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            count = overlap_count[num_g_y - y_idx - 1, x_idx]
            c_val = oc[num_g_y - y_idx - 1, x_idx]

            if plot and count > 0:# and c_val > 0.0001:
                fp.draw_rectangles(np.concatenate((lower_left, upper_right)),
                                   color=my_cmap[int(count)], fill=True)

    if plot:
        fp.draw_points(y_true[0], y_true[1], color='g', alpha=1)

    val = get_probability(y_true, overlap_count, g_size, num_g_y)

    if plot:
        for p, n in zip(plots, norms):
            add_colorbar(p, cmap, n)

    #fp.show_plot()
    num_selected_cells = len(np.where(overlap_count > 0)[0])
    return val, num_selected_cells, overlap_count, norm_overlap_count, oc


def get_probability(y_true, overlap_count, g_size, num_g_y):
    oc = overlap_count / np.sum(overlap_count)
    g = (y_true / g_size).astype(int)
    try:
        val = oc[num_g_y - g[1] - 1, g[0]]
        s = np.sum(oc[np.where(oc >= val)])
    except IndexError:
        return 0
    return val


def kernel_value(y_pred, lower_left, upper_right):
    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    c_x = min_x + (max_x - min_x) / 2
    c_y = min_y + (max_y - min_y) / 2

    return gaussian_kernel(c_x - y_pred[0], c_y - y_pred[1], o=6, cutoff=200)


def gaussian_kernel(x, y, o, cutoff=10):
    if np.square(x) + np.square(y) > cutoff:
        return np.nan

    return (1 / (2*PI*np.square(o))) \
           * np.exp(- (np.square(x) + np.square(y)) / (2*np.square(o)))


def overlap_grid(y_pred, lower_left, upper_right):
    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    ll_x = y_pred[:, 0] - y_pred[:, 2]/2.0
    ll_y = y_pred[:, 1] - y_pred[:, 3]/2.0
    ll = np.stack((ll_x, ll_y), axis=1)

    ur_x = y_pred[:, 0] + y_pred[:, 2]/2.0
    ur_y = y_pred[:, 1] + y_pred[:, 3] / 2.0
    ur = np.stack((ur_x, ur_y), axis=1)

    ll_range = np.logical_and(ll[:, 0] <= min_x, ll[:, 1] <= min_y)
    ur_range = np.logical_and(ur[:, 0] >= max_x, ur[:, 1] >= max_y)

    range = np.logical_and(ll_range, ur_range)

    mask = np.where(range)[0]

    if len(mask) == 0:
        return np.nan
    else:
        return len(mask)

def kde_error_plot():
    #pipe: Pipeline = Storable.load("/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/deep/output/REG")
    pipe: Pipeline = Storable.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/grid_test/output/REG_TEST")

    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)

    error = np.linalg.norm(y_pred - y_true, axis=1)
    mean = np.mean(error)
    std = np.std(error)

    from sklearn.neighbors import KernelDensity

    # Gaussian KDE
    fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
    X_plot = np.linspace(0, 25, 1000)[:, np.newaxis]
    kde = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(error.reshape(-1, 1))
    log_dens = kde.score_samples(X_plot)
    ax[1, 1].fill(X_plot[:, 0], np.exp(log_dens), fc='#AAAAFF')
    ax[1, 1].text(-3.5, 0.31, "Gaussian Kernel Density")
    plt.show()

if __name__ == "__main__":
    main(mcd_box_f="../evaluation/lohan/evaluation/grid_test/output/BBOX_TEST_3_v5",
         mcd_point_f="../evaluation/lohan/evaluation/grid_test/output/REG_TEST",
         point_f="../evaluation/lohan/evaluation/deep/output/REG",

         )
    #kde_error_plot()
    pipe: Pipeline = Storable.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/new_knn/output/BBOX_1")
    #visualize_reg(pipe)