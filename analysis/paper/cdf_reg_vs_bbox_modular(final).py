import os
import sys

from sklearn import linear_model

from analysis.correlation_knn import evaluate_correlation

sys.path.append('/home/laskama/PycharmProjects/bboxPrediction')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import interp1d
import seaborn as sns

from analysis.plot_analysis import get_avg_box_size
from analysis.visualize_learning_progress import \
    convert_from_2dim_overlapping_grid
from main import calc_acc_c

dbscan_split = True
dataset = "UJI"

beta_0 = "$\\beta_0$"
beta_1 = "$\\beta_1$"
r_2 = "$R^2$"

e_n = "$e$"
e_x_n = "$e_x$"
e_y_n = "$e_y$"

size_n = "$w \cdot h$"
w_n = "$w$"
h_n = "$h$"

def _calc_reg_stats(reg_size_dict, pipe_name, col_val, model_name, num_avg= 10, show_plot=True):
    #num_avg = 10

    dist_list = []
    size_list = []

    mse_list = []

    for idx in range(num_avg):

        p: Pipeline = Storable.load(pipe_name.format(idx+1))

        if f_idx is None:
            y_true = np.concatenate(p.summary.y_true_labels, axis=0)
            y_pred = np.concatenate(p.summary.y_pred, axis=0)
        else:
            y_true = p.summary.y_true_labels[f_idx]
            y_pred = p.summary.y_pred[f_idx]

        n = len(y_true)
        dist = np.sort(np.linalg.norm(y_pred - y_true, axis=1))

        res_dict["Model"] += [model_name] * len(dist)
        res_dict["MSE"] += dist.tolist()
        res_dict["fold"] += [f_idx] * len(dist)

        mse_list.append(np.mean(dist))

        size = np.pi * np.square(dist)

        dist_list.append(dist)
        size_list.append(size)

    size_avg = np.sort(np.concatenate(size_list, axis=0))
    acc_list = [i / (n * num_avg) for i in range(n * num_avg)]

    reg_size_dict["Model"].append(model_name)
    reg_size_dict["size"].append(size_avg)
    reg_size_dict["ACC"].append(np.array(acc_list))

    if show_plot:
        #plt.title(fig_title)
        plt.xlabel("Area (" + r'$\epsilon$' + ") [" + r'$m^2$' + "]")
        plt.ylabel("Success rate (" + r'$\gamma$' + ")")
        plt.plot(size_avg, acc_list,
                 linestyle="dashed", color=col_val, label=model_name)


def calc_reg_stats(reg_size_dict, num_layers=3, dataset="lohan", show_plot=True):

    colors = ["red", "blue", "green", "black"]

    # first do DNN REG
    if dataset == "lohan":
        if dbscan_split:
            # dbscan
            file = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/new_dropout/output/DNN_hidden_layers_{}"
        else:
            # random
            #file = "/home/laskama/Dropbox/evaluation/lohan/REG_{}l"
            file = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/random/output/DNN_hidden_layers_{}"

    elif dataset == "UJI":
        file = "/media/laskama/Daten/BBOX/GPU/uji/gpu/b0/f{}/new_dropout/output/DNN_hidden_layers_{}"

    for nl in range(num_layers):
        nl_idx = nl + 1
        if dataset == "lohan":
            pipe_name = file.format(nl_idx) + "_{}"
        else:
            pipe_name = file.format(floor, nl_idx) + "_{}"
        model_name = "DNN -> O ({} HL)".format(nl_idx)
        col_val = colors[nl]
        _calc_reg_stats(reg_size_dict, pipe_name, col_val, model_name, num_avg=10, show_plot=True)

    # do CNN
    if dataset == "lohan":
        if dbscan_split:
            # dbscan
            file = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/new_dropout/output/2D-CNN"
        else:
            # random
            #file = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/cnn_final/output/2D-CNN"
            file = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/random/output/2D-CNN"
        pipe_name = file + "_{}"

    elif dataset == "UJI":
        #file = "/media/laskama/Daten/BBOX/GPU/uji/gpu/b0/f{}/new_dropout/output/2D_CNN"
        file = "/media/laskama/Daten/BBOX/GPU/uji/gpu/b0/f{}/cnn_final/output/2D-CNN"
        pipe_name = file.format(floor) + "_{}"

    model_name = "CNN -> O"
    col_val = colors[-1]
    _calc_reg_stats(reg_size_dict, pipe_name, col_val, model_name,
                    num_avg=10, show_plot=True)

    # # kNN
    # file = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/uji/gpu/b0/f{}/knn/output/kNN"
    # file = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/lohan/knn_random/output/kNN"
    # file = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/lohan/knn_dbscan/output/kNN"
    # model_name = "kNN -> O"
    # pipe_name = file + "_{}"
    # col_val = colors[-1]
    # _calc_reg_stats(reg_size_dict, pipe_name, col_val, model_name,
    #                 num_avg=1, show_plot=True)

    plt.legend(loc="lower right")


def _calc_box_stats(reg_size_dict, box_dict, mse_list, pipe_name, model_name, plot_name, col_val, marker, num_avg=10, calc_aug_gain=False, show_plot=True, plot_regression=True, thres_idx=-1):
    b_size_list = []
    acc_box_list = []
    dist_list = []

    num_add_aug_ratio = -1

    summary = {"Y": [], "X": [], beta_0: [], beta_1: [], "p-value": [], r_2: [], "model": [], "RSE": [], "Y_mean": []}

    error_vec_list = []
    error_vec_x_list = []
    error_vec_y_list = []
    size_vec_list = []
    size_vec_x_list = []
    size_vec_y_list = []

    for idx in range(num_avg):

        file = pipe_name.format(idx + 1)

        if not os.path.exists(file):
            print(file)
            continue

        p: Pipeline = Storable.load(file)

        y_true_grid = np.concatenate(
            [p.data_provider.get_test_data(
                labels=p.data_provider.grid_labels,
                split_idx=idx)[1] for idx in range(
                p.data_provider.num_splits)], axis=0)

        #_, y_true_grid = p.data_provider.get_test_data(labels=p.data_provider.grid_labels)

        chosen = np.argmax(np.concatenate(p.summary.y_pred, axis=0)[:, 4::5],
                           axis=1)
        correct_grid_cell_mask = chosen == y_true_grid[:, 2]

        convert_from_2dim_overlapping_grid(p, grid_size=p.data_provider.grid_size,
                                           quantile=False,
                                           store=True,
                                           average_samples=True)
        if f_idx is None:
            y_true = np.concatenate(p.summary.y_true_labels, axis=0)
            y_pred = np.concatenate(p.summary.y_pred, axis=0)
        else:
            y_true = p.summary.y_true_labels[f_idx]
            y_pred = p.summary.y_pred[f_idx]

        thresholds = [120, 275, 550, 800]
        if thres_idx < len(thresholds) and thres_idx != -1:
            sub_size_mask = (y_pred[:, 2] * y_pred[:, 3]) < thresholds[thres_idx] # 75, 275, 550, 535
            sub_idx = np.where(np.logical_and(correct_grid_cell_mask, sub_size_mask))[0]
        else:
            sub_idx = np.where(correct_grid_cell_mask)[0]

        error_vec = np.linalg.norm(y_pred[sub_idx, :2] - y_true[sub_idx, :2],
                                   axis=1)
        error_vec_x = np.abs(y_pred[sub_idx, 0] - y_true[sub_idx, 0])
        error_vec_y = np.abs(y_pred[sub_idx, 1] - y_true[sub_idx, 1])

        size_vec = y_pred[sub_idx, 2] * y_pred[sub_idx, 3]
        size_vec_x = y_pred[sub_idx, 2]
        size_vec_y = y_pred[sub_idx, 3]

        error_vec_list.append(error_vec)
        error_vec_x_list.append(error_vec_x)
        error_vec_y_list.append(error_vec_y)

        size_vec_list.append(size_vec)
        size_vec_x_list.append(size_vec_x)
        size_vec_y_list.append(size_vec_y)

        evaluate_correlation(size_vec, error_vec, e_n, size_n, summary)

        if calc_aug_gain:
            if num_add_aug_ratio == -1 and dataset == "UJI":
                x, _ = p.data_provider.get_augmented_train_data()
                x2, _ = p.data_provider.get_train_data(
                    labels=p.data_provider.grid_labels)
                num_add_aug_ratio = (len(x) - len(x2)) / len(x2)

            elif num_add_aug_ratio == -1 and dataset == "lohan":
                if f_idx is None:
                    x_sum = 0
                    x2_sum = 0
                    for f in range(p.data_provider.num_splits):
                        p.data_provider.current_split_idx = f
                        x, _ = p.data_provider.get_augmented_train_data()
                        x2, _ = p.data_provider.get_train_data(
                            labels=p.data_provider.grid_labels)
                        x_sum += len(x)
                        x2_sum += len(x2)
                    num_add_aug_ratio = (x_sum - x2_sum) / x2_sum
                else:
                    p.data_provider.current_split_idx = f_idx
                    x, _ = p.data_provider.get_augmented_train_data()
                    x2, _ = p.data_provider.get_train_data(
                        labels=p.data_provider.grid_labels)
                    num_add_aug_ratio = (len(x) - len(x2)) / len(x2)

        acc_box, wrong_mask, correct_mask = calc_acc_c(y_true,
                                                       y_pred)
        b_size = get_avg_box_size(p)

        acc_box_list.append(acc_box)
        b_size_list.append(b_size)
        dist = np.linalg.norm(y_pred[:, :2] - y_true, axis=1)
        dist_list.append(dist)
        mse_list.append(dist)

        res_dict["Model"] += [model_name] * len(dist)
        res_dict["MSE"] += dist.tolist()
        res_dict["fold"] += [f_idx] * len(dist)

    if len(error_vec_list) > 0:

        summary["model"] += [model_name] * 10
        test_df = pd.DataFrame(summary)
        test = test_df.mean()

        error_vec_concat = np.concatenate(error_vec_list, axis=0)
        error_vec_x_concat = np.concatenate(error_vec_x_list, axis=0)
        error_vec_y_concat = np.concatenate(error_vec_y_list, axis=0)

        size_vec_concat = np.concatenate(size_vec_list, axis=0)
        size_vec_x_concat = np.concatenate(size_vec_x_list, axis=0)
        size_vec_y_concat = np.concatenate(size_vec_y_list, axis=0)

        summary_single = {"Y": [], "X": [], beta_0: [], beta_1: [], "p-value": [], r_2: [], "model": [], "RSE": [], "Y_mean": []}

        evaluate_correlation(size_vec_concat, error_vec_concat, e_n, size_n,
                             summary_single)
        evaluate_correlation(size_vec_x_concat, error_vec_x_concat, e_x_n, w_n, summary_single)

        evaluate_correlation(size_vec_y_concat, error_vec_y_concat, e_y_n, h_n, summary_single)

        evaluate_correlation(size_vec_x_concat, error_vec_y_concat, e_y_n, w_n, summary_single)
        evaluate_correlation(size_vec_y_concat, error_vec_x_concat, e_x_n, h_n, summary_single)

        if plot_regression:
            regr = linear_model.LinearRegression()

            # Train the model using the training sets
            regr.fit(error_vec_concat.reshape(-1, 1), size_vec_concat.reshape(-1, 1))
            # Make predictions using the testing set
            y_val = regr.predict(error_vec_concat.reshape(-1, 1))

            plt.figure()
            plt.title("UJI (bld. 0, floor 0)")
            plt.xlabel(r'$e \quad [m]$')
            plt.ylabel(r'$w \cdot h \quad [m^2]$')

            thres_val = 75
            thres_mask = size_vec_concat > thres_val
            t_mask = np.where(error_vec_concat > 7)[0]

            red_idx = np.where(thres_mask)[0]
            blue_idx = np.where(~thres_mask)[0]

            plt.scatter(error_vec_concat[blue_idx], size_vec_concat[blue_idx], color="blue")
            plt.scatter(error_vec_concat[red_idx], size_vec_concat[red_idx], color="red")
            plt.plot(np.array([0, 20]), np.array([thres_val, thres_val]), linestyle='dashed', color="red")

            plt.plot(error_vec_concat, y_val, color="blue")

            plt.rcParams.update(plt.rcParamsDefault)
            plt.rc("savefig", dpi=200)

            pdf = PdfPages("correlation_{}.pdf".format(model_name))

            # self.fig.set_size_inches((19.79, 12.5), forward=False)
            pdf.savefig(bbox_inches="tight")
            pdf.close()

            # plt.show()

    if len(dist_list) == 0:
        return

    size_avg = np.mean(np.array(b_size_list))
    acc_avg = np.mean(np.array(acc_box_list))
    dist_conc = np.concatenate(dist_list, axis=0)

    box_dict["Model"].append(model_name)
    #box_dict["beta"].append(o)
    box_dict["fold"].append(f_idx)
    box_dict["ACC"].append(acc_avg)
    box_dict["size"].append(size_avg)
    box_dict["c-error (mean)"].append(np.mean(dist_conc))
    box_dict["c-error (median)"].append(np.median(dist_conc))
    box_dict["c-error (min)"].append(np.min(dist_conc))
    box_dict["c-error (max)"].append(np.max(dist_conc))

    if show_plot:
        plt.scatter(np.mean(np.array(b_size_list)),
                    np.mean(np.array(acc_box_list)), color=col_val, marker=marker,
                    label=plot_name)
        # plt.ylim(0.4, 1.0)
        plt.xlim(0, 1000.0)
        plt.legend(loc="lower right")
        # plt.show()

    return num_add_aug_ratio, error_vec_list, size_vec_list, summary_single


def calc_box_stats(reg_size_dict, dataset="lohan", show_plot=True, calc_aug_gain=True):

    if dataset == "lohan":
        beta_range = [7.5, 10.0, 15.0]
        if dbscan_split:
            # dbscan
            f_dnn = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/new_dropout/output/DNN-DLB_hidden_layers_{}_augmentation_{}_delta_{}"
            f_cnn = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/new_dropout/output/2D-CNN-DLB_augmentation_{}_delta_{}"
        else:
            # random
            #f_dnn = "/media/laskama/Daten/BBOX/lohan/test/output/BBOX_{}l_{}{}"
            #f_cnn = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/cnn_final/output/2D-CNN-DLB_augmentation_{}_delta_{}"
            f_dnn = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/random/output/DNN-DLB_hidden_layers_{}_augmentation_{}_delta_{}"
            f_cnn = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/random/output/2D-CNN-DLB_augmentation_{}_delta_{}"

    elif dataset == "UJI":
        beta_range = [5.0, 10.0, 15.0, 20.0]
        f_dnn = "/media/laskama/Daten/BBOX/GPU/uji/gpu/b0/f{}/new_dropout/output/DNN-DLB_hidden_layers_{}_augmentation_{}_delta_{}"
        f_cnn = "/media/laskama/Daten/BBOX/GPU/uji/gpu/b0/f{}/cnn_final/output/2D-CNN-DLB_augmentation_{}_delta_{}"

    mse_list = []

    box_dict = {"Model": [], "beta": [], "fold": [], "ACC": [],
                "size": [], "c-error (mean)": [], "c-error (median)": [],
                "c-error (min)": [], "c-error (max)": []}

    corr_dicts = {"model": [], "Y": [], "X": [], beta_0: [], beta_1: [], "p-value": [], r_2: [],
                   "RSE": [], "Y_mean": []}

    # first do DNN-DLB models
    thres_idx = 0
    for aug in [0, 1]:
        for l in [1]:#, 2]:
            for o in beta_range:

                if (o == 5.0 and dataset == "UJI") or (o == 5.0 and dataset == "lohan" and dbscan_split and aug == 0):
                    box_dict['beta'] += [o]
                else:
                    box_dict['beta'] += [o] * 2

                # DNN
                if dataset == "lohan":
                    #if dbscan_split:
                    pipe_name = f_dnn.format(l, aug, o) + "_{}"
                    #else:
                    #    aug_sup = "_aug" if aug == 1 else ""
                    #    pipe_name = f_dnn.format(l, int(o), aug_sup) + "_{}"
                else:
                    pipe_name = f_dnn.format(floor, l, aug, o) + "_{}"
                model_name = "DNN-DLB ({} HL, {} aug)".format(l, aug)
                if o == beta_range[2]:
                    plot_name = model_name
                else:
                    plot_name = ""
                col_val = "r" if aug == 1 else "g"
                marker = "^" if l == 2 else "v"
                num_add_aug_ratio, e_vec, s_vec, corr_dict = _calc_box_stats(reg_size_dict, box_dict, mse_list, pipe_name, model_name, plot_name, col_val, marker, num_avg=10, calc_aug_gain=calc_aug_gain, thres_idx=-1)
                thres_idx += 1

                corr_dicts["Y"] += corr_dict["Y"]
                corr_dicts["X"] += corr_dict["X"]
                corr_dicts["p-value"] += corr_dict["p-value"]
                corr_dicts[r_2] += corr_dict[r_2]
                corr_dicts[beta_0] += corr_dict[beta_0]
                corr_dicts[beta_1] += corr_dict[beta_1]
                corr_dicts["RSE"] += corr_dict["RSE"]
                corr_dicts["Y_mean"] += corr_dict["Y_mean"]
                #corr_dicts["type"] += corr_dict["type"]
                corr_dicts["model"] += [model_name + str(o)] * 5

                # CNN
                if dataset == "lohan":
                    pipe_name = f_cnn.format(aug, o) + "_{}"
                else:
                    pipe_name = f_cnn.format(floor, aug, o) + "_{}"
                model_name = "2D-CNN-DLB ({} aug)".format(aug)
                if o == beta_range[2]:
                    plot_name = model_name
                else:
                    plot_name = ""
                col_val = "r" if aug == 1 else "g"
                marker = "o"
                _calc_box_stats(reg_size_dict, box_dict, mse_list, pipe_name, model_name, plot_name, col_val, marker, num_avg=10, calc_aug_gain=False, plot_regression=False)


    # error-size correlation
    corr_df = pd.DataFrame(corr_dicts)
    corr_df[beta_0] = corr_df[beta_0].round(2)
    corr_df[beta_1] = corr_df[beta_1].round(2)
    corr_df[r_2] = corr_df[r_2].round(2)

    print(corr_df.to_latex(escape=False, index=False))

    box_df = None

    # print("BBOX_MSE: {}".format(np.mean(np.array(mse_list))))

    if calc_aug_gain:

        box_df = pd.DataFrame(box_dict)

        box_dict["best_reg"] = []
        box_dict["ACC-gain"] = []
        box_dict["size-gain"] = []

        # calc ACC gain to each DNN
        for _, model in box_df.iterrows():
            b_acc = model["ACC"]
            b_size = model["size"]

            min_gain = 100.0
            # find corresponding two values in DNN
            for reg_model, reg_size, reg_acc in zip(reg_size_dict["Model"],
                                                    reg_size_dict["size"],
                                                    reg_size_dict["ACC"]):

                # size on x-axis
                test = np.argmax(reg_size > b_size)

                x = np.array([reg_size[test - 1], reg_size[test]])
                y = np.array([reg_acc[test - 1], reg_acc[test]])

                f = interp1d(x, y)
                reg_acc_int = f(b_size)
                gain = b_acc - reg_acc_int

                # acc on x-axis
                test = np.argmax(reg_acc > b_acc)

                x = np.array([reg_acc[test - 1], reg_acc[test]])
                y = np.array([reg_size[test - 1], reg_size[test]])

                f = interp1d(x, y)
                reg_size_int = f(b_acc)
                size_gain = b_size - reg_size_int

                if gain < min_gain:
                    min_gain = gain
                    min_size_gain = size_gain
                    min_model = reg_model

            box_dict["ACC-gain"].append(min_gain)
            box_dict["size-gain"].append(min_size_gain)
            box_dict["best_reg"].append(min_model)

        box_df = pd.DataFrame(box_dict)

    # print("ACC-gain: {}".format(box_df["ACC-gain"].mean()))

    return box_df, num_add_aug_ratio, corr_df


def main(dataset="UJI"):

    plt.figure()
    if dataset == "lohan":
        plt.title("Tampere (floor 1)")  # (fold-{})".format(f_idx + 1)
    else:
        plt.title("UJI (bld. 0, floor {})".format(floor))
    reg_size_dict = {"Model": [], "size": [], "ACC": []}

    #for l in [1]:
    calc_reg_stats(reg_size_dict, num_layers=3, dataset=dataset)

    box_df, num_add_aug_ratio, corr_df = calc_box_stats(reg_size_dict, dataset=dataset, calc_aug_gain=True)

    plt.rcParams.update(plt.rcParamsDefault)
    plt.rc("savefig", dpi=200)

    pdf = PdfPages("{}_metric_{}.pdf".format(dataset, floor))

    # self.fig.set_size_inches((19.79, 12.5), forward=False)
    pdf.savefig(bbox_inches="tight")
    pdf.close()
    # plt.show()

    return box_df, num_add_aug_ratio, corr_df

def rename_files():
    import glob
    import os

    a_0 = "augmentation_0"
    a_1 = "augmentation_1"

    #for floor in range(4):
    for file in glob.glob('/media/laskama/Daten/BBOX/GPU/lohan/gpu/new_dropout/output/2D-CNN-DLB_delta_7.5_*'):
        old_file = file

        file = file.replace("_delta_7.5", "")

        if a_0 in file:
            substr = a_0
        elif a_1 in file:
            substr = a_1

        inserttxt = "_delta_7.5"

        idx = file.index(substr) + len(substr)
        file = file[:idx] + inserttxt + file[idx:]
        print(old_file)
        print(file)
        print("-----------------")

        #os.rename(old_file, file)
        #print("_______________")
        #print(file.replace("_augmentation_1", "", 1))


if __name__ == "__main__":
    #rename_files()

    res_dict = {"Model": [], "MSE": [], "fold": []}
    num_add_aug_ratio_list = []
    # dataset = "UJI"
    f_idx = None
    floor = None
    box_df_concat = None

    if dataset == "UJI":
        corr_dfs = []
        for idx in range(4):
            floor = idx

            box_df, num_add_aug, corr_df = main(dataset)
            num_add_aug_ratio_list.append(num_add_aug)
            corr_dfs.append(corr_df)

            if box_df_concat is None:
                box_df_concat = box_df
            else:
                box_df_concat = pd.concat([box_df_concat, box_df],
                                          ignore_index=True)

    elif dataset == "lohan":
        for idx in [0]:#[0,1,2,3,4]:
            f_idx = None #idx
            floor = idx

            box_df, num_add_aug, corr_df = main(dataset)
            num_add_aug_ratio_list.append(num_add_aug)

            if box_df_concat is None:
                box_df_concat = box_df
            else:
                box_df_concat = pd.concat([box_df_concat, box_df],
                                          ignore_index=True)

    box_df_concat = box_df_concat[~box_df_concat["Model"].str.contains("CNN")]

    print("SIZE-GAIN (aug): {}".format(box_df_concat[box_df_concat["Model"].str.contains("1 aug")]["size-gain"].mean()))
    print("SIZE-GAIN (no-aug): {}".format(box_df_concat[box_df_concat["Model"].str.contains("0 aug")]["size-gain"].mean()))
    print("ACC-GAIN (aug): {}".format(box_df_concat[box_df_concat["Model"].str.contains("1 aug")]["ACC-gain"].mean()))
    print("ACC-GAIN (no-aug): {}".format(box_df_concat[box_df_concat["Model"].str.contains("0 aug")]["ACC-gain"].mean()))
    print("NUM-ADD_AUG_RATIO: {}".format(np.mean(num_add_aug_ratio_list)))

    # plt.show()

    df = pd.DataFrame(res_dict)

    # print("MEAN-MSE: {}".format(df[df["Model"].str.contains("1 aug")]["MSE"].mean()))
    # print("MEDIAN-MSE: {}".format(
    #     df[df["Model"].str.contains("1 aug")]["MSE"].median()))
    # print("MIN-MSE: {}".format(
    #     df[df["Model"].str.contains("1 aug")]["MSE"].min()))
    # print("MAX-MSE: {}".format(
    #     df[df["Model"].str.contains("1 aug")]["MSE"].max()))



    #print(df[df["Model"].str.contains("1 aug")]["MSE"].describe())

    # df2 = pd.DataFrame(
    #     group.describe().rename(columns={'MSE': name}).squeeze()
    #     for name, group in df.groupby('Model'))
    # print(df)

    if dataset == "UJI":
        print(df[df["Model"].isin(
            ["DNN-DLB (1 HL, 1 aug)", "DNN -> O (3 HL)", "CNN -> O",
             "2D-CNN-DLB (1 aug)"])].groupby('Model')["MSE"].describe().unstack(
            1))
    if dataset == "lohan":
        print(df[df["Model"].isin(
            ["DNN-DLB (1 HL, 1 aug)", "DNN -> O (3 HL)", "CNN -> O",
             "2D-CNN-DLB (1 aug)"])].groupby('Model')["MSE"].describe().unstack(
            1))

    # BOX plot
    fig = plt.figure()
    sns.boxplot("Model", "MSE", data=df, color="skyblue")

    plt.rcParams.update(plt.rcParamsDefault)
    plt.rc("savefig", dpi=200)

    pdf = PdfPages("{}_boxplot.pdf".format(dataset))

    fig.set_size_inches(fig.get_size_inches()*[1.6, 1.3], forward=False)

    pdf.savefig(bbox_inches="tight")
    pdf.close()

    # BOX plot zoom
    fig = plt.figure()
    sns.boxplot("Model", "MSE", data=df, color="skyblue")

    plt.rcParams.update(plt.rcParamsDefault)
    plt.rc("savefig", dpi=200)

    pdf = PdfPages("{}_boxplot_zoom.pdf".format(dataset))

    plt.ylim(-0.5, 20.0)
    fig.set_size_inches(fig.get_size_inches() * [1.6, 1.3], forward=False)

    pdf.savefig(bbox_inches="tight")
    pdf.close()

    # plt.show()
