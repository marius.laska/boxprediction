import numpy as np
from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from pkg_resources import resource_filename

from base.floor_plan_plot import FloorPlanPlotRec


def main(height=80, width=160, grid_size=40, padding_ratio=0.1):
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    linewidth = 2
    fp = FloorPlanPlotRec((200, 80), 2, floorplan_bg_img=img, filename="grid_encoding.pdf")

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size) + 1
    num_col_l1 = np.ceil(width / grid_size) + 1
    num_l1 = int(num_row_l1 * num_col_l1)

    num = num_l1

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # 1st layer
        r_idx = int(idx / num_col_l1)
        c_idx = idx % num_col_l1

        x = c_idx * grid_size
        y = r_idx * grid_size

        origins.append(np.array([x, y]))
        fp.axis.text(x + 2, y - 2.5, str(idx))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    o_plt = np.array([[o[0], o[1], 40, 40] for o in origins])
    o_plt_pad = np.array([[o[0], o[1], 48, 48] for o in origins])

    origins = np.array(origins)

    fp.draw_rectangles_new(o_plt, color="black")
    fp.draw_rectangles_new(o_plt_pad, color="grey")

    fp.draw_rectangles_new(o_plt[7], color="green")
    fp.draw_rectangles_new(o_plt_pad[[1,2,6]], color="blue")

    fp.draw_points(origins[:, 0], origins[:, 1], color="black")
    fp.draw_points([62], [21], color="green")

    o_vals = np.array([7, 1, 2, 6])
    test = (np.array([[62, 21]]) - origins[o_vals, :]) / (
                grid_size / 2.0 + grid_size * padding_ratio)
    print(np.concatenate((test, o_vals.reshape(-1, 1)), axis=1))

    fp.save_plot(bbox_inches="tight")
    fp.show_plot()


if __name__ == "__main__":
    main()
