from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable

import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pkg_resources import resource_filename
import matplotlib.pyplot as plt

from analysis.visualize_learning_progress import \
    convert_from_2dim_overlapping_grid
from base.floor_plan_plot import FloorPlanPlotRec


def pred_plot(pipe: Pipeline, dataset="lohan"):

    if dataset == "lohan":
        fp_dims = (200, 80)
        vis_idx = (0, 250)
        img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
        filename_pred = "pred_lohan.pdf"
    elif dataset == "UJI":
        fp_dims = (200, 80)
        vis_idx = (0, 207)
        img = "/home/laskama/Dropbox/PhD/BboxPaper/images/blank_bg.png"
        filename_pred = "pred_UJI.pdf"

    y_pred_box = pipe.summary.y_pred[0] #np.concatenate(pipe.summary.y_pred, axis=0)
    y_true = pipe.summary.y_true_labels[0] # np.concatenate(pipe.summary.y_true_labels, axis=0)

    fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img, filename=filename_pred)

    # plot training data
    x_train, y_train = pipe.data_provider.get_train_data(area_labels=False,
                                                          split_idx=0)
    fp.draw_points(y_train[:, 0], y_train[:, 1], color="green", alpha=0.5)

    idx_len = vis_idx[1] - vis_idx[0]

    sub_sample_mask = np.random.choice(np.arange(vis_idx[0], vis_idx[1]),
                                       int(idx_len * 0.7), replace=False)

    for idx in range(idx_len):  # len(y_true)):
        idx += vis_idx[0]
        if idx not in sub_sample_mask:
            continue

        fp.draw_rectangles_new(anchors=y_pred_box[idx, :], color='black')

        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='red', alpha=0.5)

    if dataset == "lohan":
        fp.axis.set_xlim(0, 182)
    elif dataset == "UJI":
        fp.axis.set_xlim(0, 125)
    fp.save_plot(bbox_inches="tight")

    fp.show_plot()


def size_plot(pipe: Pipeline, dataset="lohan"):

    if dataset == "lohan":
        fp_dims = (200, 80)
        vis_idx = (0, 250)
        img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
        size_filename = "size_lohan.pdf"
    elif dataset == "UJI":
        fp_dims = (200, 80)
        vis_idx = (0, 250)
        img = "/home/laskama/Dropbox/PhD/BboxPaper/images/blank_bg.png"
        size_filename = "size_UJI.pdf"

    y_pred_box = pipe.summary.y_pred[0] # np.concatenate(pipe.summary.y_pred, axis=0)
    y_true = pipe.summary.y_true_labels[0] # np.concatenate(pipe.summary.y_true_labels, axis=0)

    knn_error_x = np.abs(y_true - y_pred_box[:, :2])[:, 0]
    knn_error_y = np.abs(y_true - y_pred_box[:, :2])[:, 1]
    box_size = np.prod(y_pred_box[:, 2:], axis=1)

    coords = [(l[0], l[1]) for l in y_true]

    df = pd.DataFrame(list(
        zip(y_pred_box[:, 0], y_pred_box[:, 1], knn_error_x, knn_error_y,
            box_size)), columns=['x', 'y', 'error_x', 'error_y', 'box_size'])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    s = ax.scatter(df.x, df.y, c=df.box_size)

    # df.plot.scatter(x='x', y='y', c='box_size', colormap='viridis')
    # ax = plt.gca()
    ax.set_aspect('equal')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb = plt.colorbar(s, cax=cax)
    cb.set_label('Box size [m²]')

    plt.rcParams.update(plt.rcParamsDefault)
    plt.rc("savefig", dpi=200)

    pdf = PdfPages(size_filename)

    if dataset == "lohan":
        ax.set_xlim(0, 182)
        ax.set_yticks([0, 20, 40, 60, 80])
    # self.fig.set_size_inches((19.79, 12.5), forward=False)
    pdf.savefig(bbox_inches="tight")
    pdf.close()

    plt.show()


if __name__ == "__main__":
    # f1 = "/media/laskama/Daten/BBOX/UJIndoorLoc/b0/f1/test/output/BBOX_1l_20_aug_1"
    f1 = "/media/laskama/Daten/BBOX/lohan/test/output/BBOX_1l_10_aug_1"
    dataset = "lohan"

    pipe: Pipeline = Storable.load(f1)

    convert_from_2dim_overlapping_grid(pipe,
                                       grid_size=pipe.data_provider.grid_size,
                                       quantile=False,
                                       store=True,
                                       average_samples=True)

    pred_plot(pipe, dataset)
    size_plot(pipe, dataset)