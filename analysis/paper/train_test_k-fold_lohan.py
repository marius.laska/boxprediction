from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from pkg_resources import resource_filename

from base.floor_plan_plot import FloorPlanPlotRec


def main():
    file = "/home/laskama/Dropbox/evaluation/lohan/REG_1"
    file = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/lohan/gpu/cnn_final/output/2D-CNN_1"
    file = "/media/laskama/Daten/BBOX/GPU/lohan/gpu/new_dropout/output/DNN-DLB_hidden_layers_1_augmentation_0_delta_10.0_1"

    p: Pipeline = Storable.load(file)

    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')

    fp_dims = (200, 80)  # (200, 80)

    for split_idx in range(p.data_provider.num_splits):
        _, y_train = p.data_provider.get_train_data(split_idx=split_idx, area_labels=False)
        _, y_test = p.data_provider.get_test_data(split_idx=split_idx, area_labels=False)

        fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, filename="lohan-k_fold_{}.pdf".format(split_idx+1))

        fp.draw_points(y_train[:, 0], y_train[:, 1],
                       color="green", alpha=0.5)

        fp.draw_points(y_test[:, 0], y_test[:, 1],
                       color="red", alpha=0.5)
        #fp.draw_measurement_distribution(p.data_provider.labels, alpha=0.5)

        #fp.axis.set_frame_on(False)
        fp.axis.set_xlim(0, 180)
        #fp.fig.tight_layout()
        fp.axis.axis("off")
        fp.save_plot(bbox_inches="tight")
        #fp.show_plot()

    fp = FloorPlanPlotRec(fp_dims, 6, floorplan_bg_img=img,
                          filename="lohan-k_fold_heatmap.pdf")
    fp.draw_measurement_distribution(p.data_provider.labels, alpha=0.7)
    fp.axis.axis("off")
    fp.save_plot(bbox_inches="tight")
    #fp.show_plot()

    # plot_data_heatmap(pipe, floor_plotter=fp)


if __name__ == "__main__":
    main()