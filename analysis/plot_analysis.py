from il_pipeline.utility.storable import Storable
from il_pipeline.pipeline import Pipeline
from main import calc_acc_c, center_diff
import numpy as np
import math
from shapely.geometry import box

PI = 3.14159265359

def area_size_LDCE():
    pipe: Pipeline = Storable.load("/home/laskama/Dropbox/PhD/evaluation/revision/lohan_CDF/fine/output/DNN(deep)")

    areas = pipe.clusterer.areas

    sizes = np.array([poly.area for poly in areas])
    bbox_size = np.array([box(*poly.bounds).area for poly in areas])

    pred_areas = np.argmax(np.concatenate(pipe.summary.y_pred, axis=0), axis=1)
    pred_sizes = [sizes[idx] for idx in pred_areas]
    pred_sizes_bbox = [bbox_size[idx] for idx in pred_areas]

    print(np.mean(sizes))
    print(np.mean(bbox_size))

    print(np.mean(pred_sizes))
    print(np.mean(pred_sizes_bbox))


def plot():
    box_pipe: Pipeline = Storable.load("evaluation/UJIndoorLoc/box/b1/f1/output/BBOX")
    point_pipe: Pipeline = Storable.load("evaluation/UJIndoorLoc/box/b1/f1/output/REG")

    box_acc, _, _ = calc_acc_c(np.concatenate(box_pipe.summary.y_true_labels, axis=0), np.concatenate(box_pipe.summary.y_pred, axis=0))
    box_center_error = center_diff(np.concatenate(box_pipe.summary.y_true_labels, axis=0), np.concatenate(box_pipe.summary.y_pred, axis=0))

    print(box_acc)
    print(box_center_error)
    print(get_avg_box_size(box_pipe))

    point_error = np.mean(np.linalg.norm(np.concatenate(point_pipe.summary.y_true_labels, axis=0) - np.concatenate(point_pipe.summary.y_pred, axis=0), axis=1))
    print(point_error)
    radius = find_radius_for_acc(point_pipe, accuracy=0.8)
    print(PI * math.pow(radius, 2))


def get_radius_acc(model: Pipeline, radius):
    y_true = np.concatenate(model.summary.y_true_labels, axis=0)
    y_pred = np.concatenate(model.summary.y_pred, axis=0)

    dist = np.linalg.norm(y_true - y_pred, axis=1)
    inside = np.where(dist < radius)[0]

    return len(inside) / len(y_true)

# def get_radius_acc_train(model: Pipeline, radius):
#     y_true = np.concatenate(model.summary.y_true_labels, axis=0)
#     y_pred = np.concatenate(model.summary.y_pred, axis=0)
#
#     _, y_train = model.data_provider.get_train_data(split_idx=0)
#     _, y_pred = model.
#
#     dist = np.linalg.norm(y_true - y_pred, axis=1)
#     inside = np.where(dist < radius)[0]
#
#     return len(inside) / len(y_true)


def get_avg_box_size(model: Pipeline):
    y_pred = np.concatenate(model.summary.y_pred, axis=0)

    return np.mean(y_pred[:, 2] * y_pred[:, 3])


def find_acc_of_area(model: Pipeline, area):
    r = np.sqrt(area/PI)
    acc = get_radius_acc(model, r)

    return acc

def find_radius_for_acc(model: Pipeline, step_size=.0001, accuracy=1):
    acc = 0
    radius = 0
    while acc < accuracy:
        radius += step_size
        acc = get_radius_acc(model, radius)
    print("Radius: {}, ACC: {}".format(radius, acc))

    return radius


if __name__ == "__main__":
    # pipe: Pipeline = Storable.load("evaluation/UJIndoorLoc/box/output/REG")
    # print(np.mean(np.linalg.norm(np.concatenate(pipe.summary.y_true_labels,
    #                                       axis=0) - np.concatenate(
    #     pipe.summary.y_pred, axis=0), axis=1)))
    #
    # find_radius_for_acc(pipe, accuracy=0.76)

    #area_size_LDCE()
    plot()