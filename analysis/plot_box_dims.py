from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np

from pkg_resources import resource_filename

from main import calc_acc_c, center_diff
from analysis.visualize_quantile import get_acc_and_size


def main():

    pipe: Pipeline = Storable.load("evaluation/lohan/scale/output/BBOX")