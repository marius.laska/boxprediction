from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable

import numpy as np
from pkg_resources import resource_filename

from analysis.evaluate_circle import PI
from analysis.visualize_box_overlap import normalize
import matplotlib.pyplot as plt

from analysis.visualize_learning_progress import convert_from_grid, \
    convert_from_2dim_grid, convert_from_2dim_overlapping_grid
from base.floor_plan_plot import FloorPlanPlotRec
from analysis.full_visual_cmp import transform_quantile_to_bbox

def main():
    p_box: Pipeline = Storable.load("../evaluation/lohan/evaluation/grid_q/output/QUANTILE_2")
    p_box: Pipeline = Storable.load("../evaluation/lohan/evaluation/overlap/output/{}".format("BBOX_aug2"))

    dp = p_box.data_provider
    fp_width = dp.floorplan_width
    fp_height = dp.floorplan_height

    split_idx = 1
    radius = 10.069
    sig_diff_threshold = 2.0
    pos_diff_threshold = 100

    x_test, y_true = dp.get_test_data(split_idx=split_idx, area_labels=False)
    x_train, y_train = dp.get_train_data(split_idx=split_idx, area_labels=False)

    # convert_from_grid(p_box)
    #convert_from_2dim_grid(p_box, grid_size=40, quantile=True)
    #transform_quantile_to_bbox(p_box)
    convert_from_2dim_overlapping_grid(p_box, grid_size=40, quantile=False,
                                       store=True,
                                       average_samples=True)

    y_pred = p_box.summary.y_pred[
        split_idx]  # np.concatenate(p_box.summary.y_pred[split_idx], axis=0)
    #y_pred_reg = p_reg.summary.y_pred[split_idx]

    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    # fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    for idx in range(len(y_pred)):

        sig_diff = np.linalg.norm(x_test - x_test[idx, :], axis=1)
        pos_diff = np.linalg.norm(y_true - y_true[idx, :], axis=1)
        sig_mask = sig_diff < sig_diff_threshold
        pos_mask = pos_diff < pos_diff_threshold

        mask = np.logical_and(sig_mask, pos_mask)
        mask = np.where(mask)[0]

        sub_pred = y_pred[mask, :]
        sub_true = y_true[mask, :]

        # color with alpha depending on sig_diff
        pred_colors = np.zeros((len(mask), 4))
        # for red the first column needs to be one
        pred_colors[:, 0] = 1.0
        pred_colors[:, 3] = 1 - sig_diff[mask]/sig_diff_threshold

        true_colors = np.zeros((len(mask), 4))
        true_colors[:, 2] = 1.0
        true_colors[:, 3] = 1 - sig_diff[mask]/sig_diff_threshold

        fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="Error")

        fp.draw_rectangles_new(anchors=y_pred[idx, :], color='black')
        fp.draw_points(sub_pred[:, 0], sub_pred[:, 1], color=pred_colors, alpha=None)
        fp.draw_points(sub_true[:, 0], sub_true[:, 1], color=true_colors, alpha=None)
        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='green')

        fp.show_plot()

if __name__ == "__main__":
    main()