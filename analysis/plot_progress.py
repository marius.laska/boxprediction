from il_pipeline.utility.storable import Storable
import numpy as np

from base.bbox_pipeline import BboxPipeline
from base.floor_plan_plot import FloorPlanPlotRec
from main import calc_acc_c, center_diff
from analysis.plot_analysis import get_avg_box_size
from il_pipeline.plotting.pipeline_plots import plot_data_heatmap


def main():
    #file = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/lohan_pretrain/output/BBOX"
    #file = "/home/laskama/PycharmProjects/bboxPrediction/evaluation/gia/pretrain/output/BBOX"
    file = "evaluation/gia/evaluation/box/no_pre/output/BBOX"
    file = "evaluation/UJIndoorLoc/box/output/BBOX"
    #file = "evaluation/UJIndoorLoc/box/b1/f1/output/BBOX"

    for epoch in [1]:#,2,3,4,5]:

        pipe: BboxPipeline = Storable.load(file)#.format(epoch))
        #img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
        img = "evaluation/gia/gia_floor_4.jpg"

        # setup floor plan plot

        y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)
        y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
        acc, wrong_mask, _ = calc_acc_c(y_true, y_pred)
        print("acc: {}".format(acc))
        print("center-dist: {}".format(center_diff(y_true, y_pred)))
        print("avg_center: {}".format(np.mean(y_pred[:, :2], axis=0)))
        print("avg_box_size: {}".format(get_avg_box_size(pipe)))
        print("std-w: {}".format(np.std(pipe.summary.y_pred[0][:, 2])))
        print("std-h: {}".format(np.std(pipe.summary.y_pred[0][:, 3])))
        print("mean-w: {}".format(np.mean(pipe.summary.y_pred[0][:, 2])))
        print("mean-h: {}".format(np.mean(pipe.summary.y_pred[0][:, 3])))

        # for idx in range(10):
        max_el = len(y_true)
        # fp = FloorPlanPlotRec((83.32, 17.16), 2, floorplan_bg_img=img)
        # fp = FloorPlanPlotRec((200, 80), 4, floorplan_bg_img=img)

        # for split_idx in range(pipe.data_provider.num_splits):
        #     fp = FloorPlanPlotRec((83.32, 17.16), 2, floorplan_bg_img=img)
        #     _, t_labels = pipe.data_provider.get_test_data(area_labels=False, split_idx=split_idx)
        #     _, train_labels = pipe.data_provider.get_train_data(area_labels=False, split_idx=split_idx)
        #     fp.draw_points(train_labels[:, 0], train_labels[:, 1], color='g', alpha=0.5)
        #     fp.draw_points(t_labels[:, 0], t_labels[:, 1], color='r', alpha=0.5)
        #
        #     fp.show_plot()

        # for members in pipe.clusterer.base_clusters:
        #     fp = FloorPlanPlotRec((83.32, 17.16), 2, floorplan_bg_img=img)
        #     fp.draw_points(members[:, 0], members[:, 1], color='g',
        #                    alpha=0.5)
        #     fp.show_plot()


        fp = FloorPlanPlotRec((83.32, 17.16), 20, floorplan_bg_img=img)
        plot_data_heatmap(pipe, floor_plotter=fp)
        for idx in range(max_el):
            idx += 0
            # fp = FloorPlanPlotRec((83.32, 17.16), 2, floorplan_bg_img=img)
            fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='g', alpha=0.5)

            # box_size = y_pred[:]

            fp.draw_rectangles_new(y_pred[idx, :])

        fp.show_plot()


if __name__ == "__main__":
    main()
