import math

from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from pkg_resources import resource_filename

from analysis.visualize_learning_progress import \
    convert_from_2dim_overlapping_rotated_grid, \
    convert_grid_point_to_global_coord_system
from base.floor_plan_plot import FloorPlanPlotRec
import numpy as np
from shapely.geometry import Polygon
from base.custom_loss import compute_tf_loss, compute_tf_all_wall_loss


def compute_loss(y_true, y_pred, walls_h, walls_v):

    test = compute_tf_loss(y_true, y_pred, walls_h, walls_v)

def main_all_walls():
    f_idx = 2

    p: Pipeline = Storable.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/rot/output/BBOX_1l_20_modular_v3_1")

    p.data_provider.transform_to_2dim_overlapping_grid_encoding(
        grid_size=40, labels=p.summary.y_true_labels[f_idx])

    y_true_enc = p.data_provider.grid_labels
    y_pred_enc = p.summary.y_pred[f_idx]

    walls_h_np = np.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/analysis/hor_walls.npy")
    walls_v_np = np.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/analysis/ver_walls.npy")

    p.data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
        grid_size=40, horizontal=True, angle_only=True, overlap_strategy='keep',
        walls=walls_h_np)

    p.data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
        grid_size=40, horizontal=False, angle_only=True, overlap_strategy='keep',
        walls=walls_v_np)

    #loss = compute_tf_all_wall_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h,
    #                       p.data_provider.grid_walls_v, modus="loss")
    # l_angle_diff = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
    #                                p.data_provider.grid_walls_h,
    #                                p.data_provider.grid_walls_v,
    #                                modus="angle_diff")
    # l_angle_diff_scale = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
    #                                               p.data_provider.grid_walls_h,
    #                                               p.data_provider.grid_walls_v,
    #                                               modus="angle_diff_scale")
    # l_angle_diff_loss = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
    #                                               p.data_provider.grid_walls_h,
    #                                               p.data_provider.grid_walls_v,
    #                                               modus="angle_diff_loss")
    # dist_walls = compute_tf_all_wall_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h,
    #                       p.data_provider.grid_walls_v, modus="dist_walls")
    proj_h = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                             p.data_provider.grid_walls_h,
                             p.data_provider.grid_walls_v, modus="proj_h")
    proj_v = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                      p.data_provider.grid_walls_h,
                                      p.data_provider.grid_walls_v,
                                      modus="proj_v")

    walls_h = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                          p.data_provider.grid_walls_h,
                                          p.data_provider.grid_walls_v,
                                          modus="walls_h")

    walls_v = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                       p.data_provider.grid_walls_h,
                                       p.data_provider.grid_walls_v,
                                       modus="walls_v")

    print("test")


    #proj_v = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
    #                         p.data_provider.grid_walls_h,
    #                         p.data_provider.grid_walls_v, modus="proj_v")
    l_angle_diff_scale = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                         p.data_provider.grid_walls_h,
                                         p.data_provider.grid_walls_v,
                                         modus="angle_diff_scale")
    # l_angle_diff = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="angle_diff")
    l_d_hor_pos = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                  p.data_provider.grid_walls_h,
                                  p.data_provider.grid_walls_v,
                                  modus="d_hor_pos")
    l_d_hor_neg = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                  p.data_provider.grid_walls_h,
                                  p.data_provider.grid_walls_v,
                                  modus="d_hor_neg")
    l_d_ver_pos = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                           p.data_provider.grid_walls_h,
                                           p.data_provider.grid_walls_v,
                                           modus="d_ver_pos")
    l_d_ver_neg = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                           p.data_provider.grid_walls_h,
                                           p.data_provider.grid_walls_v,
                                           modus="d_ver_neg")

    # l_d_ver_pos = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
    #                               p.data_provider.grid_walls_h,
    #                               p.data_provider.grid_walls_v,
    #                               modus="d_ver_pos")
    # l_d_ver_neg = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
    #                               p.data_provider.grid_walls_h,
    #                               p.data_provider.grid_walls_v,
    #                               modus="d_ver_neg")

    grid_cell_pred = convert_from_2dim_overlapping_rotated_grid(p, grid_size=40,
                                                                quantile=False,
                                                                store=True,
                                                                average_samples=True)

    y_pred_box = np.concatenate(p.summary.y_pred, axis=0)
    y_pred_box = p.summary.y_pred[f_idx]  # [p1:p2, :]
    y_true = p.summary.y_true_labels[f_idx]

    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    fp_dims = (200, 80)
    # fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img, walls=walls_h[w1:w2, :])

    for idx in range(200):  # len(y_true)):

        # check diff in angle

        cell_idx = int(grid_cell_pred[f_idx][idx, 0])
        #cell_idx = int(y_true_enc[idx, 2])

        projection_points_h = proj_h[idx, :, :]
        projection_points_v = proj_v[idx, :, :]

        projection_points_h = convert_grid_point_to_global_coord_system(
            projection_points_h, cell_idx,
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)
        projection_points_v = convert_grid_point_to_global_coord_system(
            projection_points_v, cell_idx,
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        walls_h_1 = convert_grid_point_to_global_coord_system(
            walls_h[idx, :, :2], cell_idx,
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        walls_h_2 = convert_grid_point_to_global_coord_system(
            walls_h[idx, :, 2:4], cell_idx,
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        walls_v_1 = convert_grid_point_to_global_coord_system(
            walls_v[idx, :, :2], cell_idx,
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        walls_v_2 = convert_grid_point_to_global_coord_system(
            walls_v[idx, :, 2:4], cell_idx,
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        h_pos_idx = np.argmin(np.abs(l_d_hor_pos[idx, :]))
        h_neg_idx = np.argmin(np.abs(l_d_hor_neg[idx, :]))
        h_idx = []
        if np.abs(l_d_hor_pos[idx, h_pos_idx]) < 100.0:
            h_idx.append(h_pos_idx)
        if np.abs(l_d_hor_neg[idx, h_neg_idx]) < 100.0:
            h_idx.append(h_neg_idx)

        v_pos_idx = np.argmin(np.abs(l_d_ver_pos[idx, :]))
        v_neg_idx = np.argmin(np.abs(l_d_ver_neg[idx, :]))
        v_idx = []
        if np.abs(l_d_ver_pos[idx, v_pos_idx]) < 100.0:
            v_idx.append(v_pos_idx)
        if np.abs(l_d_ver_neg[idx, v_neg_idx]) < 100.0:
            v_idx.append(v_neg_idx)

        fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img,
                              walls=np.concatenate([walls_h_np, walls_v_np], axis=0))

        poly_1 = get_rotated_polygon(*y_pred_box[idx, :])
        fp.draw_polygons([poly_1])

        for w_idx in range(len(walls_h_1)):
            if w_idx not in h_idx:
                continue

            draw_box_with_nearest_walls(fp, walls_h_1, walls_h_2, idx,
                                        w_idx, projection_points_h, y_pred_box, color="purple")

        for w_idx in range(len(walls_v_1)):
            if w_idx not in v_idx:
                continue

            draw_box_with_nearest_walls(fp, walls_v_1, walls_v_2, idx,
                                        w_idx, projection_points_v, y_pred_box, hor=False, color="orange")

        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color="green")

        fp.show_plot()

        # h_w_idx_pos = np.where(pos_mask_h[idx, :])[0]

        # fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img,
        #                       walls=walls_h)  # [w1:w2, :])
        #
        # fp.draw_rectangles_new(anchors=y_pred_box[idx, :], color='black')
        #
        # fp.draw_lines(np.array([walls_h_1[:, 0], walls_h_2[:, 0]]), np.array([walls_h_1[:, 1], walls_h_2[:, 1]]), color="orange")
        # fp.draw_lines(np.array([walls_1[:, 0], walls_2[:, 0]]),
        #               np.array([walls_1[:, 1], walls_2[:, 1]]),
        #               color="purple")
        #
        #
        # fp.draw_points(y_true[idx, 0], y_true[idx, 1])
        #
        # # draw projection points
        # fp.draw_points(projection_points[:, 0], projection_points[:, 1],
        #                color="red")


        # fp.show_plot()

def draw_box_with_nearest_walls(fp, w1, w2, idx, w_idx, proj_points, y_pred, color="red", hor=True):
    m = (w2[w_idx, 1] - w1[w_idx, 1]) / (
            w2[w_idx, 0] - w1[w_idx, 0])
    n = w1[w_idx, 1] - m * w1[w_idx, 0]

    if hor:
        left = m * 0 + n
        right = m * 200 + n

        fp.draw_lines(np.array([0, 200]),
                      np.array([left, right]),
                      color=color)

        fp.draw_lines(np.array([w1[w_idx, 0], w2[w_idx, 0]]),
                      np.array([w1[w_idx, 1], w2[w_idx, 1]]),
                      color=color)

    fp.draw_lines(np.array([proj_points[w_idx, 0], y_pred[idx, 0]]),
                  np.array(
                      [proj_points[w_idx, 1], y_pred[idx, 1]]),
                  color=color)

    fp.draw_points(proj_points[w_idx, 0], proj_points[w_idx, 1],
                   color=color)
    fp.draw_points(y_pred[idx, 0], y_pred[idx, 1])

def main():
    p1 = 80
    p2 = 86

    w1 = 48
    w2 = 50

    f_idx = 2

    p: Pipeline = Storable.load("/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/rot/output/BBOX_1l_20_base_1")

    p.data_provider.transform_to_2dim_overlapping_grid_encoding(
                        grid_size=40, labels=p.summary.y_true_labels[f_idx])

    y_true_enc = p.data_provider.grid_labels
    y_pred_enc = p.summary.y_pred[f_idx]

    walls_h = np.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/analysis/hor_walls.npy")
    walls_v = np.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/analysis/ver_walls.npy")

    p.data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
        grid_size=40, horizontal=True,
        walls=walls_h)

    p.data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
        grid_size=40, horizontal=False,
        walls=walls_v)

    walls_h = np.array(
        [walls_h[idx, :] for idx in range(len(walls_h)) if idx != 145])

    #w_h = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="walls_h")
    #s_h = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="s_h")
    #c_h = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="c_h")
    #y_pred_s = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="y_pred_s")

    loss = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="loss")
    proj_h = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h,
                           p.data_provider.grid_walls_v, modus="proj_h")
    proj_v = compute_tf_loss(y_true_enc, y_pred_enc,
                             p.data_provider.grid_walls_h,
                             p.data_provider.grid_walls_v, modus="proj_v")
    l_angle_diff_scale = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="angle_diff_scale")
    #l_angle_diff = compute_tf_loss(y_true_enc, y_pred_enc, p.data_provider.grid_walls_h, p.data_provider.grid_walls_v, modus="angle_diff")
    l_d_hor_pos = compute_tf_loss(y_true_enc, y_pred_enc,
                                  p.data_provider.grid_walls_h,
                                  p.data_provider.grid_walls_v,
                                  modus="d_hor_pos")
    l_d_hor_neg = compute_tf_loss(y_true_enc, y_pred_enc,
                                  p.data_provider.grid_walls_h,
                                  p.data_provider.grid_walls_v,
                                  modus="d_hor_neg")
    l_d_ver_pos = compute_tf_loss(y_true_enc, y_pred_enc,
                                  p.data_provider.grid_walls_h,
                                  p.data_provider.grid_walls_v,
                                  modus="d_ver_pos")
    l_d_ver_neg = compute_tf_loss(y_true_enc, y_pred_enc,
                                  p.data_provider.grid_walls_h,
                                  p.data_provider.grid_walls_v,
                                  modus="d_ver_neg")
    # pos_mask_h = compute_tf_loss(y_true_enc, y_pred_enc,
    #                               p.data_provider.grid_walls_h,
    #                               p.data_provider.grid_walls_v,
    #                               modus="pos_mask_h")
    # pos_mask_v = compute_tf_loss(y_true_enc, y_pred_enc,
    #                              p.data_provider.grid_walls_h,
    #                              p.data_provider.grid_walls_v,
    #                              modus="pos_mask_v")
    walls_h_center = compute_tf_loss(y_true_enc, y_pred_enc,
                                 p.data_provider.grid_walls_h,
                                 p.data_provider.grid_walls_v,
                                 modus="walls_h_center")


    grid_cell_pred = convert_from_2dim_overlapping_rotated_grid(p, grid_size=40,
                                       quantile=False,
                                       store=True,
                                       average_samples=True)

    y_pred_box = np.concatenate(p.summary.y_pred, axis=0)
    y_pred_box = p.summary.y_pred[f_idx]#[p1:p2, :]
    y_true = p.summary.y_true_labels[f_idx]

    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    fp_dims = (200, 80)
    #fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img, walls=walls_h[w1:w2, :])

    for idx in range(200):  # len(y_true)):

        # check diff in angle
        w_idx = np.argmax(l_angle_diff_scale[idx, :])
        diff = y_pred_box[idx, 4] - p.data_provider.grid_walls_h[w_idx, 4]

        walls_idx = np.where(l_angle_diff_scale[idx, :] != 0.0)[0]

        # check closest hor wall
        h_w_idx_pos = np.argmin(np.abs(l_d_hor_pos[idx, :]))#np.where(l_d_hor_pos[idx, :] != 100.0)[0]
        h_w_idx_neg = np.argmin(np.abs(l_d_hor_neg[idx, :]))#np.where(l_d_hor_neg[idx, :] != 100.0)[0]

        walls_h_c_point = convert_grid_point_to_global_coord_system(
            walls_h_center[h_w_idx_pos], int(grid_cell_pred[f_idx][idx, 0]),
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        projection_point_pos = proj_h[idx, h_w_idx_pos, :]
        projection_point_neg = proj_h[idx, h_w_idx_neg, :]

        projection_point_pos = convert_grid_point_to_global_coord_system(
            projection_point_pos, int(grid_cell_pred[f_idx][idx, 0]),
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        projection_point_neg = convert_grid_point_to_global_coord_system(
            projection_point_neg, int(grid_cell_pred[f_idx][idx, 0]),
            p.data_provider.floorplan_height,
            p.data_provider.floorplan_width,
            grid_size=40.0)

        #h_w_idx_pos = np.where(pos_mask_h[idx, :])[0]

        h_v_idx_pos = np.argmin(np.abs(l_d_ver_pos[idx, :]))
        h_v_idx_neg = np.argmin(np.abs(l_d_ver_neg[idx, :]))

        fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img,
                              walls=walls_h[walls_idx])#[w1:w2, :])

        fp.draw_rectangles_new(anchors=y_pred_box[idx, :], color='black')
        #for angle in np.arange(0, -1, -0.1):
        #    y_pred_box[idx, 4] = angle
        poly_1 = get_rotated_polygon(*y_pred_box[idx, :])
        w = y_pred_box[idx, 2]
        y_pred_box[idx, 2] = y_pred_box[idx, 3]
        y_pred_box[idx, 3] = w
        poly_2 = get_rotated_polygon(*y_pred_box[idx, :])
        fp.draw_polygons([poly_1])

        # draw closest walls
        fp.draw_lines(np.array([walls_h[h_w_idx_pos, 0], walls_h[h_w_idx_pos, 2]]), np.array([walls_h[h_w_idx_pos, 1], walls_h[h_w_idx_pos, 3]]), color="green")

        fp.draw_lines(
           np.array([walls_h[h_w_idx_neg, 0], walls_h[h_w_idx_neg, 2]]),
           np.array([walls_h[h_w_idx_neg, 1], walls_h[h_w_idx_neg, 3]]), color="blue")

        fp.draw_points(y_true[idx, 0], y_true[idx, 1])

        # draw projection points
        fp.draw_points(projection_point_pos[0], projection_point_pos[1], color="red")
        fp.draw_points(projection_point_neg[0], projection_point_neg[1], color="red")
        fp.draw_points(walls_h_c_point[0], walls_h_c_point[1],
                       color="green")


        print("diff: {}, line: {}, box: {}".format(diff, p.data_provider.grid_walls_h[w_idx, 4], y_pred_box[idx, 4]))
        fp.show_plot()


def get_rotated_polygon(cx, cy, w, h, omega):

    omega = -omega#/2

    center = np.array([cx, cy])

    ll = np.array([cx - w/2.0, cy - h/2.0]) - center
    lr = np.array([cx + w/2.0, cy - h/2.0]) - center
    ul = np.array([cx - w/2.0, cy + h/2.0]) - center
    ur = np.array([cx + w/2.0, cy + h/2.0]) - center

    exterior = [ll, lr, ur, ul]
    exterior_r = []

    for p in exterior:
        px = p[0] * np.cos(omega * math.pi) - p[1] * np.sin(omega * math.pi)
        py = p[1] * np.cos(omega * math.pi) + p[0] * np.sin(omega * math.pi)

        exterior_r.append(np.array([px, py]) + center)

    return Polygon(np.array(exterior_r))


def rotate_around_point(p, omega, o=None):
    p_man = np.copy(p)

    if o is not None:
        p_man -= o

    px = p_man[0] * np.cos(omega * math.pi) - p_man[1] * np.sin(omega * math.pi)
    py = p_man[1] * np.cos(omega * math.pi) + p_man[0] * np.sin(omega * math.pi)

    p_man = np.array([px, py])

    if o is not None:
        p_man += o

    return p_man


def loss_rotation_visualization():
    import matplotlib.pyplot as plt

    omega = -0.1666666666

    fig = plt.figure()

    grid_cell = np.array([[-1, -1], [1, -1], [1,1], [-1, 1], [-1,-1]])

    hor_line = np.array([[0.2, 0.7], [0.5, 0.7]])

    y_true = np.array([-0.5, 0.1])
    center = np.array([-0.3, 0.4])
    dims = np.array([0.3, 0.2])

    ll = center - dims/2
    ur = center + dims/2
    lr = center + np.array([1, -1])*dims/2
    ul = center + np.array([-1, 1])*dims/2

    box = np.array([ll, lr, ur, ul, ll])

    plt.scatter(y_true[0], y_true[1], color="blue")
    plt.plot(box[:, 0], box[:, 1])
    plt.plot(grid_cell[:, 0], grid_cell[:, 1])

    plt.plot(hor_line[:, 0], hor_line[:, 1], color="orange")
    plt.scatter(hor_line[0,0], hor_line[0, 1], color="blue")
    plt.scatter(hor_line[1, 0], hor_line[1, 1], color="red")

    y_t = rotate_around_point(y_true, omega)

    grid_cell_rot = np.array([rotate_around_point(p, omega) for p in grid_cell])
    plt.plot(grid_cell_rot[:, 0], grid_cell_rot[:, 1], color="red")

    # rotate center of box but not dims
    center_r = rotate_around_point(center, omega)
    ll_r = center_r - dims / 2
    ur_r = center_r + dims / 2
    lr_r = center_r + np.array([1, -1]) * dims / 2
    ul_r = center_r + np.array([-1, 1]) * dims / 2

    box_r = np.array([ll_r, lr_r, ur_r, ul_r, ll_r])
    plt.plot(box_r[:, 0], box_r[:, 1])

    #box_rot = np.array([rotate_around_point(p, omega) for p in box])
    #plt.plot(box_rot[:, 0], box_rot[:, 1], color="green")

    line_center = np.mean(hor_line, axis=0)
    line_rot = np.array([rotate_around_point(p, -omega, o=hor_line[0, :]) for p in hor_line])
    plt.plot(line_rot[:, 0], line_rot[:, 1], color="orange")
    plt.scatter(line_rot[0, 0], line_rot[0, 1], color="blue")
    plt.scatter(line_rot[1, 0], line_rot[1, 1], color="red")

    # get omega of rotated line
    omega_1 = np.arctan2(
        line_rot[1, 1] - line_rot[0, 1], line_rot[1, 0] - line_rot[0, 0]) /math.pi
    print(omega_1)

    plt.scatter(y_t[0], y_t[1], color="green")

    plt.axis('equal')
    plt.show()


def test_vis():

    p1 = np.array([1.0, 1.0])
    p2 = np.array([3.0, 1.0])
    line = np.array([p1,p2])
    omega = 0.25

    center = np.mean(line, axis=0)

    rot = np.array([rotate_around_point(p, omega, o=center) for p in line])

    import matplotlib.pyplot as plt
    plt.figure()

    plt.scatter(line[:, 0], line[:, 1])
    plt.scatter(rot[:, 0], rot[:, 1])
    plt.plot(line[:, 0], line[:, 1])
    plt.plot(rot[:, 0], rot[:, 1])

    plt.axis('equal')
    plt.show()


def pointOnLine(p0, p1, q):

    # p0 and p1 define the line segment
    # q is the reference point (aka mouse)
    # returns point on the line closest to px

    if p0[0] == p1[0] and p0[1] == p1[1]:
      p0[0] -= 0.00001

    Unumer = ((q[0] - p0[0]) * (p1[0] - p0[0])) + ((q[1] - p0[1]) * (p1[1] - p0[1]));
    Udenom = math.pow(p1[0] - p0[0], 2) + math.pow(p1[1] - p0[1], 2);
    U = Unumer / Udenom;

    r = np.array([p0[0] + (U * (p1[0] - p0[0])), p0[1] + (U * (p1[1] - p0[1]))])

    minx = min(p0[0], p1[0]);
    maxx = max(p0[0], p1[0]);
    miny = min(p0[1], p1[1]);
    maxy = max(p0[1], p1[1]);

    isValid = (r[0] >= minx and r[0] <= maxx) and (r[1] >= miny and r[1] <= maxy);

    if isValid:
        return r
    else:
        return None

def orthongonal_projection(s, p):

    return (np.dot(p, s) / np.dot(s, s)) * s

def test_point_on_line():

    p0 = np.array([2,1])
    p1 = np.array([5,8])

    q = np.array([6, 2])

    p1_dash = p1 - p0
    q_dash = q - p0





    p = np.array([p0, p1])

    import matplotlib.pyplot as plt

    plt.figure()

    plt.scatter(p[:, 0], p[:, 1])
    plt.scatter(*q)
    plt.plot(p[:, 0], p[:, 1])

    proj = orthongonal_projection(p1_dash, q_dash)
    proj += p0
    plt.scatter(proj[0], proj[1])

    plt.axis("equal")
    plt.show()


def test_all_wall_loss():
    f_idx = 2

    p: Pipeline = Storable.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/rot/output/BBOX_1l_20_base_1")

    p.data_provider.transform_to_2dim_overlapping_grid_encoding(
        grid_size=40, labels=p.summary.y_true_labels[f_idx])

    y_true_enc = p.data_provider.grid_labels
    y_pred_enc = p.summary.y_pred[f_idx]

    walls_h = np.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/analysis/hor_walls.npy")
    walls_v = np.load(
        "/Users/mariuslaska/PycharmProjects/boxprediction/analysis/ver_walls.npy")

    p.data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
        grid_size=40, horizontal=True, angle_only=True, overlap_strategy='keep',
        walls=walls_h)

    p.data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
        grid_size=40, horizontal=False, angle_only=True, overlap_strategy='keep',
        walls=walls_v)

    loss = compute_tf_all_wall_loss(y_true_enc, y_pred_enc,
                                    p.data_provider.grid_walls_h[:, :5], p.data_provider.grid_walls_v[:, :5], n_rows=3, n_cols=6, gs=40.0, pad_ratio=0.1, modus="loss")

    print("test")

if __name__ == '__main__':
    #test_vis()
    #loss_rotation_visualization()

    main_all_walls()
    #test_all_wall_loss()
    #main()

    #test_point_on_line()

    walls_h = np.array([[1,3,1,2], [2,1,-3,5], [1,1,5,5]])

    y_pred = np.array([[1,2], [2,5], [-1,3], [2,1]])

    c_h = y_pred[:, np.newaxis, :2] - walls_h[:, :2]

    print("test")