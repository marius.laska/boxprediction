from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable

import numpy as np
from pkg_resources import resource_filename

from analysis.evaluate_circle import PI
from analysis.visualize_box_overlap import normalize
import matplotlib.pyplot as plt

from analysis.visualize_learning_progress import convert_from_grid, convert_from_2dim_grid
from base.floor_plan_plot import FloorPlanPlotRec


def main():

    p_box: Pipeline = Storable.load("/home/laskama/Dropbox/BBOX_1")
    p_reg: Pipeline = Storable.load("/home/laskama/Dropbox/REG_1")
    dp = p_box.data_provider
    fp_width = dp.floorplan_width
    fp_height = dp.floorplan_height

    split_idx = 1
    radius = 10.069

    x_test, y_true = dp.get_test_data(split_idx=split_idx, area_labels=False)
    x_train, y_train = dp.get_train_data(split_idx=split_idx, area_labels=False)

    #convert_from_grid(p_box)
    convert_from_2dim_grid(p_box, grid_size=40)

    y_pred = p_box.summary.y_pred[split_idx]#np.concatenate(p_box.summary.y_pred[split_idx], axis=0)
    y_pred_reg = p_reg.summary.y_pred[split_idx]

    #y_true = np.concatenate(p_box.summary.y_true[split_idx], axis=0)

    # get test point
    idx = 0
    g_size = 6

    size = np.prod(y_pred[:, 2:], axis=1)
    mask = np.where(size * 0.7 > radius**2 * PI)[0]

    for idx in range(len(size)):
        # idx += 50
        draw(fp_width, fp_height, g_size, x_test, y_true, x_train, y_train, y_pred, y_pred_reg, idx)




    print("test")


def draw(fp_width, fp_height, g_size, x_test, y_true, x_train, y_train, y_pred_box, y_pred_reg, idx=0):
    num_g_x = int(fp_width / g_size)
    num_g_y = int(fp_height / g_size)

    overlap_count = np.full((num_g_y, num_g_x), np.nan)

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            error_diff(y_true, y_pred_box, lower_left, upper_right, threshold=5)

            error = error_grid(x_test[idx], x_train, y_train, lower_left,
                               upper_right)
            overlap_count[num_g_y - y_idx - 1, x_idx] = error

    overlap_count, norm_overlap_count = normalize(overlap_count, min=1, max=255)

    # colormap
    cmap = plt.cm.jet  # define the colormap
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # force the first color entry to be grey
    cmaplist[0] = (.5, .5, .5, 0.1)

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))

    # Set alpha
    my_cmap[:, -1] = 0.8
    my_cmap[0] = (.75, .75, .75, 0.8)

    #img = "../evaluation/gia/gia_floor_4.jpg"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    #fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="Error")

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            count = overlap_count[num_g_y - y_idx - 1, x_idx]

            fp.draw_rectangles(np.concatenate((lower_left, upper_right)),
                               color=my_cmap[int(count)], fill=True)

            fp.draw_circles(centers=y_pred_reg[idx, :2], radius=10.069,
                            color='b')

            fp.draw_points(y_true[idx, 0], y_true[idx, 1], color="green")

    fp.draw_rectangles_new(anchors=y_pred_box[idx, :], color='black')

    plt.show()


def error_diff(y_true, y_pred, lower_left, upper_right, threshold=5):
    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    x_range = np.logical_and(y_true[:, 0] <= max_x, y_true[:, 0] >= min_x)
    y_range = np.logical_and(y_true[:, 1] <= max_y, y_true[:, 1] >= min_y)

    f_range = np.logical_and(x_range, y_range)

    # get subset that where target value is inside of grid cell
    g_mask = np.where(f_range)[0]

    if len(g_mask) < 3:
        return

    center_diff = np.full((len(g_mask), len(g_mask)), np.inf)
    for i in range(len(g_mask)):
        for j in range(len(g_mask)):
            if j>i:
                val = np.linalg.norm(y_pred[g_mask[i]] - y_pred[g_mask[j]])
                center_diff[i, j] = val
                #center_diff[j, i] = val

    # get subset where predicted
    s_mask = np.where(center_diff < threshold)

    if len(s_mask[0]) < 1:
        return

    s_diff_sum = 0
    for i, j in zip(*s_mask):
        box_i = y_pred[g_mask[i], 2:]
        box_j = y_pred[g_mask[j], 2:]

        s_diff_sum += np.linalg.norm(box_i - box_j)

    s_diff_avg = s_diff_sum / len(s_mask[0])
    if s_diff_avg > 4:
        print(s_diff_avg)


def error_grid(x_ref, x, y, lower_left, upper_right):#y_pred, y_true, y_pred_reg, lower_left, upper_right):

    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    x_range = np.logical_and(y[:, 0] <= max_x, y[:, 0] >= min_x)
    y_range = np.logical_and(y[:, 1] <= max_y, y[:, 1] >= min_y)

    range = np.logical_and(x_range, y_range)

    mask = np.where(range)[0]

    if len(mask) < 1:
        return np.nan

    diff = np.linalg.norm(x[mask] - x_ref, axis=1)

    return np.mean(diff)


if __name__ == "__main__":
    main()