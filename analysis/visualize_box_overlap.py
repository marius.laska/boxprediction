from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pkg_resources import resource_filename
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

from analysis.full_visual_cmp import transform_quantile_to_bbox
from analysis.visualize_learning_progress import convert_from_2dim_grid, convert_from_grid

import matplotlib.pyplot as plt
import matplotlib as mpl

from base.floor_plan_plot import FloorPlanPlotRec


def plot_mean_error_per_grid(pipe: Pipeline):

    p_c = pipe

    #convert_from_grid(pipe)
    convert_from_2dim_grid(pipe, grid_size=40, quantile=True)
    transform_quantile_to_bbox(pipe)

    dp = pipe.data_provider
    fp_height = dp.floorplan_height
    fp_width = dp.floorplan_width
    g_size = 10

    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)

    y_pred_reg = np.concatenate(p_c.summary.y_pred, axis=0)

    error = np.linalg.norm(y_pred_reg[:, :2] - y_true, axis=1)
    mask = np.where(error <= 60)[0] #np.logical_and(error <= 10, y_true[:, 0] >= 40))[0]
    y_true = y_true[mask]
    y_pred = y_pred[mask]
    y_pred_reg = y_pred_reg[mask]

    num_g_x = int(fp_width / g_size)
    num_g_y = int(fp_height / g_size)

    overlap_count = np.full((num_g_y, num_g_x), np.nan)
    pred_size = np.full((num_g_y, num_g_x), np.nan)
    succ_rate = np.full((num_g_y, num_g_x), np.nan)
    num_grid = np.full((num_g_y, num_g_x), np.nan)
    std_size_grid = np.full((num_g_y, num_g_x), np.nan)
    r_score_grid = np.full((num_g_y, num_g_x), np.nan)
    r_coeff_grid = np.full((num_g_y, num_g_x), np.nan)

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            # get average error per grid
            error, size, size_std, r_score, r_coeff, succ = error_grid(y_pred, y_true, y_pred_reg, lower_left, upper_right)
            num = get_num_grid(y_pred, y_true, y_pred_reg, lower_left, upper_right)
            overlap_count[num_g_y - y_idx - 1, x_idx] = error
            pred_size[num_g_y - y_idx - 1, x_idx] = size #avg_box_size_grid(y_pred, lower_left, upper_right)
            succ_rate[num_g_y - y_idx - 1, x_idx] = succ
            num_grid[num_g_y - y_idx - 1, x_idx] = num
            std_size_grid[num_g_y - y_idx - 1, x_idx] = size_std
            r_score_grid[num_g_y - y_idx - 1, x_idx] = r_score
            r_coeff_grid[num_g_y - y_idx - 1, x_idx] = r_coeff

    # scale since size grows quadratically
    #

    # normalize between 0 and 255 to match cmap values
    overlap_count, norm_overlap_count = normalize(overlap_count, min=1, max=255)
    pred_size, norm_pred_size = normalize(pred_size, min=1, max=255)
    succ_rate, norm_succ_rate = normalize(succ_rate, min=1, max=255)
    num_grid, norm_num_grid = normalize(num_grid, min=0, max=255)
    std_size_grid, norm_std_size_grid = normalize(std_size_grid, min=0, max=255)
    r_score_grid, norm_r_score_grid = normalize(r_score_grid, min=1, max= 255)
    r_coeff_grid, norm_r_coeff_grid = normalize(r_coeff_grid, min=1, max=255)

    # colormap
    cmap = plt.cm.jet  # define the colormap
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # force the first color entry to be grey
    cmaplist[0] = (.5, .5, .5, 0.1)

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))

    # Set alpha
    my_cmap[:, -1] = 0.8
    my_cmap[0] = (.75, .75, .75, 0.8)

    #img = "../evaluation/gia/gia_floor_4.jpg"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    #fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="Error")
    fp_size = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="BoxSize")
    fp_succ = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="ACC")
    fp_num = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="#Test")
    fp_size_std = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="BoxSizeSTD")
    fp_r_score = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img, title="R-score")
    fp_r_coeff = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img,
                                  title="R-coeff")

    plots = [fp, fp_size, fp_succ, fp_num, fp_size_std, fp_r_score, fp_r_coeff]
    norms = [norm_overlap_count, norm_pred_size, norm_succ_rate, norm_num_grid, norm_std_size_grid, norm_r_score_grid, norm_r_coeff_grid]

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            count = overlap_count[num_g_y - y_idx - 1, x_idx]
            size = pred_size[num_g_y - y_idx - 1, x_idx]
            succ = succ_rate[num_g_y - y_idx - 1, x_idx]
            num = num_grid[num_g_y - y_idx - 1, x_idx]
            s_std = std_size_grid[num_g_y - y_idx - 1, x_idx]
            r_score = r_score_grid[num_g_y - y_idx - 1, x_idx]
            r_coeff = r_coeff_grid[num_g_y - y_idx - 1, x_idx]

            fp.draw_rectangles(np.concatenate((lower_left, upper_right)),
                               color=my_cmap[int(count)], fill=True)
            fp_size.draw_rectangles(np.concatenate((lower_left, upper_right)),
                               color=my_cmap[int(size)], fill=True)
            fp_succ.draw_rectangles(np.concatenate((lower_left, upper_right)),
                                    color=my_cmap[int(succ)], fill=True)
            fp_num.draw_rectangles(np.concatenate((lower_left, upper_right)),
                                    color=my_cmap[int(num)], fill=True)
            fp_size_std.draw_rectangles(np.concatenate((lower_left, upper_right)),
                                   color=my_cmap[int(s_std)], fill=True)
            fp_r_score.draw_rectangles(
                np.concatenate((lower_left, upper_right)),
                color=my_cmap[int(r_score)], fill=True)
            fp_r_coeff.draw_rectangles(
                np.concatenate((lower_left, upper_right)),
                color=my_cmap[int(r_coeff)], fill=True)

    for p,n in zip(plots, norms):
        add_colorbar(p, cmap, n)

    fp.show_plot()


def main():

    #pipe: Pipeline = Storable.load("evaluation/gia/all_data/full_cmp/output/BBOX")
    pipe: Pipeline = Storable.load("evaluation/lohan/full_cmp/output/BBOX")
    radius = 5

    dp = pipe.data_provider
    fp_height = dp.floorplan_height
    fp_width = dp.floorplan_width
    g_size = 1

    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    num_g_x = int(fp_width / g_size)
    num_g_y = int(fp_height / g_size)

    overlap_count = np.zeros((num_g_y, num_g_x))

    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):

            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            # get number of fully overlapping prediction boxes
            overlap_count[num_g_y - y_idx - 1, x_idx] = overlap(y_pred, lower_left, upper_right)

    overlap_count = (((overlap_count - np.min(overlap_count)) * (255 - 0)) / (
                np.max(overlap_count) - np.min(overlap_count))) + 0

    # colormap
    cmap = plt.cm.jet  # define the colormap
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # force the first color entry to be grey
    cmaplist[0] = (.5, .5, .5, 0.1)

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))

    # Set alpha
    my_cmap[:, -1] = np.linspace(0, 1, cmap.N)

    #img = "evaluation/gia/gia_floor_4.jpg"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    #fp_dims = (83.32, 17.16)
    fp_dims = (200, 80)

    fp = FloorPlanPlotRec(fp_dims, 20, floorplan_bg_img=img)
    for y_idx in range(num_g_y):
        for x_idx in range(num_g_x):
            lower_left = np.array([x_idx * g_size, (y_idx) * g_size])
            upper_right = np.array([(x_idx + 1) * g_size, (y_idx + 1) * g_size])

            count = overlap_count[num_g_y - y_idx - 1, x_idx]

            fp.draw_rectangles(np.concatenate((lower_left, upper_right)), color=cmaplist[int(count)], fill=True)

    fp.show_plot()


def avg_box_size_grid(y_pred, lower_left, upper_right):
    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    ll = y_pred[:, 2:]
    ur = y_pred[:, :2]

    range_ll = np.logical_and(ll[:, 0] <= min_x, ll[:, 1] <= min_y)
    range_ur = np.logical_and(ur[:, 0] >= max_x, ur[:, 1] >= max_y)

    range = np.logical_and(range_ll, range_ur)

    mask = np.where(range)[0]

    if len(mask) == 0:
        return np.nan

    # size of prediction cells
    return np.mean(np.prod(y_pred[mask, 2:], axis=1))


def get_num_grid(y_pred, y_true, y_pred_reg, lower_left, upper_right):
    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    x_range = np.logical_and(y_true[:, 0] <= max_x, y_true[:, 0] >= min_x)
    y_range = np.logical_and(y_true[:, 1] <= max_y, y_true[:, 1] >= min_y)

    range = np.logical_and(x_range, y_range)

    mask = np.where(range)[0]

    return len(mask)


def error_grid(y_pred, y_true, y_pred_reg, lower_left, upper_right):

    min_x = lower_left[0]
    min_y = lower_left[1]
    max_x = upper_right[0]
    max_y = upper_right[1]

    x_range = np.logical_and(y_true[:, 0] <= max_x, y_true[:, 0] >= min_x)
    y_range = np.logical_and(y_true[:, 1] <= max_y, y_true[:, 1] >= min_y)

    range = np.logical_and(x_range, y_range)

    mask = np.where(range)[0]

    if len(mask) < 10:
        return np.nan, np.nan, np.nan, np.nan, np.nan, np.nan

    #size = np.abs(y_pred[mask, 0] - y_pred[mask, 2]) * np.abs(y_pred[mask, 1] - y_pred[mask, 3])

    diff = np.absolute(y_true[mask] - y_pred[mask, :2])
    w = y_pred[mask, 2]
    h = y_pred[mask, 3]

    x_range = diff[:, 0] <= w/2
    y_range = diff[:, 1] <= h/2

    correct = np.where(np.logical_and(x_range, y_range))[0]

    size = np.prod(y_pred[mask, 2:], axis=1)
    diff = np.linalg.norm(y_true[mask] - y_pred_reg[mask, :2], axis=1)

    regr = linear_model.LinearRegression()

    # Train the model using the training sets
    regr.fit(diff.reshape(-1, 1), size.reshape(-1, 1))
    # Make predictions using the testing set
    y_val = regr.predict(diff.reshape(-1, 1))

    r_score = r2_score(size, y_val)

    if r_score > 0.2:
        plt.scatter(diff, size)
        plt.plot(diff, y_val)

        # The coefficients
        print('Coefficients: \n', regr.coef_)
        # The mean squared error
        print('Mean squared error: %.2f'
              % mean_squared_error(size, y_val))
        # The coefficient of determination: 1 is perfect prediction
        print('Coefficient of determination: %.2f'
              % r_score)

    plt.show()

    return np.mean(diff), np.mean(size), np.std(size), r_score, regr.coef_[0][0], len(correct)/len(mask)


def overlap_reg(y_pred, radius, lower_left, upper_right):
    ll = lower_left
    up = upper_right
    ul = ll + np.array([0, upper_right[1]])
    lr = ll + np.array([upper_right[0], 0])

    diff_ll = np.linalg.norm(ll - y_pred, axis=1)
    diff_up = np.linalg.norm(up - y_pred, axis=1)
    diff_ul = np.linalg.norm(ul - y_pred, axis=1)
    diff_lr = np.linalg.norm(lr - y_pred, axis=1)

    a = diff_ll <= radius
    b = diff_up <= radius
    c = diff_ul <= radius
    d = diff_lr <= radius

    mask = np.logical_and(a, np.logical_and(b, np.logical_and(c, d)))

    return len(np.where(mask)[0])


def add_colorbar(fp, cmap, norm):
    divider = make_axes_locatable(fp.axis)
    ax_cb = divider.new_horizontal(size="5%", pad=0.15)
    cb1 = mpl.colorbar.ColorbarBase(ax_cb, cmap=cmap,
                                    norm=norm,
                                    orientation='vertical')
    fp.fig.add_axes(ax_cb)


def normalize(x, min=1, max=255, nan_val=0):
    norm = mpl.colors.Normalize(vmin=np.nanmin(x),
                                vmax=np.nanmax(x))

    x = (((x - np.nanmin(x)) * (max - min)) / (
            np.nanmax(x) - np.nanmin(x))) + min
    x[np.where(np.isnan(x))] = nan_val

    return x, norm


def overlap(y_pred, lower_left, upper_right):
    centers = y_pred[:, :2]
    widths = y_pred[:, 2]
    heights = y_pred[:, 3]

    p_lower_lefts_x = centers[:, 0] - widths/2
    p_lower_lefts_y = centers[:, 1] - heights/2

    p_upper_rights_x = centers[:, 0] + widths / 2
    p_upper_rights_y = centers[:, 1] + heights / 2

    a = p_lower_lefts_x <= lower_left[0]
    b = p_lower_lefts_y <= lower_left[1]

    c = p_upper_rights_x >= upper_right[0]
    d = p_upper_rights_y >= upper_right[1]

    mask = np.logical_and(a, np.logical_and(b, np.logical_and(c, d)))

    return len(np.where(mask)[0])


if __name__ == "__main__":
    #main()
    for id in [128]:

        pipe: Pipeline = Storable.load("../evaluation/lohan/evaluation/grid_q/output/QUANTILE")

        plot_mean_error_per_grid(pipe)