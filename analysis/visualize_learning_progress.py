import math

import numpy as np
from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from pkg_resources import resource_filename

from base.floor_plan_plot import FloorPlanPlotRec


def main():
    num = 8
    # path = "/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/scaled_output_2/output/BBOX"
    path = "../evaluation/lohan/evaluation/grid/output/BBOX"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    #img = "../evaluation/gia/gia_floor_4.jpg"
    fp_dims = (200, 80)
    #fp_dims = (83.32, 17.16)

    for exp in [0]:

        pipe: Pipeline = Storable.load(path)  # .format(2**exp))

        # visualize train test

        convert_from_grid(pipe)

        # for fold_idx in range(pipe.data_provider.num_splits):
        #     pred_fold = np.zeros((len(pipe.summary.y_pred[fold_idx]), 4))
        #
        #     sub = pipe.summary.y_pred[fold_idx][:, 4::5]
        #     test = np.argmax(sub, axis=1)
        #
        #     for idx in range(len(pipe.summary.y_pred[fold_idx])):
        #         local_box = pipe.summary.y_pred[fold_idx][idx,
        #                             test[idx] * 5:(test[idx] + 1) * 5 - 1]
        #
        #         local_box[0] = (test[idx] + local_box[0]) * min(pipe.data_provider.floorplan_height, pipe.data_provider.floorplan_width)
        #         local_box[1:] *= min(pipe.data_provider.floorplan_width, pipe.data_provider.floorplan_height)
        #
        #         pred_fold[idx] = local_box
        #
        #     #pipe.summary.y_true_labels[fold_idx][:, 0] *= fp_dims[0]
        #     #pipe.summary.y_true_labels[fold_idx][:, 1] *= fp_dims[1]
        #
        #     pipe.summary.y_pred[fold_idx] = pred_fold
        #     #pipe.summary.y_pred[fold_idx][:, [0, 2]] *= fp_dims[0]
        #     #pipe.summary.y_pred[fold_idx][:, [1, 3]] *= fp_dims[1]

        # dp = pipe.data_provider
        # for s_idx in range(dp.num_splits):
        #     x_train, x_val, x_test, y_train, y_val, y_test = dp.get_train_val_test(split_idx=s_idx, area_labels=False)
        #     train_y = np.concatenate((y_train, y_val), axis=0)
        #
        #     fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
        #     fp.draw_points(train_y[:, 0], train_y[:, 1], color="green")
        #     fp.draw_points(y_test[:, 0], y_test[:, 1], color="red")
        #
        #     fp.show_plot()

        y_pred_box = np.concatenate(pipe.summary.y_pred, axis=0)
        y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)

        vertical = np.logical_and(y_true[:, 1] <= 50, y_true[:, 1] >= 20)
        horizontal = np.logical_and(y_true[:, 0] <= 50, y_true[:, 0] >= 25)
        subarea = np.logical_and(vertical, horizontal)

        dist = np.absolute(y_true - y_pred_box[:, :2])
        inside = np.logical_and(dist[:, 0] <= y_pred_box[:, 2] / 2,
                                dist[:, 1] <= y_pred_box[:, 3] / 2)

        mask = np.where(subarea)#np.logical_and(subarea, ~inside))[0]
        # mask = np.where(inside)[0]
        #mask = mask[:100]
        #mask = range(len(y_true))  # len(y_true))

        #fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
        for idx in mask:  # len(y_true)):
            idx += 0
            fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
            fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='g', alpha=0.5)
            y_pred_box[:, 2:] = np.abs(y_pred_box[:, 2:])
            fp.draw_rectangles_new(anchors=y_pred_box[idx, :], color='black')

            fp.show_plot()


def convert_from_2dim_2layer_grid(pipe: Pipeline, grid_size=20, quantile=False, store=True, average_samples=True):
    n_cols = math.ceil(pipe.data_provider.floorplan_width / grid_size)
    res_folds = []

    height = pipe.data_provider.floorplan_height
    width = pipe.data_provider.floorplan_width

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size)
    num_col_l1 = np.ceil(width / grid_size)
    num_l1 = int(num_row_l1 * num_col_l1)

    num_row_l2 = num_row_l1 - 1
    num_col_l2 = num_col_l1 - 1
    num_l2 = int(num_row_l2 * num_col_l2)

    num = num_l1 + num_l2

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # check whether 1st or 2nd layer
        if idx < num_l1:
            # 1st layer
            r_idx = int(idx / num_col_l1)
            c_idx = idx % num_col_l1

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(np.array([x, y]))

        else:
            # 2nd layer
            idx -= num_l1
            r_idx = int(idx / num_col_l2)
            c_idx = idx % num_col_l2

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(np.array([x + grid_size / 2.0, y + grid_size / 2.0]))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    origins = np.array(origins)

    for fold_idx in range(pipe.data_provider.num_splits):

        pred_vals = pipe.summary.y_pred[fold_idx]
        if type(pred_vals) is not list:
            # need to average
            pred_val_samples = [pred_vals]
        else:
            pred_val_samples = pred_vals

        pred_fold = np.zeros((len(pred_val_samples[0]), 4, len(pred_val_samples)))

        for s_idx, pred_vals in enumerate(pred_val_samples):

            sub = pred_vals[:, 4::5]
            chosen = np.argmax(sub, axis=1)

            for idx in range(len(pred_vals)):
                local_box = pred_vals[idx,
                            chosen[idx] * 5:(chosen[idx] + 1) * 5 - 1]

                local_box[:2] = origins[chosen[idx], :] + local_box[:2] * grid_size

                #local_box[0] = ((chosen[idx] + local_box[0]) % n_cols ) * grid_size

                #local_box[1] = (int(chosen[idx] / n_cols) + local_box[1]) * grid_size

                # 3rd and 4th dimension contain width and height
                local_box[2:] *= grid_size

                pred_fold[idx, :, s_idx] = local_box

        # average pred fold
        if average_samples:
            pred_fold = np.mean(pred_fold, axis=2)

        if store:
            pipe.summary.y_pred[fold_idx] = pred_fold

        else:
            res_folds.append(pred_fold)

    if len(res_folds) > 0:
        return res_folds


def convert_from_2dim_3layer_grid(pipe: Pipeline, grid_size=20, quantile=False, store=True, average_samples=True):
    n_cols = math.ceil(pipe.data_provider.floorplan_width / grid_size)
    res_folds = []

    height = pipe.data_provider.floorplan_height
    width = pipe.data_provider.floorplan_width

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size)
    num_col_l1 = np.ceil(width / grid_size)
    num_l1 = int(num_row_l1 * num_col_l1)

    num_row_l2 = num_row_l1 + 1
    num_col_l2 = num_col_l1
    num_l2 = int(num_row_l2 * num_col_l2)

    num_row_l3 = num_row_l1 - 1
    num_col_l3 = num_col_l1
    num_l3 = int(num_row_l3 * num_col_l3)

    num = num_l1 + num_l2 + num_l3

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # check whether 1st or 2nd layer
        if idx < num_l1:
            # 1st layer
            r_idx = int(idx / num_col_l1)
            c_idx = idx % num_col_l1

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(np.array([x, y]))

        elif idx < (num_l1 + num_l2):
            # 2nd layer
            idx -= num_l1
            r_idx = int(idx / num_col_l2)
            c_idx = idx % num_col_l2

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(np.array([x - grid_size / 2.0, y - grid_size / 2.0]))

        else:
            # 3rd layer
            idx -= (num_l1 + num_l2)
            r_idx = int(idx / num_col_l3)
            c_idx = idx % num_col_l3

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(
                np.array([x, y + grid_size / 2.0]))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    origins = np.array(origins)

    for fold_idx in range(pipe.data_provider.num_splits):

        pred_vals = pipe.summary.y_pred[fold_idx]
        if type(pred_vals) is not list:
            # need to average
            pred_val_samples = [pred_vals]
        else:
            pred_val_samples = pred_vals

        pred_fold = np.zeros((len(pred_val_samples[0]), 4, len(pred_val_samples)))

        for s_idx, pred_vals in enumerate(pred_val_samples):

            sub = pred_vals[:, 4::5]
            chosen = np.argmax(sub, axis=1)

            for idx in range(len(pred_vals)):
                local_box = pred_vals[idx,
                            chosen[idx] * 5:(chosen[idx] + 1) * 5 - 1]

                local_box[:2] = origins[chosen[idx], :] + local_box[:2] * grid_size

                #local_box[0] = ((chosen[idx] + local_box[0]) % n_cols ) * grid_size

                #local_box[1] = (int(chosen[idx] / n_cols) + local_box[1]) * grid_size

                # 3rd and 4th dimension contain width and height
                local_box[2:] *= grid_size

                pred_fold[idx, :, s_idx] = local_box

        # average pred fold
        if average_samples:
            pred_fold = np.mean(pred_fold, axis=2)

        if store:
            pipe.summary.y_pred[fold_idx] = pred_fold

        else:
            res_folds.append(pred_fold)

    if len(res_folds) > 0:
        return res_folds


def convert_from_2dim_overlapping_grid(pipe: Pipeline, grid_size=20, padding_ratio=0.1, quantile=False, store=True, average_samples=True):
    n_cols = math.ceil(pipe.data_provider.floorplan_width / grid_size)
    res_folds = []

    height = pipe.data_provider.floorplan_height
    width = pipe.data_provider.floorplan_width

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size) + 1
    num_col_l1 = np.ceil(width / grid_size) + 1
    num_l1 = int(num_row_l1 * num_col_l1)

    num = num_l1

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # 1st layer
        r_idx = int(idx / num_col_l1)
        c_idx = idx % num_col_l1

        x = c_idx * grid_size
        y = r_idx * grid_size

        origins.append(np.array([x, y]))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    origins = np.array(origins)

    for fold_idx in range(pipe.data_provider.num_splits):

        pred_vals = pipe.summary.y_pred[fold_idx]
        if type(pred_vals) is not list:
            # need to average
            pred_val_samples = [pred_vals]
        else:
            pred_val_samples = pred_vals

        pred_fold = np.zeros((len(pred_val_samples[0]), 4, len(pred_val_samples)))

        for s_idx, pred_vals in enumerate(pred_val_samples):

            sub = pred_vals[:, 4::5]
            chosen = np.argmax(sub, axis=1)

            for idx in range(len(pred_vals)):
                local_box = pred_vals[idx,
                            chosen[idx] * 5:(chosen[idx] + 1) * 5 - 1]

                local_box[:2] = origins[chosen[idx], :] + local_box[:2] * (grid_size / 2.0 + grid_size * padding_ratio)

                # 3rd and 4th dimension contain width and height
                # convert from [-1,1] to [0,1]
                local_box[2:] = (local_box[2:] + 1) / 2.0
                local_box[2:] *= grid_size

                pred_fold[idx, :, s_idx] = local_box

        # average pred fold
        if average_samples:
            pred_fold = np.mean(pred_fold, axis=2)

        if store:
            pipe.summary.y_pred[fold_idx] = pred_fold

        else:
            res_folds.append(pred_fold)

    if len(res_folds) > 0:
        return res_folds

def convert_grid_point_to_global_coord_system(point, grid_cell_idx, height, width, grid_size=40.0, padding_ratio=0.1):

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size) + 1
    num_col_l1 = np.ceil(width / grid_size) + 1
    num_l1 = int(num_row_l1 * num_col_l1)

    num = num_l1

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # 1st layer
        r_idx = int(idx / num_col_l1)
        c_idx = idx % num_col_l1

        x = c_idx * grid_size
        y = r_idx * grid_size

        origins.append(np.array([x, y]))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    origins = np.array(origins)

    point = origins[grid_cell_idx, :] + point[:, :2] * (
                grid_size / 2.0 + grid_size * padding_ratio)

    return point


def convert_from_2dim_overlapping_rotated_grid(pipe: Pipeline, grid_size=20, padding_ratio=0.1, quantile=False, store=True, average_samples=True):
    n_cols = math.ceil(pipe.data_provider.floorplan_width / grid_size)
    res_folds = []
    res_grid_cell_folds = []

    height = pipe.data_provider.floorplan_height
    width = pipe.data_provider.floorplan_width

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size) + 1
    num_col_l1 = np.ceil(width / grid_size) + 1
    num_l1 = int(num_row_l1 * num_col_l1)

    num = num_l1

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # 1st layer
        r_idx = int(idx / num_col_l1)
        c_idx = idx % num_col_l1

        x = c_idx * grid_size
        y = r_idx * grid_size

        origins.append(np.array([x, y]))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    origins = np.array(origins)

    for fold_idx in range(pipe.data_provider.num_splits):

        pred_vals = pipe.summary.y_pred[fold_idx]
        if type(pred_vals) is not list:
            # need to average
            pred_val_samples = [pred_vals]
        else:
            pred_val_samples = pred_vals

        pred_fold = np.zeros((len(pred_val_samples[0]), 5, len(pred_val_samples)))
        pred_grid_cell = np.zeros((len(pred_val_samples[0]), len(pred_val_samples)))

        for s_idx, pred_vals in enumerate(pred_val_samples):

            sub = pred_vals[:, 5::6]
            chosen = np.argmax(sub, axis=1)

            for idx in range(len(pred_vals)):
                local_box = pred_vals[idx,
                            chosen[idx] * 6:(chosen[idx] + 1) * 6 - 1]

                local_box[:2] = origins[chosen[idx], :] + local_box[:2] * (grid_size / 2.0 + grid_size * padding_ratio)

                # 3rd and 4th dimension contain width and height
                # convert from [-1,1] to [0,1]
                local_box[2:4] = (local_box[2:4] + 1) / 2.0
                local_box[2:4] *= grid_size

                pred_fold[idx, :, s_idx] = local_box
                pred_grid_cell[idx, s_idx] = chosen[idx]

        # average pred fold
        res_grid_cell_folds.append(pred_grid_cell)

        if average_samples:
            pred_fold = np.mean(pred_fold, axis=2)

        if store:
            pipe.summary.y_pred[fold_idx] = pred_fold

        else:
            res_folds.append(pred_fold)

    if len(res_folds) > 0:
        return res_folds
    else:
        return res_grid_cell_folds


def convert_from_2dim_grid(pipe: Pipeline, grid_size=20, quantile=False, store=True, average_samples=True):
    n_cols = math.ceil(pipe.data_provider.floorplan_width / grid_size)
    res_folds = []

    for fold_idx in range(pipe.data_provider.num_splits):

        pred_vals = pipe.summary.y_pred[fold_idx]
        if type(pred_vals) is not list:
            # need to average
            pred_val_samples = [pred_vals]
        else:
            pred_val_samples = pred_vals

        pred_fold = np.zeros((len(pred_val_samples[0]), 4, len(pred_val_samples)))

        for s_idx, pred_vals in enumerate(pred_val_samples):

            sub = pred_vals[:, 4::5]
            chosen = np.argmax(sub, axis=1)

            for idx in range(len(pred_vals)):
                local_box = pred_vals[idx,
                            chosen[idx] * 5:(chosen[idx] + 1) * 5 - 1]

                local_box[0] = ((chosen[idx] + local_box[0]) % n_cols ) * grid_size

                local_box[1] = (int(chosen[idx] / n_cols) + local_box[1]) * grid_size

                if quantile:
                    # 3rd and 4th dimension contain x,y coords of upper quantile
                    local_box[2] = ((chosen[idx] + local_box[
                        2]) % n_cols) * grid_size

                    local_box[3] = (int(chosen[idx] / n_cols) + local_box[
                        3]) * grid_size
                else:
                    # 3rd and 4th dimension contain width and height
                    local_box[2:] *= grid_size

                pred_fold[idx, :, s_idx] = local_box

        # average pred fold
        if average_samples:
            pred_fold = np.mean(pred_fold, axis=2)

        if store:
            pipe.summary.y_pred[fold_idx] = pred_fold

        else:
            res_folds.append(pred_fold)

    if len(res_folds) > 0:
        return res_folds


def convert_from_grid(pipe: Pipeline):

    for fold_idx in range(pipe.data_provider.num_splits):
        pred_fold = np.zeros((len(pipe.summary.y_pred[fold_idx]), 4))

        sub = pipe.summary.y_pred[fold_idx][:, 4::5]
        test = np.argmax(sub, axis=1)

        for idx in range(len(pipe.summary.y_pred[fold_idx])):
            local_box = pipe.summary.y_pred[fold_idx][idx,
                        test[idx] * 5:(test[idx] + 1) * 5 - 1]

            local_box[0] = (test[idx] + local_box[0]) * min(
                pipe.data_provider.floorplan_height,
                pipe.data_provider.floorplan_width)
            local_box[1:] *= min(pipe.data_provider.floorplan_width,
                                 pipe.data_provider.floorplan_height)

            #local_box[2:] += 5

            pred_fold[idx] = local_box

        pipe.summary.y_pred[fold_idx] = pred_fold


if __name__ == "__main__":
    main()
