from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
import numpy as np

from base.floor_plan_plot import FloorPlanPlotRec

PI = 3.14159265359


def main():

    pipe: Pipeline = Storable.load("evaluation/gia/quantile/output/QUANTILE")

    acc, size = get_acc_and_size(pipe)

    print("ACC: {}, SIZE: {}".format(acc, size))

    visualize(pipe)

def get_acc_and_size(pipe: Pipeline):
    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    size = (y_pred[:, 0] - y_pred[:, 2]) * (y_pred[:, 1] - y_pred[:, 3])

    upper = np.all(y_true <= y_pred[:, :2], axis=1)
    lower = np.all(y_true >= y_pred[:, 2:], axis=1)
    mask = np.logical_and(upper, lower)

    idx = np.where(mask)[0]

    return len(idx)/len(y_true), np.mean(size)

def visualize(pipe: Pipeline):

    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    max_el = len(y_true)
    img = "evaluation/gia/gia_floor_4.jpg"
    # fp = FloorPlanPlotRec((83.32, 17.16), 20, floorplan_bg_img=img)
    #plot_data_heatmap(pipe, floor_plotter=fp)
    for idx in range(max_el):
        idx += 0
        fp = FloorPlanPlotRec((83.32, 17.16), 2, floorplan_bg_img=img)
        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='g', alpha=0.5)

        fp.draw_rectangles(y_pred[idx, :])

        fp.draw_points(y_pred[idx, 0], y_pred[idx, 1], color='b', alpha=0.5)
        fp.draw_points(y_pred[idx, 2], y_pred[idx, 3], color='r', alpha=0.5)

        fp.show_plot()


if __name__ == "__main__":
    main()