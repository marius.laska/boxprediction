import numpy as np
from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from pkg_resources import resource_filename

from base.floor_plan_plot import FloorPlanPlotRec


def main():

    num = 8
    u_alpha = "09"
    l_alpha = "01"

    path = "evaluation/lohan/sq/output/SQ_{}_{}"
    img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
    fp_dims = (200, 80)


    p_x_u: Pipeline = Storable.load(path.format('x', u_alpha))
    p_x_l: Pipeline = Storable.load(path.format('x', l_alpha))
    #p_y_u: Pipeline = Storable.load(path.format('y', u_alpha))
    #p_y_l: Pipeline = Storable.load(path.format('y', l_alpha))

    y_true = np.concatenate(p_x_u.summary.y_true_labels, axis=0)

    y_pred_xl = np.concatenate(p_x_l.summary.y_pred, axis=0)
    y_pred_xu = np.concatenate(p_x_u.summary.y_pred, axis=0)
    #y_pred_yl = np.concatenate(p_y_l.summary.y_pred, axis=0)
    #y_pred_yu = np.concatenate(p_y_u.summary.y_pred, axis=0)


    mask = range(100)

    #fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
    for idx in mask:  # len(y_true)):
        idx += 0
        fp = FloorPlanPlotRec(fp_dims, 2, floorplan_bg_img=img)
        fp.draw_points(y_true[idx, 0], y_true[idx, 1], color='g', alpha=0.5)
        fp.draw_points(y_pred_xl[idx], np.array([40]),#y_pred_yl[idx],
                       color="r")
        fp.draw_points(y_pred_xu[idx], np.array([40]), #y_pred_yu[idx],
                       color="b")

        #fp.draw_rectangles(anchors=y_pred_quantile[idx, :], color="g")

        fp.show_plot()


if __name__ == "__main__":
    main()