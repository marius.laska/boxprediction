import math
import os
import numpy as np

from data.data_provider_base import DataProviderBase
from il_pipeline.models.dnn_model import DnnModel
from il_pipeline.summary.summary import KFoldClassSummary
from il_pipeline.models.tf_model_definition import classification_model_for_generator, cnn_model_for_generator
from tensorflow.keras.activations import softmax, linear, sigmoid, tanh
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras.backend import categorical_crossentropy, relu, softmax, elu
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.optimizers import Adam, Adamax

from base.bbox_model_definition import bbox_model_for_generator, \
    cnn_deep_loc_box, ae_model, cnn_bbox_model_for_generator, scalabel_bbox_model, scalable_ae_model
from base.hier_model_definition import hier_model_for_generator
from base.custom_loss import bbox_loss, define_bbox_loss, define_circle_loss, \
    define_quantile_loss, \
    define_single_quantile_loss, define_yolo_loss, define_grid_quantile_loss, \
    define_yolo_loss_tanh, define_yolo_loss_tanh_no_size, \
    define_rotated_box_loss, define_rotated_box_loss_all_walls
from base.custom_rot_loss import define_rot_box_loss_modular


class BboxModel(DnnModel):

    def __init__(self, type="classification", summary: KFoldClassSummary=None,
                 data_provider: DataProviderBase=None,
                 params=None, output_dir=None, filename=None):
        super().__init__(type, summary, data_provider, params, output_dir, filename)

    def load_weights(self, file=None):
        if file is None:
            file = self.output_dir + self.filename + "_f{}.hdf5".format(self.data_provider.current_split_idx)
        self.classifier.load_weights(file)

    def setup_params(self):
        params = self.params

        # update parameters with standard values for classification model
        params.update({
            'shape': 'brick',
            'weight_regulizer': None,
            'kernel_initializer': 'he_normal',
            'optimizer': Adam,
            'activation': relu})

        if self.type == "classification" or self.type == "CNN":
            params.update({'last_activation': softmax,
                           'losses': categorical_crossentropy})

        elif self.type == "regression":
            params.update({'last_activation': linear,
                           'losses': mean_squared_error})

        elif self.type == "BBOX":
            if 'loss' in params:
                loss_func = define_bbox_loss(params['loss'])
            else:
                loss_func = bbox_loss

            params.update({'last_activation': sigmoid,
                           'losses': loss_func})

        elif self.type == "GRID-BBOX" or self.type == "GRID_2L-BBOX" or self.type == "GRID_3L-BBOX":
            if 'loss' in params:
                loss_func = define_yolo_loss(params['loss'])
            else:
                loss_func = bbox_loss

            params.update({'last_activation': sigmoid,
                           'losses': loss_func})

        elif self.type == "GRID_OVERLAP-BBOX" or self.type == "GRID_OVERLAP-BBOX_SCALABLE-DNN":
            if 'loss' in params:
                loss_func = define_yolo_loss_tanh_no_size(params['loss'])

            params.update({'last_activation': tanh,
                           'losses': loss_func})

        elif self.type == "GRID_OVERLAP-BBOX_CNN":
            if 'loss' in params:
                loss_func = define_yolo_loss_tanh_no_size(params['loss'])

            params.update({'last_activation': tanh,
                           'losses': loss_func,
                           'activation': elu,
                           'optimizer': Adamax})

        elif self.type == "GRID_OVERLAP-BBOX_2D-CNN":
            if 'loss' in params:
                loss_func = define_yolo_loss_tanh_no_size(params['loss'])

            params.update({'last_activation': tanh,
                           'losses': loss_func})

        elif self.type == "CNNLoc_REG":
            params.update({'activation': elu,
                           'losses': mean_squared_error,
                           'optimizer': Adamax,
                           'last_activation': linear})

        elif self.type == "GRID_ROT-BBOX":
            if 'loss' in params:
                height = self.data_provider.floorplan_height
                width = self.data_provider.floorplan_width
                grid_size = self.data_provider.grid_size
                loss_func = define_rot_box_loss_modular(
                    params['loss'],
                    tf_walls_h=self.data_provider.grid_walls_h[:, :5],
                    tf_walls_v=self.data_provider.grid_walls_v[:, :5],
                    n_rows=np.ceil(height / grid_size) + 1,
                    n_cols=np.ceil(width / grid_size) + 1,
                    gs=grid_size)
                # loss_func = define_rotated_box_loss(
                #     params['loss'],
                #     tf_walls_h=self.data_provider.grid_walls_h,
                #     tf_walls_v=self.data_provider.grid_walls_v)

            params.update({'last_activation': tanh,
                           'losses': loss_func})

        elif self.type == "CIRCLE":
            if 'loss' in params:
                loss_func = define_circle_loss(params['loss'])
            else:
                print("ERROR: no loss defined for CIRCLE model")

            params.update({'last_activation': linear,
                           'losses': loss_func})

        elif self.type == "QUANTILE":
            if 'loss' in params:
                loss_func = define_quantile_loss(params['loss'])

                params.update({'last_activation': linear,
                               'losses': loss_func})

        elif self.type == "GRID-QUANTILE":
            if 'loss' in params:
                loss_func = define_grid_quantile_loss(params['loss'])

                params.update({'last_activation': sigmoid,
                               'losses': loss_func})

        elif self.type == "S_Q":
            if 'loss' in params:
                loss_func = define_single_quantile_loss(params['loss'])

                params.update({'last_activation': linear,
                               'losses': loss_func})

    def train_autoencoder(self):
        x_cols, _ = self.data_provider.get_data_dims("regression")
        if self.type == "GRID_OVERLAP-BBOX_SCALABLE-DNN":
            self.classifier, bottleneck_model = scalable_ae_model(x_cols, self.params)
        else:
            self.classifier, bottleneck_model = ae_model(x_cols, self.params)
        self._train_model(save_weights_only=False, evaluate=False, autoencoder=True)
        bottleneck_model.save(self.output_dir + "AE_bottleneck_{}_f{}.hdf5".format(
            self.filename, self.data_provider.current_split_idx), include_optimizer=False)

    def pre_train_model(self, params):
        if 'loss' in params:
            if self.type == "BBOX":
                loss = define_bbox_loss(params['loss'])
            elif self.type == "CIRCLE":
                loss = define_circle_loss(params['loss'])

            self.params.update({'losses': loss})

        if 'epochs' in params:
            self.params.update({'epochs': params['epochs']})

        self.setup_model()
        self._train_model(save_weights_only=False, evaluate=False)

    def setup_model(self, setup_params=False):
        # use instance variables if no optional variables are supplied
        output_dir = self.output_dir
        dp = self.data_provider
        params = self.params

        if setup_params:
            self.setup_params()

        # obtain Keras classifier (Sequential) for specified parameters
        x_cols, y_cols = dp.get_data_dims("regression") # was regression
        if self.type == "regression":
            y_cols = 2
        if self.type == "BBOX":
            y_cols = 4
        if self.type == "CIRCLE":
            y_cols = 3
        if self.type == "QUANTILE":
            y_cols = 4
        if self.type == "S_Q":
            y_cols = 1
        if self.type == "CNNLoc_REG":
            y_cols = 2
        if self.type == "GRID-BBOX" or self.type == "GRID-QUANTILE":
            if dp.grid_size is None:
                # single row or column encoding
                ma = max(dp.floorplan_width, dp.floorplan_height)
                mi = min(dp.floorplan_width, dp.floorplan_height)
                y_cols = 5 * math.ceil(ma/mi)
            else:
                # grid encoding with fixed grid size
                y_cols = 5 * math.ceil(
                    dp.floorplan_height / dp.grid_size) * math.ceil(
                    dp.floorplan_width / dp.grid_size)

        if self.type == "GRID_OVERLAP-BBOX" \
                or self.type == "GRID_OVERLAP-BBOX_CNN" \
                or self.type == "GRID_OVERLAP-BBOX_2D-CNN" \
                or self.type == "GRID_OVERLAP-BBOX_SCALABLE-DNN":
            num_gs = dp.get_num_grid_cells()
            if num_gs is None:
                y_cols = 5 * (math.ceil(
                    dp.floorplan_height / dp.grid_size) + 1) * (math.ceil(
                    dp.floorplan_width / dp.grid_size) + 1)
            else:
                y_cols = 5 * num_gs

        if self.type == "GRID_2L-BBOX":
            # grid encoding with fixed grid size
            n_row = math.ceil(dp.floorplan_height / dp.grid_size)
            n_col = math.ceil(dp.floorplan_width / dp.grid_size)

            y_cols = 5 * (n_row * n_col + (n_row-1)*(n_col-1))

        if self.type == "GRID_3L-BBOX":
            # grid encoding with fixed grid size
            n_row = math.ceil(dp.floorplan_height / dp.grid_size)
            n_col = math.ceil(dp.floorplan_width / dp.grid_size)

            y_cols = 5 * (n_row * n_col + (n_row+1)*(n_col) + (n_row - 1) * n_col)

        if self.type == "GRID_ROT-BBOX":
            y_cols = 6 * (math.ceil(
                dp.floorplan_height / dp.grid_size) + 1) * (math.ceil(
                dp.floorplan_width / dp.grid_size) + 1)

        # obtain the model definition
        if self.type == "GRID_OVERLAP-BBOX_CNN":
            ae_file = self.output_dir + "AE_bottleneck_{}_f{}.hdf5".format(
                self.filename, self.data_provider.current_split_idx)
            self.classifier = cnn_deep_loc_box(ae_file, y_cols, params)

        elif self.type == "GRID_OVERLAP-BBOX_SCALABLE-DNN":
            ae_file = self.output_dir + "AE_bottleneck_{}_f{}.hdf5".format(
                self.filename, self.data_provider.current_split_idx)
            self.classifier = scalabel_bbox_model(x_cols, y_cols, ae_file, params)

        elif self.type == "GRID_OVERLAP-BBOX_2D-CNN":
            x_cols, _ = dp.get_data_dims("CNN")
            classifier_template = cnn_bbox_model_for_generator()
            self.classifier = classifier_template(x_cols, y_cols, params)

        elif "BBOX" in self.type or "CIRCLE" in self.type or "QUANTILE" in self.type or "S_Q" in self.type:
            classifier_template = bbox_model_for_generator(metrics=[])
            self.classifier = classifier_template(X_cols=x_cols, Y_cols=y_cols, params=params)
        elif self.type == "CNN":
            classifier_template = cnn_model_for_generator(metrics=[])
            self.classifier = classifier_template(input_shape=x_cols,
                                                  Y_cols=y_cols, params=params)
        elif self.type == "CNNLoc_REG":
            ae_file = self.output_dir + "AE_bottleneck_{}_f{}.hdf5".format(
                self.filename, self.data_provider.current_split_idx)
            self.classifier = cnn_deep_loc_box(ae_file, y_cols, self.params)
        else:
            classifier_template = classification_model_for_generator(
                metrics=[])

            self.classifier = classifier_template(
                X_cols=x_cols, Y_cols=y_cols, params=params)

        if "2D-CNN" not in self.type:
            self.type = "regression"

    def _train_model(self, save_weights_only=False, evaluate=True, autoencoder=False):
        """
        Trains a DNN model with the specified parameters using Keras
        fit_generator function. Model is evaluated on separate test data
        """
        output_dir = self.output_dir
        data_provider = self.data_provider
        params = self.params

        # self.setup_model()

        # calculate batch sizes (might be smaller than specified batch size if
        # not enough data supplied)
        num_train, num_val, num_test = data_provider.get_train_val_test_num(area_labels=False)

        train_bs = min(num_train, params['batch_size'])
        val_bs = min(num_val, train_bs)
        test_bs = min(num_test, train_bs)

        # calculate the steps per epoch (used by generator) to determine
        # after how many steps a new epoch starts (model has seen all data)
        val_steps_per_epoch = math.ceil(num_val / val_bs)
        num_augs = 0
        if 'augmentation' in params and params['augmentation'] is not None:
            num_augs = params['augmentation']

        train_steps_per_epoch = math.ceil(num_train * (num_augs + 1) / train_bs)

        # setup callbacks
        ae_str = "AE_" if autoencoder else ""
        checkpoint_file_name = output_dir + "{}{}_f{}.hdf5".format(ae_str, self.filename, data_provider.current_split_idx)

        # save best performing model parameters
        checkpoint = ModelCheckpoint(checkpoint_file_name, verbose=0,
                                     monitor='val_loss',
                                     save_best_only=True, mode='auto',
                                     save_weights_only=save_weights_only)

        earlyStopping = EarlyStopping(monitor='val_loss',
                                      patience=500,
                                      verbose=0,
                                      mode='auto')

        # obtain generator function from data provider
        generator = data_provider.in_mem_data_generator
        area_labels = self.type == "classification" or "CNN" in self.type

        noise_percentage = None
        if 'noise_percentage' in self.params:
            noise_percentage = self.params['noise_percentage']

        train_on_noisy_labels = False
        if 'noisy_labels' in self.params:
            train_on_noisy_labels = True

        self.classifier.fit_generator(
            generator(mode='train',
                      model_type=self.type,
                      area_labels=area_labels,
                      augmentation=num_augs,
                      autoencoder=autoencoder,
                      noise_percentage=noise_percentage,
                      noisy_labels=train_on_noisy_labels),
            validation_data=generator(mode='val', model_type=self.type,
                                      area_labels=area_labels,
                                      autoencoder=autoencoder,
                                      noisy_labels=train_on_noisy_labels,
                                      batch_size=val_bs),
            validation_steps=val_steps_per_epoch,
            steps_per_epoch=train_steps_per_epoch,
            epochs=params['epochs'],
            callbacks=[earlyStopping, checkpoint],
            verbose=2)

        # evaluate model and store results in summary file
        if evaluate:
            #self.type = "regression"
            self.evaluate_model(test_bs, chkpt_file=checkpoint_file_name)

    def collect_train_progress(self, test_bs):
        """
        Evaluates the model on separate test data (supplied by data generator)
        :param test_bs: batch size of test data
        """

        data_provider = self.data_provider
        summary = self.summary
        classifier = self.classifier

        path = self.output_dir + "{}.hdf5".format(self.filename)
        if os.path.exists(path):
            classifier.load_weights(path)

        # calculate the amount of steps on the test data before one batch
        # is completed (required by generator)
        _, _, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")
        test_steps_per_epoch = math.ceil(num_test / test_bs)

        area_labels = self.type == "classification" or self.type == "cnn"
        y_pred = classifier.predict_generator(
            data_provider.in_mem_data_generator(
                mode='val', model_type=self.type,
                area_labels=area_labels, batch_size=test_bs),
            steps=test_steps_per_epoch,
            verbose=0)

        #if self.type != "regression":
        if data_provider.area_labels is not None:
            _, y_true = data_provider.get_test_data()
            summary.y_true.append(y_true)

        _, y_true_labels = data_provider.get_test_data(area_labels=False)
        summary.y_pred.append(y_pred)
        summary.y_true_labels.append(y_true_labels)

        summary.num_folds += 1

    # def evaluate_model(self, test_bs):
    #     """
    #     Evaluates the model on separate test data (supplied by data generator)
    #     :param test_bs: batch size of test data
    #     """
    #
    #     data_provider = self.data_provider
    #     summary = self.summary
    #     classifier = self.classifier
    #
    #     path = self.output_dir + "{}_f{}.hdf5".format(self.filename, data_provider.current_split_idx)
    #     if os.path.exists(path):
    #         classifier.load_weights(path)
    #
    #     # calculate the amount of steps on the test data before one batch
    #     # is completed (required by generator)
    #     _, _, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")
    #     test_steps_per_epoch = math.ceil(num_test / test_bs)
    #
    #     area_labels = self.type == "classification" or self.type == "cnn"
    #
    #     y_pred_list = []
    #     for idx in range(100):
    #
    #         y_pred = classifier.predict_generator(
    #             data_provider.in_mem_data_generator(
    #                 mode='test', model_type=self.type,
    #                 area_labels=area_labels, batch_size=test_bs),
    #             steps=test_steps_per_epoch,
    #             verbose=0)
    #
    #         y_pred_list.append(y_pred)
    #
    #     #if self.type != "regression":
    #     if data_provider.area_labels is not None:
    #         _, y_true = data_provider.get_test_data()
    #         summary.y_true.append(y_true)
    #
    #     _, y_true_labels = data_provider.get_test_data(area_labels=False)
    #     summary.y_pred.append(y_pred_list)
    #     summary.y_true_labels.append(y_true_labels)
    #
    #     summary.num_folds += 1