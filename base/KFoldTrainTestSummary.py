from il_pipeline.summary.summary import KFoldClassSummary


class KFoldTrainTestSummary(KFoldClassSummary):

    def __init__(self, loc_scores=None, accuracies=None,
                 fmeasure_accuracies=None):
        super().__init__(self, loc_scores, accuracies,
                 fmeasure_accuracies)

        self.y_pred_train = None