import numpy as np

from il_pipeline.pipeline import Pipeline
from pkg_resources import resource_filename

from base.floor_plan_plot import FloorPlanPlotRec
from main import calc_acc_c
import math
from shapely.geometry import box

img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')


def bbox_acs(pipe: Pipeline, lambda_const=0.5):

    dp = pipe.data_provider

    # positive mask
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)

    acc, wrong_idx, correct_idx = calc_acc_c(y_true, y_pred)

    area = np.prod(y_pred[correct_idx, 2:], axis=1)

    area_cov, covered_boxes = get_covered_area_box(pipe)
    area_tot = dp.floorplan_height * dp.floorplan_width

    fp = FloorPlanPlotRec((dp.floorplan_width, dp.floorplan_height), 2, floorplan_bg_img=img)
    fp.draw_rectangles_new(covered_boxes, color="g")
    #fp.show_plot()

    term = (1/area**lambda_const)

    acs = (1/len(y_true)) * (area_cov / area_tot) * np.sum(term)

    return acs


def reg_acs(pipe: Pipeline, lambda_const=0.5, radius=5):
    dp = pipe.data_provider

    # positive mask
    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)
    y_true = np.concatenate(pipe.summary.y_true_labels, axis=0)

    dist = np.linalg.norm(y_pred - y_true, axis=1)
    correct_idx = np.where(dist <= radius)[0]

    size = math.pi * math.pow(radius, 2)

    area_cov, covered_boxes = get_covered_area_reg(pipe, radius=radius)
    area_tot = dp.floorplan_height * dp.floorplan_width

    fp = FloorPlanPlotRec((dp.floorplan_width, dp.floorplan_height), 2,
                          floorplan_bg_img=img)
    fp.draw_rectangles_new(covered_boxes, color="g")
    #fp.show_plot()

    acs = (len(correct_idx)/len(y_true)) * (area_cov/area_tot) * 1/size**lambda_const

    return acs


def get_covered_area_reg(pipe: Pipeline, grid_size=1.0, radius=5):
    dp = pipe.data_provider

    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    x = 0
    area = 0

    covered_boxes = []

    while x < dp.floorplan_width:
        y = 0
        while y < dp.floorplan_height:

            dist_ll = np.linalg.norm(y_pred - np.array([x, y]), axis=1)
            dist_ur = np.linalg.norm(y_pred - np.array([x+grid_size, y+grid_size]), axis=1)

            covered = np.any(np.logical_and(dist_ll < radius, dist_ur < radius), axis=0)

            if covered:
                area += np.square(grid_size)
                covered_boxes.append(np.array([x+0.5, y+0.5, grid_size, grid_size]))

            y += grid_size

        x += grid_size

    return area, np.array(covered_boxes)


def get_covered_area_box(pipe:Pipeline, grid_size=1.0):
    dp = pipe.data_provider

    y_pred = np.concatenate(pipe.summary.y_pred, axis=0)

    ll = y_pred[:, :2] - y_pred[:, 2:] / 2
    ur = y_pred[:, :2] + y_pred[:, 2:] / 2

    x = 0
    area = 0

    covered_boxes = []

    while x < dp.floorplan_width:
        y = 0
        while y < dp.floorplan_height:

            ll_covered = np.logical_and(ll[:, 0] < x, ll[:, 1] < y)
            ur_covered = np.logical_and(ur[:, 0] > x+grid_size, ur[:, 1] > y+grid_size)

            covered = np.any(np.logical_and(ll_covered, ur_covered), axis=0)

            if covered:
                area += np.square(grid_size)
                covered_boxes.append(
                    np.array([x + 0.5, y + 0.5, grid_size, grid_size]))

            y += grid_size

        x += grid_size

    return area, np.array(covered_boxes)
