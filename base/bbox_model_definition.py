from tensorflow.keras import regularizers, initializers
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, BatchNormalization, Input, Reshape, Conv1D
from tensorflow.keras.losses import MSE
from tensorflow.keras.optimizers import Adam, Adamax
from base.custom_dropout import Dropout
from tensorflow.keras.models import Sequential, Model, load_model
from il_pipeline.models.layer_generator import hidden_layers
from il_pipeline.models.lr_normalizer import lr_normalizer

from base.custom_loss import define_yolo_loss_tanh_no_size


def reg_model_for_talos(x_train, y_train, x_val, y_val, params):
    model = Sequential()

    model.add(Dense(params['first_neuron'], input_dim=x_train.shape[1],
                    activation=params['activation'],
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])
                    ))

    model.add(Dropout(params['dropout'], training=None))

    hidden_layers(model, params, x_train.shape[1])

    model.add(Dense(y_train.shape[1], activation=params['last_activation'],
                    # kernel_initializer=params['kernel_initializer'],
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])))  # ,
    # kernel_initializer=initializers.RandomUniform(minval=-10, maxval=15)))#,
    # bias_initializer=initializers.RandomUniform(minval=0, maxval=0)))

    # compile the model
    model.compile(loss=params['losses'],
                  optimizer=params['optimizer'](
                      lr=lr_normalizer(params['lr'], params['optimizer'])))

    out = model.fit(x_train, y_train,
                    batch_size=params['batch_size'],
                    epochs=params['epochs'],
                    validation_data=[x_val, y_val],
                    verbose=0)

    return out, model


def bbox_model_for_talos(x_train, y_train, x_val, y_val, params):
    model = Sequential()

    model.add(Dense(params['first_neuron'], input_dim=x_train.shape[1],
                    activation=params['activation'],
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])
                    ))

    model.add(Dropout(params['dropout']))

    hidden_layers(model, params, x_train.shape[1])

    model.add(Dense(5*3, activation=params['last_activation'],
                    # kernel_initializer=params['kernel_initializer'],
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])))  # ,
    # kernel_initializer=initializers.RandomUniform(minval=-10, maxval=15)))#,
    # bias_initializer=initializers.RandomUniform(minval=0, maxval=0)))

    # compile the model
    model.compile(loss=params['losses'],
                  optimizer=params['optimizer'](
                      lr=lr_normalizer(params['lr'], params['optimizer'])))

    out = model.fit(x_train, y_train,
                    batch_size=params['batch_size'],
                    epochs=params['epochs'],
                    validation_data=[x_val, y_val],
                    verbose=0)

    return out, model


def bbox_model_for_generator(metrics):

    def define_classification_model_for_generator(X_cols, Y_cols, params) -> Sequential:
        model = Sequential()

        model.add(Dense(params['first_neuron'], input_dim=X_cols,
                        activation=params['activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])
                        ))

        #model.add(BatchNormalization())

        seed_val = None
        if 'seed' in params:
            seed_val = params['seed']

        model.add(Dropout(params['dropout'], training=None, seed=seed_val))

        hidden_layers(model, params, X_cols, seed=seed_val)

        model.add(Dense(Y_cols, activation=params['last_activation'],
                        #kernel_initializer=params['kernel_initializer'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))#,
                        #kernel_initializer=initializers.RandomUniform(minval=-10, maxval=15)))#,
                        #bias_initializer=initializers.RandomUniform(minval=0, maxval=0)))

        # compile the model
        model.compile(loss=params['losses'],
                      metrics=[*metrics],
                      optimizer=params['optimizer'](
                          lr=lr_normalizer(params['lr'], params['optimizer'])))

        return model

    return define_classification_model_for_generator


def scalabel_bbox_model(X_cols, Y_cols, ae_base_model_file, params):
    ae_model = load_model(ae_base_model_file) #, compile=False)

    for layer in ae_model.layers:
        layer.trainable = True

    model = Sequential()

    model.add(ae_model)

    model.add(Dense(64, input_dim=X_cols,
                    activation="relu",
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])
                    ))

    seed_val = None
    if 'seed' in params:
        seed_val = params['seed']

    model.add(Dropout(params['dropout'], training=None, seed=seed_val))

    model.add(Dense(128,
                    activation="relu",
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])
                    ))
    #
    model.add(Dense(Y_cols, activation=params['last_activation'],
                    # kernel_initializer=params['kernel_initializer'],
                    kernel_regularizer=regularizers.l2(
                        params['regularization_penalty'])))  # ,

    # compile the model
    model.compile(loss=params['losses'],
                  optimizer=params['optimizer'](
                      lr=lr_normalizer(params['lr'], params['optimizer'])))

    return model


def scalable_ae_model(X_cols, params):
    input = Input((X_cols,))
    encode_layer = Dense(256, activation=params['activation'], name='en1')(
        input)
    encode_layer = Dense(128, activation=params['activation'], name='en2')(
        encode_layer)

    decode_layer = Dense(256, activation=params['activation'], name='de2')(
        encode_layer)
    decode_layer = Dense(X_cols, activation=params['activation'], name='de-1')(
        decode_layer)
    encoder_model_256_128 = Model(inputs=input, outputs=decode_layer)
    bottleneck_model_256_128 = Model(inputs=input,
                                     outputs=encode_layer)

    encoder_model = encoder_model_256_128
    bottleneck_model = bottleneck_model_256_128

    encoder_model.compile(
        loss=MSE,
        optimizer=params['optimizer'](
            lr=lr_normalizer(params['lr'], params['optimizer']))  # lr=0.005)
    )

    return encoder_model, bottleneck_model


def ae_model(X_cols, params):
    input = Input((X_cols,))
    encode_layer = Dense(128, activation=params['activation'], name='en1')(input)
    encode_layer = Dense(64, activation=params['activation'], name='en2')(encode_layer)

    decode_layer = Dense(128, activation=params['activation'], name='de2')(encode_layer)
    decode_layer = Dense(X_cols, activation=params['activation'], name='de-1')(decode_layer)
    encoder_model_256_128 = Model(inputs=input, outputs=decode_layer)
    bottleneck_model_256_128 = Model(inputs=input,
                                          outputs=encode_layer)

    encoder_model = encoder_model_256_128
    bottleneck_model = bottleneck_model_256_128

    encoder_model.compile(
        loss=MSE,
        optimizer=Adam(lr=params['lr'])#lr=0.005)
    )

    return encoder_model, bottleneck_model


def cnn_deep_loc_box(ae_base_model_file, y_cols, params):

    position_base_model = load_model(ae_base_model_file, compile=False)

    for layer in position_base_model.layers:
        layer.trainable = True

    position_net_input = Reshape((position_base_model.output_shape[1], 1))(
        position_base_model.output)
    if 'dropout' in params:
        position_net_input = Dropout(params['dropout'])(position_net_input)
    position_net = Conv1D(99, 22, activation=params['activation'])(position_net_input)
    position_net = Conv1D(66, 22, activation=params['activation'])(position_net)
    position_net = Conv1D(33, 22, activation=params['activation'])(position_net)
    position_net = Flatten()(position_net)

    position_predict_output = Dense(y_cols, activation=params['last_activation'])(
        position_net)
    position_model = Model(inputs=position_base_model.input,
                           outputs=position_predict_output)

    position_model.compile(
        loss=params['losses'],
        optimizer=Adam(lr=params['lr'])#lr=0.005)
    )

    return position_model


def cnn_bbox_model_for_generator():

    def define_cnn_model_for_generator(input_shape, Y_cols,
                                                  params) -> Sequential:

        model = Sequential()

        model.add(Conv2D(16, kernel_size=(3, 3),
                         activation='relu',
                         input_shape=input_shape, name="Conv2D_1", padding="same"))
        model.add(Dropout(params['dropout']))

        model.add(Conv2D(16, (3, 3), activation='relu', name="Conv2D_2", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))
        model.add(Dropout(params['dropout']))

        model.add(Conv2D(8, (3, 3), activation='relu', name="Conv2D_3", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))

        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        #model.add(Dropout(0.25))
        model.add(Dense(Y_cols, activation=params['last_activation']))

        model.compile(loss=params['losses'],
                      optimizer=params['optimizer'](
                          lr=lr_normalizer(params['lr'], params['optimizer'])))

        return model

    return define_cnn_model_for_generator
