from data.data_provider_base import DataProviderBase

from base.gia_full_grid_provider import GiaProviderFullGrid
from base.simulation_data_provider import SimulationDataProvider
from data.gia.gia_data_provider import PDdataProvider
from data.lohan.lohan_data_provider import LohanDSprovider
from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.config_reader import ConfigReader
from ldce.base import ClusterBase
from base.BboxModel import BboxModel
from base.hier_model import HierModel
from base.ujiindoorloc_provider import UJIndoorLocProvider
from base.KFoldTrainTestSummary import KFoldTrainTestSummary
from base.gia_data_provider_grid import GiaDataProviderGrid
from base.lohan_data_provider_grid import LohanDataProviderGrid
from base.ujiindoorloc_provider_grid import UJIndoorLocProviderGrid
from base.ujiindoorloc_fullgrid_provider import UJIndoorLocProviderFullGrid


class BboxPipeline(Pipeline):
    def __init__(self, data_provider: DataProviderBase=None,
                 clusterer: ClusterBase=None, config: ConfigReader=None,
                 params=None, filename=None):
        super().__init__(data_provider, clusterer, config, params, filename)
        self.summary: KFoldTrainTestSummary = None

    def store(self, filename=None):
        if 'losses' in self.model_params:
            del self.model_params['losses']
        super().store(filename)

        #
        # Obtain data provider
        #

    @classmethod
    def get_data_provider(cls, data_params, pre_params):
        if data_params['provider'] == "PDdataProvider":
            dp = GiaDataProviderGrid.setup_data_provider(
                data_params=data_params,
                pre_params=pre_params,
            )

        elif data_params['provider'] == "LohanDSprovider":
            dp = LohanDataProviderGrid.setup_data_provider(
                data_params=data_params,
                pre_params=pre_params
            )
        elif data_params['provider'] == "UJIndoorLocProvider":
            dp = UJIndoorLocProviderGrid.setup_data_provider(
                data_params=data_params,
                pre_params=pre_params
            )
        elif data_params['provider'] == "SimulationProvider":
            dp = SimulationDataProvider.setup_data_provider(
                data_params=data_params,
                pre_params=pre_params
            )
        elif data_params['provider'] == "UJIndoorLocFullProvider":
            dp = UJIndoorLocProviderFullGrid()
            dp.setup_dps(data_params, pre_params)
        elif data_params['provider'] == "GiaFullProvider":
            dp = GiaProviderFullGrid()
            dp.setup_dps(data_params, pre_params)
        else:
            dp = None

        return dp

    def train_model(self):
        super().train_model()

        params = self.model_params

        # if params['type'] == "DNN":
        #     m_type = params['pred'] if 'pred' in params else "classification"
        #     model = DnnModel(m_type, self.summary, self.data_provider, params,
        #              self.config.output_dir, self.filename)
        #     model.setup_model()
        #     model.train_model()

        if "HIER" in params['type']:
            model = HierModel(params['type'], self.summary, self.data_provider, params,
                     self.config.output_dir, self.filename)
            #model.setup_params()
            model.setup_model(setup_params=True)
            model.train_model()

        if params['type'] == "CNNLoc_REG":
            model = BboxModel(params['type'], self.summary, self.data_provider,
                              params,
                              self.config.output_dir, self.filename)
            model.setup_params()

            if 'autoencoder' in params and params['autoencoder'] == "train":
                model.train_autoencoder()

            model.setup_model(setup_params=True)
            model.train_model()

        if "BBOX" in params['type']:
            m_type = params['type']
            model = BboxModel(params['type'], self.summary, self.data_provider, params,
                     self.config.output_dir, self.filename)
            model.setup_params()

            num_epochs = params['epochs']

            #if 'pretrain' in params:
            #    model.pre_train_model(params['pretrain'])

            if 'autoencoder' in params and params['autoencoder'] == "train":
                model.train_autoencoder()

            model.type = m_type
            model.params.update({'epochs': num_epochs})
            model.setup_model(setup_params=True)
            if 'pretrain' in params:
                model.load_weights()
            model.train_model()

        if params['type'] == "CIRCLE":
            model = BboxModel(params['type'], self.summary, self.data_provider, params,
                     self.config.output_dir, self.filename)

            model.setup_params()

            if 'pretrain' in params:
                model.pre_train_model(params['pretrain'])

            model.type = "CIRCLE"
            model.setup_model(setup_params=True)
            if 'pretrain' in params:
                model.load_weights()
            model.train_model()

        if params['type'] == "QUANTILE":
            model = BboxModel(params['type'], self.summary, self.data_provider,
                              params,
                              self.config.output_dir, self.filename)

            model.setup_model(setup_params=True)
            model.train_model()

        if params['type'] == "S_Q":
            model = BboxModel(params['type'], self.summary, self.data_provider,
                              params,
                              self.config.output_dir, self.filename)

            model.setup_model(setup_params=True)
            model.train_model()

        if params['type'] == "GRID-QUANTILE":
            model = BboxModel(params['type'], self.summary, self.data_provider,
                              params,
                              self.config.output_dir, self.filename)

            model.setup_model(setup_params=True)
            model.train_model()