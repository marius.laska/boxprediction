import numpy as np
import tensorflow as tf
import math as m


def hier_loss(y_true, y_pred):

    g = 3

    p_range = tf.range(tf.shape(y_pred)[1])

    t_x = tf.cast(tf.mod(y_true[:, 2], g), dtype=tf.float32)
    t_y = tf.cast(tf.cast(y_true[:, 2] / g, dtype=tf.int32), tf.float32)

    t_x = tf.transpose(tf.ones([tf.shape(y_pred)[1], 1]) * t_x)
    t_y = tf.transpose(tf.ones([tf.shape(y_pred)[1], 1]) * t_y)

    p_x = tf.cast(tf.mod(p_range, g), dtype=tf.float32)
    p_y = tf.cast(tf.cast(p_range / g, dtype=tf.int32), tf.float32)

    p_x = tf.ones([tf.shape(y_true)[0], 1]) * p_x
    p_y = tf.ones([tf.shape(y_true)[0], 1]) * p_y

    t_x_offset = tf.transpose(tf.ones([tf.shape(y_pred)[1], 1]) * y_true[:, 0])
    t_y_offset = tf.transpose(tf.ones([tf.shape(y_pred)[1], 1]) * y_true[:, 1])

    x_diff = t_x - p_x + t_x_offset
    y_diff = t_y - p_y + t_y_offset
    
    diff = tf.sqrt(tf.square(x_diff) + tf.square(y_diff))

    loss = tf.multiply(diff, y_pred)

    sum_loss = tf.reduce_sum(loss, axis=1)

    return sum_loss

def define_quantile_loss(params):

    def quantile_loss(y_true, y_pred):
        upper_diff = tf.subtract(y_true, y_pred[:, :2]) / tf.pow(y_true, 0.5)
        lower_diff = tf.subtract(y_true, y_pred[:, 2:]) / tf.pow(y_true, 0.5)

        u_alpha = params['upper']
        l_alpha = params['lower']

        # upper
        upper_loss = tf.reduce_mean(tf.maximum(u_alpha * upper_diff, (u_alpha - 1) * upper_diff), axis=-1)
        lower_loss = tf.reduce_mean(
            tf.maximum(l_alpha * lower_diff, (l_alpha - 1) * lower_diff),
            axis=-1)

        loss = tf.add(upper_loss, lower_loss)

        return loss

    return quantile_loss


def define_single_quantile_loss(params):

    if params['target'] == "x":
        y_idx = 0
    elif params['target'] == "y":
        y_idx = 1

    alpha = params['alpha']

    def quantile_loss(y_true, y_pred):
        diff = tf.subtract(y_true[:, y_idx], y_pred)

        loss = tf.reduce_mean(tf.maximum(alpha * diff, (alpha-1) * diff), axis=-1)

        return loss

    return quantile_loss


def define_circle_loss_try(params):

    def circle_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                axis=1)
        loss += c_loss

        outside = tf.subtract(tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                    axis=1), (y_pred[:, 2]/1.5))

        outside_loss = tf.square(outside)

        if "outside" in params:

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            loss += outside_loss

        return loss

    return circle_loss

def define_circle_loss(params):

    def circle_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(
                    tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)
            loss += c_loss

        if "size" in params:
            size_loss = tf.abs(y_pred[:, 2])

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]
                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "outside" in params:

            outside = tf.subtract(tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1), (y_pred[:, 2]) )

            outside_loss = tf.maximum(tf.zeros_like(outside), outside)

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            loss += outside_loss

        return loss

    return circle_loss


def define_bbox_loss_scaled(params):

    def bbox_loss_scaled(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)
            loss += c_loss

        if "outside" in params:

            if "beta" in params["outside"]:
                beta = params["outside"]["beta"]
            else:
                beta = 2.0

            divisor_x = y_true[:, 0]/200.0 # tf.constant(beta)
            divisor_y = y_true[:, 1]/80.0 # tf.constant(beta)

            abs_diff_x = tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            abs_diff_y = tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            outside_x_loss = (abs_diff_x - tf.divide(y_pred[:, 2], divisor_x))
            outside_y_loss = (abs_diff_y - tf.divide(y_pred[:, 3], divisor_y))

            #outside_x_loss = tf.divide(outside_x_loss, abs_diff_x)
            #outside_y_loss = tf.divide(outside_y_loss, abs_diff_y)

            outside_loss = outside_x_loss + outside_y_loss

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            loss += outside_loss

        return loss

    return bbox_loss_scaled


def define_bbox_loss_new(params):

    def bbox_loss_new(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        diff_x = tf.subtract(y_true[:, 0], y_pred[:, 0])
        diff_y = tf.subtract(y_true[:, 1], y_pred[:, 1])

        abs_diff_x = tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))
        abs_diff_y = tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))

        dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

        outside_x = abs_diff_x - tf.divide(y_pred[:, 2], 2.0)
        outside_y = abs_diff_y - tf.divide(y_pred[:, 3],
                                           2.0)

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(
                    tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)

            if "scale" in params["center"]:
                c_scale = params["center"]["scale"]
                c_loss = tf.multiply(c_loss, c_scale)

            loss += c_loss



        if "size" in params:
            #size_loss = tf.reduce_sum(tf.square(y_pred[:, 2:]), axis=1)
            size_loss = tf.reduce_sum(tf.abs(y_pred[:, 2:]), axis=1)

            dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            #size_loss_x = tf.abs(y_pred[:, 2])

            #size_loss_y = tf.abs(y_pred[:, 3])

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]

                if "tau" in params["size"]:
                    tau = params["size"]["tau"]
                else:
                    tau = 20.0

                # size_loss_x = tf.maximum(tf.zeros_like(size_loss_x),
                #                        tf.subtract(size_loss_x, tf.multiply(
                #                            tf.ones_like(size_loss_x),
                #                            threshold)))
                #
                # size_loss_y = tf.maximum(tf.zeros_like(size_loss_y),
                #                        tf.subtract(size_loss_y, tf.multiply(
                #                            tf.ones_like(size_loss_y),
                #                            threshold)))

                size_loss_tau = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

                size_loss = tf.where(dist < tau, size_loss_tau, size_loss)

            #size_loss = size_loss_x/200.0 + size_loss_y/80.0

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "piecewise" in params:

            if "tau" in params["piecewise"]:
                tau = params["piecewise"]["tau"]
            else:
                tau = 5.0

            abs_diff_x = tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            abs_diff_y = tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            #condition_x = tf.greater(abs_diff_x, 20.0)
            #condition_y = tf.greater(abs_diff_y, 20.0)

            outside_x = abs_diff_x - tf.divide(y_pred[:, 2], 2.0)
            outside_y = abs_diff_y - tf.divide(y_pred[:, 3],
                                         2.0)

            outside_x = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y = tf.maximum(tf.zeros_like(outside_y), outside_y)

            piece_wise_loss_x = tf.where(dist < tau, outside_x, abs_diff_x)#tf.zeros_like(loss))
            piece_wise_loss_y = tf.where(dist < tau, outside_y, abs_diff_y)#tf.zeros_like(loss))

            p_loss = piece_wise_loss_x + piece_wise_loss_y

            if "scale" in params["piecewise"]:
                o_scale = params["piecewise"]["scale"]
                p_loss = tf.multiply(p_loss, o_scale)

            loss += p_loss

        return loss

    return bbox_loss_new


def define_bbox_loss_pw_squared(params):

    def bbox_loss_final(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(
                    tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)

            if "scale" in params["center"]:
                c_scale = params["center"]["scale"]
                c_loss = tf.multiply(c_loss, c_scale)

            loss += c_loss

        if "size" in params:

            size_loss = tf.reduce_sum(tf.square(y_pred[:, 2:]), axis=1)

            dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]

                if "tau" in params["size"]:
                    tau = params["size"]["tau"]
                else:
                    tau = 20.0

                size_loss_tau = tf.maximum(tf.zeros_like(size_loss),
                                           tf.subtract(size_loss, tf.multiply(
                                               tf.ones_like(size_loss),
                                               threshold)))

                size_loss = tf.where(dist < tau, size_loss_tau, size_loss)

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "piecewise" in params:

            if "tau" in params["piecewise"]:
                tau = params["piecewise"]["tau"]
            else:
                tau = 5.0

            abs_diff_x = tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            abs_diff_y = tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            squared_diff_x = tf.square(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            squared_diff_y = tf.square(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            outside_x = squared_diff_x - tf.square(tf.multiply(y_pred[:, 2], 0.5))
            outside_y = squared_diff_y - tf.square(tf.multiply(y_pred[:, 3], 0.5))

            outside_x = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y = tf.maximum(tf.zeros_like(outside_y), outside_y)

            piece_wise_loss_x = tf.where(dist < tau, outside_x, squared_diff_x)
            piece_wise_loss_y = tf.where(dist < tau, outside_y, squared_diff_y)

            p_loss = piece_wise_loss_x + piece_wise_loss_y

            if "scale" in params["piecewise"]:
                o_scale = params["piecewise"]["scale"]
                p_loss = tf.multiply(p_loss, o_scale)

            loss += p_loss

        return loss

    return bbox_loss_final


def define_bbox_loss_basis(params):

    def bbox_loss_scaled(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":

                c_loss_x = tf.square(tf.subtract(y_true[:, 0], y_pred[:, 0]))
                c_loss_y = tf.square(tf.subtract(y_true[:, 1], y_pred[:, 0]))

                c_loss = c_loss_x + c_loss_y

            loss += c_loss

        if "size" in params:

            size_loss = tf.abs(y_pred[:, 2]) + tf.abs(y_pred[:, 3])

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]

                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "outside" in params:

            abs_diff_x = tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            abs_diff_y = tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            outside_x = abs_diff_x - tf.divide(y_pred[:, 2], 2.0)
            outside_y = abs_diff_y - tf.divide(y_pred[:, 3],
                                         2.0)

            outside_x = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y = tf.maximum(tf.zeros_like(outside_y), outside_y)

            p_loss = outside_x + outside_y

            if "scale" in params["outside"]:
                o_scale = params["outside"]["scale"]
                p_loss = tf.multiply(p_loss, o_scale)

            loss += p_loss

        return loss

    return bbox_loss_scaled


def define_bbox_loss_piecewise(params):

    def bbox_loss_piecewise(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(
                    tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]),
                                        axis=1)

            if "scale" in params["center"]:
                c_scale = params["center"]["scale"]
                c_loss = tf.multiply(c_loss, c_scale)

            loss += c_loss

        if "size" in params:

            size_loss = tf.reduce_sum(tf.abs(y_pred[:, 2:]), axis=1)

            dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]

                if "tau" in params["size"]:
                    tau = params["size"]["tau"]
                else:
                    tau = 20.0

                size_loss_tau = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

                size_loss = tf.where(dist < tau, size_loss_tau, size_loss)

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "piecewise" in params:

            if "tau" in params["piecewise"]:
                tau = params["piecewise"]["tau"]
            else:
                tau = 5.0

            abs_diff_x = tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            abs_diff_y = tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            squared_diff_x = tf.square(tf.subtract(y_true[:, 0], y_pred[:, 0]))
            squared_diff_y = tf.square(tf.subtract(y_true[:, 1], y_pred[:, 1]))

            dist = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            outside_x = abs_diff_x - tf.divide(y_pred[:, 2], 2.0)
            outside_y = abs_diff_y - tf.divide(y_pred[:, 3],
                                         2.0)

            outside_x = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y = tf.maximum(tf.zeros_like(outside_y), outside_y)

            piece_wise_loss_x = tf.where(dist < tau, outside_x, squared_diff_x)
            piece_wise_loss_y = tf.where(dist < tau, outside_y, squared_diff_y)

            p_loss = piece_wise_loss_x + piece_wise_loss_y

            if "scale" in params["piecewise"]:
                o_scale = params["piecewise"]["scale"]
                p_loss = tf.multiply(p_loss, o_scale)

            loss += p_loss

        return loss

    return bbox_loss_piecewise


def define_bbox_loss_squared_diff(params):

    def bbox_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

            if "scale" in params["center"]:
                c_scale = params["center"]["scale"]
                c_loss = tf.multiply(c_loss, c_scale)

            loss += c_loss

        if "size" in params:
            #size_loss = tf.reduce_sum(tf.square(y_pred[:, 2:]), axis=1)
            size_loss = tf.reduce_sum(tf.abs(y_pred[:, 2:]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]
                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "outside" in params:
            if "beta" in params["outside"]:
                beta = params["outside"]["beta"]
            else:
                beta = 2.0

            divisor_x = tf.constant(beta)#tf.add(tf.multiply(tf.divide(tf.multiply(tf.ones_like(y_true[:, 0]), 200.0) - y_true[:, 0], 200.0), (6.0 - 1.0)), 1.0) #tf.constant(3.5)#
            divisor_y = tf.constant(beta)#tf.add(tf.multiply(tf.divide(tf.multiply(tf.ones_like(y_true[:, 1]), 80.0) - y_true[:, 1], 80.0), (6.0 - 1.0)), 1.0)

            width = y_pred[:, 2]# * tf.constant(200.0)
            height = y_pred[:, 3]# * tf.constant(80.0)

            outside_x = tf.square(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0])),
                tf.divide(width, divisor_x)))
            outside_y = tf.square(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1])),
                tf.divide(height, divisor_y)))

            #outside_x_loss = outside_x / tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0]))#tf.maximum(tf.zeros_like(outside_x), outside_x)
            #outside_y_loss = outside_y / tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1]))# tf.maximum(tf.zeros_like(outside_y), outside_y)

            #o_scale_x = tf.divide(tf.ones_like(outside_x_loss), y_true[:, 0])
            #o_scale_y = tf.divide(tf.ones_like(outside_y_loss), y_true[:, 1])

            #outside_x_loss = tf.square(tf.multiply(outside_x_loss, o_scale_x))
            #outside_y_loss = tf.square(tf.multiply(outside_y_loss, o_scale_y))

            outside_loss = outside_x + outside_y

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            loss += outside_loss

        return loss

    return bbox_loss


def define_bbox_loss(params):

    def bbox_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        if "center" in params:
            if params["center"] == "MSE":
                c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true, y_pred[:, :2])), axis=1)
            elif params["center"] == "L2":
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)
            else:
                c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)
            loss += c_loss

        if "size" in params:
            size_loss = tf.reduce_sum(tf.square(y_pred[:, 2:]), axis=1)

            #size_loss = tf.reduce_sum(tf.multiply(tf.abs(y_pred[:, 2:]), y_true), axis=1)
            #size_loss = tf.reduce_sum(
            #    tf.abs(y_pred[:, 2:]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]
                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "outside" in params:
            outside_x = tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0])),
                tf.divide(y_pred[:, 2], tf.constant(3.0)))

            outside_y = tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1])),
                tf.divide(y_pred[:, 3], tf.constant(3.0)))

            outside_x_loss = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y_loss = tf.maximum(tf.zeros_like(outside_y), outside_y)

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            loss += outside_loss

        return loss

    return bbox_loss


def define_grid_quantile_loss(params):
    def grid_quantile_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 4::5]

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32),
                                    depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(
            tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_s = tf.reshape(y_pred, (
        tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 5))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_s, tf.cast(g_idx, tf.int32),
                             axis=1)  # [0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        lower_diff = tf.subtract(y_true[:, :2], y_pred_s[:, :2])
        upper_diff = tf.subtract(y_true[:, :2], y_pred_s[:, 2:4])

        u_alpha = params['upper']
        l_alpha = params['lower']

        # upper
        upper_loss = tf.reduce_mean(
            tf.maximum(u_alpha * upper_diff, (u_alpha - 1) * upper_diff),
            axis=-1)
        lower_loss = tf.reduce_mean(
            tf.maximum(l_alpha * lower_diff, (l_alpha - 1) * lower_diff),
            axis=-1)

        loss += tf.add(upper_loss, lower_loss)

        return loss

    return grid_quantile_loss


def define_yolo_loss_tanh(params):
    def yolo_loss(y_true, y_pred):

        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 4::5]

        # scale from [-1,1] to [0,1]
        grid_cell_pred = tf.multiply(tf.add(grid_cell_pred, 1.0), 0.5)

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32), depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_sa = tf.reshape(y_pred, (tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 5))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx,tf.int32), axis=1)#[0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        #
        # SCALE tanh box size output to [0,1]
        #

        zeros = tf.zeros([tf.shape(y_pred_s)[0], 2])
        ones = tf.ones([tf.shape(y_pred_s)[0], 2])
        scale_add = tf.concat([zeros, ones, tf.zeros([tf.shape(y_pred_s)[0], 1])], axis=1)

        y_pred_s = tf.add(y_pred_s, scale_add)

        # box loss
        box_loss = tf.zeros_like(y_true[:, 0])
        box_loss_scale = tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true))

        # center loss
        c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
                              axis=1)

        box_loss += c_loss

        if "size" in params:
            size_loss = tf.reduce_sum(tf.square(y_pred_s[:, 2:]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]
                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            box_loss += size_loss

        if "outside" in params:
            outside_x = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
                tf.divide(y_pred_s[:, 2], tf.constant(2.5))),
                tf.constant(0.0))

            outside_y = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
                tf.divide(y_pred_s[:, 3], tf.constant(2.5))),
                tf.constant(0.0))

            outside_x_loss = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y_loss = tf.maximum(tf.zeros_like(outside_y), outside_y)

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)
            #outside_loss = outside_x_loss + outside_y_loss

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            box_loss += outside_loss

        loss += box_loss * box_loss_scale

        return loss

    return yolo_loss


def define_yolo_loss_tanh_no_size(params):
    def yolo_loss(y_true, y_pred):

        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 4::5]

        # scale from [-1,1] to [0,1]
        grid_cell_pred = tf.multiply(tf.add(grid_cell_pred, 1.0), 0.5)

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32), depth=tf.shape(grid_cell_pred)[1])

        # class_loss = tf.reduce_sum(tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        # class_loss = tf.losses.softmax_cross_entropy(grid_cell_true, logits=grid_cell_pred)
        class_loss = tf.keras.losses.categorical_crossentropy(grid_cell_true, grid_cell_pred)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_sa = tf.reshape(y_pred, (tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 5))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx,tf.int32), axis=1)#[0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        #
        # SCALE tanh box size output to [0,1]
        #

        zeros = tf.zeros([tf.shape(y_pred_s)[0], 2])
        ones = tf.ones([tf.shape(y_pred_s)[0], 2])
        scale_add = tf.concat([zeros, ones, tf.zeros([tf.shape(y_pred_s)[0], 1])], axis=1)

        y_pred_s = tf.add(y_pred_s, scale_add)

        # box loss
        box_loss = tf.zeros_like(y_true[:, 0])
        # add factor based on general batch performance
        # if grid cell chosen correctly shift loss importance to bbox loss
        box_loss_scale = tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true))#, axis=1)

        # center loss
        c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
                              axis=1)

        box_loss += c_loss

        # if "size" in params:
        #     size_loss = tf.reduce_sum(tf.square(y_pred_s[:, 2:]), axis=1)
        #
        #     if "threshold" in params["size"]:
        #         # only count size loss if greater than 50.0
        #         threshold = params["size"]["threshold"]
        #         size_loss = tf.maximum(tf.zeros_like(size_loss),
        #                                tf.subtract(size_loss, tf.multiply(
        #                                    tf.ones_like(size_loss), threshold)))
        #
        #     if "scale" in params["size"]:
        #         factor = params["size"]["scale"]
        #         size_loss = tf.multiply(size_loss, tf.constant(factor))
        #
        #     box_loss += size_loss

        if "outside" in params:

            delta = 20.0
            if "delta" in params['outside']:
                delta = params['outside']['delta']

            outside_x = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
                tf.divide(y_pred_s[:, 2], tf.constant(delta))),
                tf.constant(0.0))

            outside_y = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
                tf.divide(y_pred_s[:, 3], tf.constant(delta))),
                tf.constant(0.0))

            outside_x_loss = outside_x # tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y_loss = outside_y # tf.maximum(tf.zeros_like(outside_y), outside_y)

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)
            #outside_loss = outside_x_loss + outside_y_loss

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            box_loss += outside_loss

        loss += box_loss # * box_loss_scale

        return loss

    return yolo_loss


def define_rotated_box_loss(params, tf_walls_h, tf_walls_v, modus="loss"):
    import math as m
    tf_PI = tf.constant(m.pi)
    walls_h = tf.constant(tf_walls_h, dtype=tf.float32)
    walls_v = tf.constant(tf_walls_v, dtype=tf.float32)
    MAX_DIST = tf.constant(100, dtype=tf.float32)

    # mask to select columns of wall tensor that includes everything but angle
    non_omega_idx = tf.constant([0, 1, 2, 3, 5], dtype=tf.int32)

    def yolo_loss(y_true, y_pred):

        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 5::6]

        # scale from [-1,1] to [0,1]
        grid_cell_pred = tf.multiply(tf.add(grid_cell_pred, 1.0), 0.5)

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32), depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_sa = tf.reshape(y_pred, (tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 6))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx,tf.int32), axis=1)#[0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        #
        # SCALE tanh box size output to [0,1]
        #

        zeros = tf.zeros([tf.shape(y_pred_s)[0], 2])
        ones = tf.ones([tf.shape(y_pred_s)[0], 2])
        scale_add = tf.concat([zeros, ones, tf.zeros([tf.shape(y_pred_s)[0], 2])], axis=1)

        y_pred_s = tf.add(y_pred_s, scale_add)

        #
        # Wall loss (before rotation)
        #

        # calculate mask that stores whether wall is in grid cell of ground truth

        # helping tensor that contains all horizontal walls without the angle
        # parameter, such that it can be concatenated with vertical walls
        # that do not include encoded angle
        walls_h_no_omega = tf.concat([walls_h[:, :4], tf.reshape(walls_h[:, 5], [-1, 1])], axis=1)

        walls = tf.concat([walls_h_no_omega, walls_v], axis=0)

        wall_cell_mask_bool = tf.equal(tf.reshape(y_true[:, 2], [-1, 1]),
                                  tf.reshape(walls[:, 4], [1, -1]))
        wall_cell_mask = tf.cast(wall_cell_mask_bool, tf.float32)
        wall_cell_mask_float = tf.add(
            tf.cast(wall_cell_mask_bool, tf.float32),
            tf.cast(~wall_cell_mask_bool, tf.float32) * MAX_DIST)

        wall_h_cell_mask = tf.cast(tf.equal(tf.reshape(y_true[:, 2], [-1, 1]),
                                    tf.reshape(walls_h[:, 4], [1, -1])), tf.float32)

        wall_v_cell_mask = tf.cast(tf.equal(tf.reshape(y_true[:, 2], [-1, 1]),
                                    tf.reshape(walls_v[:, 4], [1, -1])), tf.float32)

        # get subset of walls that is within

        # 1) compute distance from centers to walls (dist_walls)

        nom = tf.matmul(
            tf.reshape(y_pred_s[:, 0], [-1, 1]),
            tf.reshape((walls[:, 3] - walls[:, 1]), [1, -1])) \
              - tf.matmul(
                  tf.reshape(y_pred_s[:, 1], [-1, 1]),
                  tf.reshape((walls[:, 2] - walls[:, 0]), [1, -1])) \
              + walls[:, 2] * walls[:, 1] \
              - walls[:, 3] * walls[:, 0]

        # mask to only include walls within grid cell
        nom = nom * wall_cell_mask #+ tf.cast(~wall_cell_mask_bool, tf.float32) * MAX_DIST

        nom = tf.where(~wall_cell_mask_bool, tf.ones_like(nom)*MAX_DIST, nom)

        #nom = tf.assign(nom, tf.where(wall_cell_mask_bool, tf.zeros_like(nom), nom))

        denom = tf.square(walls[:, 3] - walls[:, 1]) + tf.square(walls[:, 2] - walls[:, 0])

        # mask to only include walls within grid cell
        denom = tf.reshape(tf.tile(denom, [tf.shape(y_true)[0]]), [-1, tf.shape(wall_cell_mask_float)[1]])
        denom = tf.where(~wall_cell_mask_bool, tf.ones_like(denom) * MAX_DIST, denom)

        dist_walls = tf.abs(nom) / tf.sqrt(denom)

        # compute orthogonal projection of center onto walls

        # subtract first wall supporting point from wall coordinates such that
        # wall has first supporting point in center (vector projection)
        y_pred_non_rot = y_pred_s
        s_h = tf.concat([tf.reshape(walls_h[:, 2] - walls_h[:, 0], [-1, 1]), tf.reshape(walls_h[:, 3] - walls_h[:, 1], [-1, 1])], axis=1)
        c_h = y_pred_s[:, tf.newaxis, :2] - walls_h[:, :2]

        proj_h = tf.einsum('ij,jk->ijk', (tf.einsum('ijk,jk->ij', c_h, s_h) / tf.einsum('ij,ij->i', s_h, s_h)), s_h)
        # translate back
        proj_h = proj_h + walls_h[:, :2]

        # check whether projected point lies within bounds of supporting points of lines (in x-dim)
        walls_h_x = tf.concat([tf.reshape(walls_h[:, 0], [-1, 1]), tf.reshape(walls_h[:, 2], [-1, 1])], axis=1)
        walls_h_x_max = tf.reduce_max(walls_h_x, axis=1)
        walls_h_x_min = tf.reduce_min(walls_h_x, axis=1)

        wall_h_x_max_range = tf.less_equal(proj_h[:,:,0], walls_h_x_max)
        wall_h_x_min_range = tf.greater_equal(proj_h[:, :, 0], walls_h_x_min)

        wall_h_range = tf.logical_and(wall_h_x_max_range, wall_h_x_min_range)

        s_v = tf.concat([tf.reshape(walls_v[:, 2] - walls_v[:, 0], [-1, 1]), tf.reshape(walls_v[:, 3] - walls_v[:, 1], [-1, 1])], axis=1)
        c_v = y_pred_s[:, tf.newaxis, :2] - walls_v[:, :2]
        proj_v = tf.einsum('ij,jk->ijk', (tf.einsum('ijk,jk->ij', c_v, s_v) / tf.einsum('ij,ij->i', s_v, s_v)), s_v)
        # translate back
        proj_v = proj_v + walls_v[:, :2]

        walls_v_y = tf.concat([tf.reshape(walls_v[:, 1], [-1, 1]),
                               tf.reshape(walls_v[:, 3], [-1, 1])], axis=1)
        walls_v_y_max = tf.reduce_max(walls_v_y, axis=1)
        walls_v_y_min = tf.reduce_min(walls_v_y, axis=1)

        wall_v_y_max_range = tf.less_equal(proj_v[:, :, 1], walls_v_y_max)
        wall_v_y_min_range = tf.greater_equal(proj_v[:, :, 1], walls_v_y_min)

        wall_v_range = tf.logical_and(wall_v_y_max_range, wall_v_y_min_range)

        # 2) compute dist between horizontal box boundary and horizontal walls

        # compute center of horizontal walls

        walls_h_center_x = tf.reduce_mean(tf.concat(
            [tf.reshape(walls_h[:, 0], [-1, 1]),
             tf.reshape(walls_h[:, 2], [-1, 1])], axis=1), axis=1)
        walls_h_center_y = tf.reduce_mean(tf.concat(
            [tf.reshape(walls_h[:, 1], [-1, 1]),
             tf.reshape(walls_h[:, 3], [-1, 1])], axis=1), axis=1)

        walls_h_center = tf.concat([tf.reshape(walls_h_center_x, [-1, 1]),
                                    tf.reshape(walls_h_center_y, [-1, 1])],
                                   axis=1)

        # new computation of distance between center and wall using vector projection on wall
        d_hor = tf.abs(proj_h[:, :, 1] - tf.reshape(y_pred_s[:, 1], [-1, 1])) - tf.reshape(y_pred_s[:, 3]/2, [-1, 1])

        #d_hor = tf.transpose(tf.transpose(
        #    dist_walls[:, :tf.shape(walls_h)[0]]) - y_pred_s[:, 3]/2)
        d_hor = tf.where(~wall_cell_mask_bool[:, :tf.shape(walls_h)[0]], tf.ones_like(d_hor)*MAX_DIST, d_hor)
        d_hor = tf.where(~wall_h_range, tf.ones_like(d_hor)*MAX_DIST, d_hor)

        pos_mask_h = tf.reshape(y_pred_s[:, 1], [-1, 1]) > tf.reshape(walls_h_center[:, 1], [1, -1])

        d_hor_pos_only = tf.where(pos_mask_h, tf.ones_like(d_hor)*MAX_DIST, d_hor)
        d_hor_neg_only = tf.where(~pos_mask_h, tf.ones_like(d_hor) * MAX_DIST,
                                  d_hor)

        d_ver = tf.abs(
            proj_v[:, :, 0] - tf.reshape(y_pred_s[:, 0], [-1, 1])) - tf.reshape(
            y_pred_s[:, 2] / 2, [-1, 1])
        #d_ver = tf.transpose(tf.transpose(
        #    dist_walls[:, tf.shape(walls_h)[0]:]) - y_pred_s[:, 2]/2)
        d_ver = tf.where(~wall_cell_mask_bool[:, tf.shape(walls_h)[0]:], tf.ones_like(d_ver)*MAX_DIST, d_ver)
        d_ver = tf.where(~wall_v_range, tf.ones_like(d_ver) * MAX_DIST, d_ver)

        # compute center of vertical walls

        walls_v_center_x = tf.reduce_mean(tf.concat(
            [tf.reshape(walls_v[:, 0], [-1, 1]),
             tf.reshape(walls_v[:, 2], [-1, 1])], axis=1), axis=1)
        walls_v_center_y = tf.reduce_mean(tf.concat(
            [tf.reshape(walls_v[:, 1], [-1, 1]),
             tf.reshape(walls_v[:, 3], [-1, 1])], axis=1), axis=1)

        walls_v_center = tf.concat([tf.reshape(walls_v_center_x, [-1, 1]), tf.reshape(walls_v_center_y, [-1, 1])], axis=1)

        pos_mask_v = tf.reshape(y_pred_s[:, 0], [-1, 1]) > tf.reshape(
            walls_v_center[:, 0], [1, -1])

        d_ver_pos_only = tf.where(pos_mask_v, tf.ones_like(d_ver) * MAX_DIST,
                                  d_ver)
        d_ver_neg_only = tf.where(~pos_mask_v, tf.ones_like(d_ver) * MAX_DIST,
                                  d_ver)

        wall_loss_hor = tf.reduce_min(tf.abs(d_hor_pos_only), axis=1) + tf.reduce_min(tf.abs(d_hor_neg_only), axis=1)
        wall_loss_ver = tf.reduce_min(tf.abs(d_ver_pos_only), axis=1) + tf.reduce_min(tf.abs(d_ver_neg_only), axis=1)

        #wall_loss_hor = tf.reduce_min(tf.abs(d_hor), axis=1)
        #wall_loss_ver = tf.reduce_min(tf.abs(d_ver), axis=1)

        wall_loss_sum = tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true), axis=1) * (wall_loss_hor + wall_loss_ver)
        loss += tf.constant(0.1) * wall_loss_sum

        #
        # Angle diff to horizontal walls (inversely weighted by distance)
        #

        angle_diff_scale = tf.transpose(
            tf.transpose(
                1/dist_walls[:, :tf.shape(walls_h)[0]]) *
            tf.reduce_min(
                dist_walls[:, :tf.shape(walls_h)[0]], axis=1))

        angle_diff_scale = tf.where(~wall_cell_mask_bool[:, :tf.shape(walls_h)[0]], tf.zeros_like(angle_diff_scale), angle_diff_scale)

        angle_diff = tf.square(tf.reshape(y_pred_s[:, 4], [-1, 1]) - tf.reshape(walls_h[:, 4], [1, -1]))

        angle_diff_loss = tf.square(angle_diff_scale) * angle_diff

        loss += tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true), axis=1) * tf.reduce_sum(angle_diff_loss, axis=1)

        #
        # following operations are computed with rotated local coordinate system
        #

        # rotate (c_x, c_y) and (t_x, t_y) by omega * PI around center of grid cell
        omega = y_pred_s[:, 4]

        rot_cx = tf.multiply(y_pred_s[:, 0], tf.cos(tf_PI * omega)) - tf.multiply(y_pred_s[:, 1], tf.sin(tf_PI * omega))
        rot_cy = tf.multiply(y_pred_s[:, 1], tf.cos(tf_PI * omega)) + tf.multiply(y_pred_s[:, 0], tf.sin(tf_PI * omega))

        rot_tx = tf.multiply(y_true[:, 0], tf.cos(tf_PI * omega)) - tf.multiply(y_true[:, 1], tf.sin(tf_PI * omega))
        rot_ty = tf.multiply(y_true[:, 1], tf.cos(tf_PI * omega)) + tf.multiply(y_true[:, 0], tf.sin(tf_PI * omega))

        y_true = tf.concat([tf.reshape(rot_tx, [-1, 1]), tf.reshape(rot_ty,[-1, 1]), tf.reshape(y_true[:, 2],[-1, 1])], axis=1)

        y_pred_s = tf.concat([tf.reshape(rot_cx, [-1, 1]), tf.reshape(rot_cy,[-1, 1]), y_pred_s[:, 2:]], axis=1)

        # box loss
        box_loss = tf.zeros_like(y_true[:, 0])
        box_loss_scale = tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true), axis=1)

        # center loss
        c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
                              axis=1)

        box_loss += c_loss

        if "outside" in params:

            delta = 20.0
            if "delta" in params['outside']:
                delta = params['outside']['delta']

            outside_x = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
                tf.divide(y_pred_s[:, 2], tf.constant(delta))),
                tf.constant(0.0))

            outside_y = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
                tf.divide(y_pred_s[:, 3], tf.constant(delta))),
                tf.constant(0.0))

            outside_x_loss = outside_x
            outside_y_loss = outside_y

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            box_loss += outside_loss

        loss += box_loss * box_loss_scale

        if modus == "loss":
            return test
        if modus == "angle_diff_scale":
            return angle_diff_scale#angle_diff_scale
        if modus == "angle_diff":
            return angle_diff
        if modus == "d_hor_pos":
            return d_hor_pos_only
        if modus == "d_hor_neg":
            return d_hor_neg_only
        if modus == "d_ver_pos":
            return d_ver_pos_only
        if modus == "d_ver_neg":
            return d_ver_neg_only
        if modus == "d_ver":
            return d_ver
        if modus == "wall_loss_hor":
            return wall_loss_hor
        if modus == "wall_loss_ver":
            return wall_loss_ver
        if modus == "pos_mask_h":
            return pos_mask_h
        if modus == "pos_mask_v":
            return pos_mask_v
        if modus == "walls_h_center":
            return walls_h_center
        if modus == "proj_h":
            return proj_h
        if modus == "proj_v":
            return proj_v
        if modus == "s_h":
            return s_h
        if modus == "c_h":
            return c_h
        if modus == "walls_h":
            return walls_h
        if modus == "y_pred_s":
            return y_pred_non_rot

    return yolo_loss

def test_rot_los(params, tf_walls_h, tf_walls_v, n_rows, n_cols, gs=40.0, pad_ratio=0.1, modus="loss"):
    import math as m
    tf_PI = tf.constant(m.pi)
    walls_h = tf.constant(tf_walls_h, dtype=tf.float32)
    walls_v = tf.constant(tf_walls_v, dtype=tf.float32)
    MAX_DIST = tf.constant(100, dtype=tf.float32)

    #
    # compute walls with respect to each center?
    # precomputation
    #

    gs = tf.constant(gs, dtype=tf.float32)
    gs_idx = tf.range(0, n_rows * n_cols)
    r_idx = tf.cast(tf.cast(gs_idx / n_cols, tf.int32), tf.float32)
    c_idx = tf.cast(tf.mod(gs_idx, n_cols), tf.float32)
    gs_centers_x = c_idx * gs
    gs_centers_y = r_idx * gs

    gs_centers = tf.concat(
        [tf.reshape(gs_centers_x, [-1, 1]), tf.reshape(gs_centers_y, [-1, 1])],
        axis=1)

    omega_walls = walls_h[:, 4]

    w_h_1 = (walls_h[:, tf.newaxis, :2] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    w_h_2 = (walls_h[:, tf.newaxis, 2:4] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    walls_h = tf.concat([w_h_1, w_h_2], axis=2)

    w_v_1 = (walls_v[:, tf.newaxis, :2] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    w_v_2 = (walls_v[:, tf.newaxis, 2:4] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    walls_v = tf.concat([w_v_1, w_v_2], axis=2)

    walls_all = tf.concat([walls_h, walls_v], axis=0)

    num_wh = tf.shape(walls_h)[0]

    def yolo_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 5::6]

        # scale from [-1,1] to [0,1]
        grid_cell_pred = tf.multiply(tf.add(grid_cell_pred, 1.0), 0.5)

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32),
                                    depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(
            tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_sa = tf.reshape(y_pred, (
            tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 6))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx, tf.int32),
                             axis=1)  # [0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        #
        # SCALE tanh box size output to [0,1]
        #

        zeros = tf.zeros([tf.shape(y_pred_s)[0], 2])
        ones = tf.ones([tf.shape(y_pred_s)[0], 2])
        scale_add = tf.concat(
            [zeros, ones, tf.zeros([tf.shape(y_pred_s)[0], 2])], axis=1)

        y_pred_s = tf.add(y_pred_s, scale_add)

        # 1) compute distance from centers to walls (dist_walls)

        walls = walls_all[:, :, :]

        # walls = tf.ones([tf.shape(y_true)[0], 1]) * walls
        walls = tf.reshape(tf.tile(walls, [tf.shape(y_true)[0], 1, 1]), [tf.shape(y_true)[0], tf.shape(walls)[0], tf.shape(walls)[1], tf.shape(walls)[2]])
        # walls = tf.stack([walls] * tf.shape(y_true)[0], axis=0)

        return walls

    return yolo_loss
        #walls = tf.transpose(walls, [0, 2, 1, 3])



def define_rotated_box_loss_all_walls(params, tf_walls_h, tf_walls_v, n_rows, n_cols, gs=40.0, pad_ratio=0.1, modus="loss"):

    tf_PI = tf.constant(m.pi)
    walls_h = tf.constant(tf_walls_h, dtype=tf.float32)
    walls_v = tf.constant(tf_walls_v, dtype=tf.float32)
    MAX_DIST = tf.constant(100, dtype=tf.float32)

    #
    # compute walls with respect to each center?
    # precomputation
    #

    gs = tf.constant(gs, dtype=tf.float32)
    gs_idx = tf.range(0, n_rows * n_cols)
    r_idx = tf.cast(tf.cast(gs_idx / n_cols, tf.int32), tf.float32)
    c_idx = tf.cast(tf.mod(gs_idx, n_cols), tf.float32)
    gs_centers_x = c_idx * gs
    gs_centers_y = r_idx * gs

    gs_centers = tf.concat(
        [tf.reshape(gs_centers_x, [-1, 1]), tf.reshape(gs_centers_y, [-1, 1])],
        axis=1)

    omega_walls = walls_h[:, 4]

    w_h_1 = (walls_h[:, tf.newaxis, :2] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    w_h_2 = (walls_h[:, tf.newaxis, 2:4] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    walls_h = tf.concat([w_h_1, w_h_2], axis=2)

    w_v_1 = (walls_v[:, tf.newaxis, :2] - gs_centers) / (
                gs / 2.0 + gs * pad_ratio)
    w_v_2 = (walls_v[:, tf.newaxis, 2:4] - gs_centers) / (
                gs / 2.0 + gs * pad_ratio)
    walls_v = tf.concat([w_v_1, w_v_2], axis=2)

    walls_all = tf.concat([walls_h, walls_v], axis=0)

    num_wh = tf.shape(walls_h)[0]

    def yolo_loss(y_true, y_pred):
        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 5::6]

        # scale from [-1,1] to [0,1]
        grid_cell_pred = tf.multiply(tf.add(grid_cell_pred, 1.0), 0.5)

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32),
                                    depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(
            tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_sa = tf.reshape(y_pred, (
        tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 6))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx, tf.int32),
                             axis=1)  # [0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        #
        # SCALE tanh box size output to [0,1]
        #

        zeros = tf.zeros([tf.shape(y_pred_s)[0], 2])
        ones = tf.ones([tf.shape(y_pred_s)[0], 2])
        scale_add = tf.concat(
            [zeros, ones, tf.zeros([tf.shape(y_pred_s)[0], 2])], axis=1)

        y_pred_s = tf.add(y_pred_s, scale_add)

        # 1) compute distance from centers to walls (dist_walls)

        walls = walls_all[:, :, :]

        # repeat walls along first axis for batch-size times
        walls = tf.reshape(tf.tile(walls, [tf.shape(y_true)[0], 1, 1]),
                           [tf.shape(y_true)[0], tf.shape(walls)[0],
                            tf.shape(walls)[1], tf.shape(walls)[2]])
        #walls = tf.stack([walls] * tf.shape(y_true)[0], axis=0)

        walls = tf.transpose(walls, [0,2,1,3])

        gs_selector = tf.cast(tf.reshape(y_true[:, 2], [-1, 1]), tf.int32)
        idx = tf.stack([tf.reshape(tf.range(tf.shape(walls)[0]), (-1, 1)), gs_selector], axis=-1)
        walls = tf.gather_nd(walls, idx)[:, 0, :, :]

        nom = tf.einsum("i,ij->ij", y_pred_s[:, 0], (walls[:, :, 3] - walls[:, :, 1])) - \
              tf.einsum("i,ij->ij", y_pred_s[:, 1], (walls[:, :, 2] - walls[:, :, 0])) + \
              walls[:, :, 2] * walls[:, :, 1] - walls[:, :, 3] * walls[:, :, 0]

        denom = tf.square(walls[:, :, 3] - walls[:, :, 1]) + tf.square(
            walls[:, :, 2] - walls[:, :, 0])

        dist_walls = tf.abs(nom) / tf.sqrt(denom)

        #
        # compute orthogonal projection of center onto walls
        #

        # subtract first wall supporting point from wall coordinates such that
        # wall has first supporting point in center (vector projection)

        s_h = tf.concat([(walls[:, :num_wh, 2] - walls[:, :num_wh, 0])[:, :, tf.newaxis],
                         (walls[:, :num_wh, 3] - walls[:, :num_wh, 1])[:, :, tf.newaxis]],
                        axis=2)

        c_h = y_pred_s[:, tf.newaxis, :2] - walls[:, :num_wh, :2]

        proj_h = tf.einsum('ij,ijk->ijk',
                           tf.einsum('ijk,ijk->ij', c_h, s_h) /
                           tf.einsum('ijk,ijk->ij', s_h, s_h), s_h)

        t1 = tf.einsum('ijk,ijk->ij', c_h, s_h)
        t2 = tf.einsum('ijk,ijk->ij', s_h, s_h)
        t3 = t1 / t2

        # translate back
        proj_h = proj_h + walls[:, :num_wh, :2]

        # check whether projected point lies within bounds of supporting points of lines (in x-dim)
        walls_h_x = tf.concat([(walls[:, :num_wh, 0])[:, :, tf.newaxis],
                               (walls[:, :num_wh, 2])[:, :, tf.newaxis]], axis=2)
        walls_h_x_max = tf.reduce_max(walls_h_x, axis=2)
        walls_h_x_min = tf.reduce_min(walls_h_x, axis=2)

        wall_h_x_max_range = tf.less_equal(proj_h[:, :, 0], walls_h_x_max)
        wall_h_x_min_range = tf.greater_equal(proj_h[:, :, 0], walls_h_x_min)

        wall_h_range = tf.logical_and(wall_h_x_max_range, wall_h_x_min_range)

        s_v = tf.concat(
            [(walls[:, num_wh:, 2] - walls[:, num_wh:, 0])[:, :, tf.newaxis],
             (walls[:, num_wh:, 3] - walls[:, num_wh:, 1])[:, :, tf.newaxis]],
            axis=2)

        c_v = y_pred_s[:, tf.newaxis, :2] - walls[:, num_wh:, :2]

        proj_v = tf.einsum('ij,ijk->ijk',
                           tf.einsum('ijk,ijk->ij', c_v, s_v) /
                           tf.einsum('ijk,ijk->ij', s_v, s_v), s_v)

        # translate back
        proj_v = proj_v + walls[:, num_wh:, :2]

        # check whether projected point lies within bounds of supporting points of lines (in x-dim)
        walls_v_y = tf.concat([(walls[:, num_wh:, 1])[:, :, tf.newaxis],
                               (walls[:, num_wh:, 3])[:, :, tf.newaxis]],
                              axis=2)
        walls_v_y_max = tf.reduce_max(walls_v_y, axis=2)
        walls_v_y_min = tf.reduce_min(walls_v_y, axis=2)

        wall_v_y_max_range = tf.less_equal(proj_v[:, :, 1], walls_v_y_max)
        wall_v_y_min_range = tf.greater_equal(proj_v[:, :, 1], walls_v_y_min)

        wall_v_range = tf.logical_and(wall_v_y_max_range, wall_v_y_min_range)

        # new computation of distance between center and wall using vector projection on wall
        dist_bound_wall_h = tf.abs(proj_h[:, :, 1] - tf.reshape(y_pred_s[:, 1], [-1, 1]))
        d_hor = dist_bound_wall_h - tf.reshape(y_pred_s[:, 3] / 2, [-1, 1])

        d_hor = tf.where(~wall_h_range, tf.ones_like(d_hor) * MAX_DIST, d_hor)
        d_hor = tf.where(dist_bound_wall_h > 1.0, tf.ones_like(d_hor) * MAX_DIST, d_hor)

        pos_mask_h = tf.reshape(y_pred_s[:, 1], [-1, 1]) > proj_h[:, :, 1]

        d_hor_pos_only = tf.where(pos_mask_h, tf.ones_like(d_hor) * MAX_DIST,
                                  d_hor)
        d_hor_neg_only = tf.where(~pos_mask_h, tf.ones_like(d_hor) * MAX_DIST,
                                  d_hor)

        dist_bound_wall_v = tf.abs(proj_v[:, :, 0] - tf.reshape(y_pred_s[:, 0], [-1, 1]))
        d_ver = dist_bound_wall_v - tf.reshape(y_pred_s[:, 2] / 2, [-1, 1])

        d_ver = tf.where(~wall_v_range, tf.ones_like(d_ver) * MAX_DIST, d_ver)
        d_ver = tf.where(dist_bound_wall_v > 1.0,
                         tf.ones_like(d_ver) * MAX_DIST, d_ver)

        pos_mask_v = tf.reshape(y_pred_s[:, 0], [-1, 1]) > proj_v[:, :, 0]

        d_ver_pos_only = tf.where(pos_mask_v, tf.ones_like(d_ver) * MAX_DIST,
                                  d_ver)
        d_ver_neg_only = tf.where(~pos_mask_v, tf.ones_like(d_ver) * MAX_DIST,
                                  d_ver)

        wall_loss_hor_pos = tf.reduce_min(tf.abs(d_hor_pos_only), axis=1)
        wall_loss_hor_neg = tf.reduce_min(tf.abs(d_hor_neg_only), axis=1)
        wall_loss_hor_pos = tf.where(wall_loss_hor_pos == MAX_DIST, tf.zeros_like(wall_loss_hor_pos), wall_loss_hor_pos)
        wall_loss_hor_neg = tf.where(wall_loss_hor_neg == MAX_DIST,
                                     tf.zeros_like(wall_loss_hor_neg),
                                     wall_loss_hor_neg)
        wall_loss_hor = wall_loss_hor_pos + wall_loss_hor_neg

        wall_loss_ver_pos = tf.reduce_min(tf.abs(d_ver_pos_only), axis=1)
        wall_loss_ver_neg = tf.reduce_min(tf.abs(d_ver_neg_only), axis=1)
        wall_loss_ver_pos = tf.where(wall_loss_ver_pos == MAX_DIST,
                                     tf.zeros_like(wall_loss_ver_pos),
                                     wall_loss_ver_pos)
        wall_loss_ver_neg = tf.where(wall_loss_ver_neg == MAX_DIST,
                                     tf.zeros_like(wall_loss_ver_neg),
                                     wall_loss_ver_neg)
        wall_loss_ver = wall_loss_ver_pos + wall_loss_ver_neg

        wall_loss_sum = tf.reduce_sum(
            tf.multiply(grid_cell_pred, grid_cell_true), axis=1) * (
                                    wall_loss_hor + wall_loss_ver)

        loss += wall_loss_sum * tf.constant(0.1)

        #
        # Angle diff to horizontal walls (inversely weighted by distance)
        #

        min_dist_wall = tf.reduce_min(np.abs(d_hor), axis=1)
        min_dist_wall = tf.where(min_dist_wall >= MAX_DIST, tf.zeros_like(min_dist_wall), min_dist_wall)

        angle_diff_scale = tf.square( (1.0 / d_hor) * min_dist_wall[:, tf.newaxis] )

        angle_diff = tf.square(y_pred_s[:, 4, tf.newaxis] - omega_walls[tf.newaxis, :])

        angle_diff_loss = tf.reduce_sum(angle_diff_scale * angle_diff, axis=1)

        loss += tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true),
                              axis=1) * angle_diff_loss

        #
        # following operations are computed with rotated local coordinate system
        #

        # rotate (c_x, c_y) and (t_x, t_y) by omega * PI around center of grid cell
        omega = y_pred_s[:, 4]

        rot_cx = tf.multiply(y_pred_s[:, 0],
                             tf.cos(tf_PI * omega)) - tf.multiply(
            y_pred_s[:, 1], tf.sin(tf_PI * omega))
        rot_cy = tf.multiply(y_pred_s[:, 1],
                             tf.cos(tf_PI * omega)) + tf.multiply(
            y_pred_s[:, 0], tf.sin(tf_PI * omega))

        rot_tx = tf.multiply(y_true[:, 0], tf.cos(tf_PI * omega)) - tf.multiply(
            y_true[:, 1], tf.sin(tf_PI * omega))
        rot_ty = tf.multiply(y_true[:, 1], tf.cos(tf_PI * omega)) + tf.multiply(
            y_true[:, 0], tf.sin(tf_PI * omega))

        y_true = tf.concat(
            [tf.reshape(rot_tx, [-1, 1]), tf.reshape(rot_ty, [-1, 1]),
             tf.reshape(y_true[:, 2], [-1, 1])], axis=1)

        y_pred_s = tf.concat(
            [tf.reshape(rot_cx, [-1, 1]), tf.reshape(rot_cy, [-1, 1]),
             y_pred_s[:, 2:]], axis=1)

        # box loss
        box_loss = tf.zeros_like(y_true[:, 0])
        box_loss_scale = tf.reduce_sum(
            tf.multiply(grid_cell_pred, grid_cell_true), axis=1)

        # center loss
        c_loss = tf.reduce_sum(
            tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
            axis=1)

        box_loss += c_loss

        if "outside" in params:

            delta = 20.0
            if "delta" in params['outside']:
                delta = params['outside']['delta']

            outside_x = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
                tf.divide(y_pred_s[:, 2], tf.constant(delta))),
                tf.constant(0.0))

            outside_y = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
                tf.divide(y_pred_s[:, 3], tf.constant(delta))),
                tf.constant(0.0))

            outside_x_loss = outside_x
            outside_y_loss = outside_y

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            box_loss += outside_loss

        loss += box_loss * box_loss_scale

        if modus == "loss":
            return loss
        #if modus == "dist_walls":
        #    return dist_walls
        if modus == "walls_h":
            return walls[:tf.shape(walls_h)[0]]
        if modus == "walls_v":
            return walls[tf.shape(walls_h)[0]:]
        if modus == "walls":
            return walls
        if modus == "wall_h_range":
            return wall_h_range
        if modus == "angle_diff_scale":
           return angle_diff_scale#angle_diff_scale
        if modus == "angle_diff":
            return angle_diff
        if modus == "angle_diff_loss":
            return angle_diff_loss
        if modus == "d_hor_pos":
            return d_hor_pos_only
        if modus == "d_hor_neg":
            return d_hor_neg_only
        if modus == "d_ver_pos":
            return d_ver_pos_only
        if modus == "d_ver_neg":
            return d_ver_neg_only
        if modus == "d_ver":
            return d_ver
        if modus == "wall_loss_hor":
            return wall_loss_hor
        if modus == "wall_loss_ver":
            return wall_loss_ver
        if modus == "pos_mask_h":
            return pos_mask_h
        if modus == "pos_mask_v":
            return pos_mask_v
        if modus == "proj_h":
            return proj_h
        if modus == "proj_v":
            return proj_v
        if modus == "s_h":
            return s_h
        if modus == "c_h":
            return c_h
        if modus == "walls_h":
            return walls_h
        if modus == "t1":
            return t1
        if modus == "t2":
            return t2
        if modus == "t3":
            return t3
        if modus == "d_hor":
            return d_hor

    return yolo_loss


def define_rot_box_loss_modular(params, tf_walls_h, tf_walls_v, n_rows, n_cols, gs=40.0, pad_ratio=0.1):
    tf_PI = tf.constant(m.pi)
    walls_h = tf.constant(tf_walls_h, dtype=tf.float32)
    walls_v = tf.constant(tf_walls_v, dtype=tf.float32)
    MAX_DIST = tf.constant(100, dtype=tf.float32)
    omega_walls = walls_h[:, 4]

    # encode walls with respect to each grid cell origin
    walls_h, walls_v, num_wh = compute_multi_origin_wall_encoding(
        gs, n_rows, n_cols, pad_ratio, walls_h, walls_v)

    walls_all = tf.concat([walls_h, walls_v], axis=0)

    def yolo_loss(y_true, y_pred):
        #
        # Setup
        #

        loss = tf.zeros_like(y_true[:, 0])

        grid_cell_true, grid_cell_pred = get_grid_cell_values(y_true, y_pred)

        # get the box prediction with highest confidence
        # (used for subsequent loss computations)
        y_pred_s = get_highest_confidence_prediction(y_true, y_pred)

        # get the encoded walls with origin equal to grid cell center of y_true
        walls = get_walls_encoding_with_respect_to_y_true_grid_cell(walls_all,
                                                                    y_true)

        d_hor_pos_only, d_hor_neg_only, d_hor, \
        d_ver_pos_only, d_ver_neg_only, _ = \
            get_distance_between_wall_bound_and_projection_point(
                y_pred_s, walls, num_wh, MAX_DIST)
        #
        # Classification loss: is the right grid cell chosen?
        #

        loss += class_grid_cell_loss(y_true, y_pred, params)

        #
        # Wall loss: minimize distance between box boundary and closest wall
        #

        loss += get_wall_loss(d_hor_pos_only, d_ver_neg_only, d_ver_pos_only,
                              d_ver_neg_only, grid_cell_true, grid_cell_pred,
                              MAX_DIST, scale=1.0)

        #
        # Angle loss: orientation should adapt to local wall structure
        # (only uses horizontal walls)
        #

        loss += get_angle_loss(d_hor, y_pred_s, omega_walls, grid_cell_true, grid_cell_pred, MAX_DIST)

        #
        # Box loss: classic box loss (requires previous rotation of values)
        #

        y_true, y_pred_s = get_rotated_values(y_true, y_pred_s, tf_PI)

        loss += get_box_loss(y_true, y_pred_s, grid_cell_true, grid_cell_pred, params)

        return loss

    return yolo_loss


def define_yolo_loss(params):
    def yolo_loss(y_true, y_pred):

        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 4::5]

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32), depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_sa = tf.reshape(y_pred, (tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 5))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx,tf.int32), axis=1)#[0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        # box loss
        box_loss = tf.zeros_like(y_true[:, 0])
        box_loss_scale = tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true))

        # center loss
        c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
                              axis=1)

        box_loss += c_loss

        if "size" in params:
            size_loss = tf.reduce_sum(tf.square(y_pred_s[:, 2:]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]
                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            box_loss += size_loss

        if "outside" in params:
            outside_x = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
                tf.divide(y_pred_s[:, 2], tf.constant(3.0))),
                tf.constant(0.0))

            outside_y = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
                tf.divide(y_pred_s[:, 3], tf.constant(3.0))),
                tf.constant(0.0))

            outside_x_loss = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y_loss = tf.maximum(tf.zeros_like(outside_y), outside_y)

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)
            #outside_loss = outside_x_loss + outside_y_loss

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            box_loss += outside_loss

        loss += box_loss * box_loss_scale

        return loss

    return yolo_loss


def define_yolo_loss_backup(params):
    def yolo_loss(y_true, y_pred):

        loss = tf.zeros_like(y_true[:, 0])

        # grid cell classification loss
        grid_cell_pred = y_pred[:, 4::5]

        # grid cell true
        grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32), depth=tf.shape(grid_cell_pred)[1])

        class_loss = tf.reduce_sum(tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

        if "scale" in params["grid"]:
            factor = params["grid"]["scale"]
            class_loss = tf.multiply(class_loss, tf.constant(factor))

        loss += class_loss

        # pred subset
        g_idx = y_true[:, 2]

        # convert to 3-dim tensor
        y_pred_s = tf.reshape(y_pred, (tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 5))

        # gather along 2nd dimension (Grids)
        grid_sub = tf.gather(y_pred_s, tf.cast(g_idx,tf.int32), axis=1)#[0,0,:]

        # Read dimensions
        # (see: https://stackoverflow.com/questions/57387169/
        # getting-the-diagonal-elements-of-only-part-of-a-tensor)

        s = tf.shape(grid_sub)
        # Make indices for gathering
        ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
        idx = tf.stack([ii, ii, jj], axis=-1)

        # Gather result
        y_pred_s = tf.gather_nd(grid_sub, idx)

        # center loss
        c_loss = tf.reduce_sum(tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
                              axis=1)

        loss += c_loss

        if "size" in params:
            size_loss = tf.reduce_sum(tf.square(y_pred_s[:, 2:]), axis=1)

            if "threshold" in params["size"]:
                # only count size loss if greater than 50.0
                threshold = params["size"]["threshold"]
                size_loss = tf.maximum(tf.zeros_like(size_loss),
                                       tf.subtract(size_loss, tf.multiply(
                                           tf.ones_like(size_loss), threshold)))

            if "scale" in params["size"]:
                factor = params["size"]["scale"]
                size_loss = tf.multiply(size_loss, tf.constant(factor))

            loss += size_loss

        if "outside" in params:
            outside_x = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
                tf.divide(y_pred_s[:, 2], tf.constant(3.0))),
                tf.constant(0.0))

            outside_y = tf.subtract(tf.subtract(
                tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
                tf.divide(y_pred_s[:, 3], tf.constant(3.0))),
                tf.constant(0.0))

            outside_x_loss = tf.maximum(tf.zeros_like(outside_x), outside_x)
            outside_y_loss = tf.maximum(tf.zeros_like(outside_y), outside_y)

            outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)

            if "scale" in params["outside"]:
                factor = params["outside"]["scale"]
                outside_loss = tf.multiply(outside_loss, tf.constant(factor))

            loss += outside_loss

        return y_pred_s # loss

    return yolo_loss

def test_quantile_loss():
    init = tf.global_variables_initializer()

    y_true = np.array([[4, 3], [9, 2], [3, 1], [2, 2], [7, 2], [100, 100]])

    y_pred = np.array(
        [[3, 2, 6, 4], [8, 0, 1, 3], [10, 2, 4, 1], [3, 1, 2, 1],
         [8, 1, 3, 5], [50, 50, 75, 75]])

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    params = {
        "upper": 0.9,
        "lower": 0.1}  # , "size": {"threshold": 0.0, "scale": 1.0}, "outside": {"scale": 1.0}}

    loss_func = define_quantile_loss(params)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        print(sess.run(loss_func(y_true, y_pred)))
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()

def test_hier_loss():
    init = tf.global_variables_initializer()

    y_true = np.array([[0, 0.2, 0.1], [0, 0.1, -0.3], [1, 0.1, 0.0]])

    y_pred = np.array([[0.8, 0.1, 0.1, 0.0], [0.7, 0.2, 0.1, 0.0], [0.5, 0.3, 0.1, 0.1]])

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        print(sess.run(hier_loss(y_true, y_pred)))
        #writer = tf.summary.FileWriter("output", sess.graph)

        #writer.close()


def compute_tf_all_wall_loss(y_true, y_pred, walls_h, walls_v, n_rows=3, n_cols=6, gs=40.0, pad_ratio=0.1, modus="loss"):
    init = tf.global_variables_initializer()

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    loss_func = define_rotated_box_loss_all_walls(
        params=dict(grid={"scale": 20.0},
                    outside={"scale": 0.1,
                             "delta": 20.0}),
        tf_walls_h=walls_h, tf_walls_v=walls_v,
        n_rows=n_rows, n_cols=n_cols, gs=gs,
        pad_ratio=pad_ratio, modus=modus)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        result = sess.run(loss_func(y_true, y_pred))
        print(result)
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()

    return result

def compute_tf_loss(y_true, y_pred, walls_h, walls_v, modus="loss"):
    init = tf.global_variables_initializer()

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    loss_func = define_rotated_box_loss(
        params=dict(grid={"scale": 20.0},
                    outside={"scale": 0.1,
                             "delta": 20.0}),
        tf_walls_h=walls_h, tf_walls_v=walls_v, modus=modus)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        result = sess.run(loss_func(y_true, y_pred))
        print(result)
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()

    return result

def test_yolo_loss():
    init = tf.global_variables_initializer()

    y_true = np.array([[0.6, 0.3, 0], [0.1, 0.05, 1]])

    y_pred = np.array([[0.7, 0.3, 0.5, 0.2, 0, 0.6, 0.4, 0.2, 0.9, 0.1,0, -0.4], [0.3, 0.2, 0.4, 0.1, 0, -0.2, 0.2, 0.2, 0.6, 0.1,0, -0.8]])

    walls_h = np.array([[-0.7, 0.2, 0.5, 0.2, 0, 1], [0.6, 0.4, 0.2, 0.9, 0.1, 0], [-0.7, 0.2, 0.5, 0.1, -0.5, 0]])
    walls_v = np.array([[-0.7, 0.2, 0.5, 0.1, 1], [-0.6, 0.4, 0.2, 0.9, 1]])

    #c_sub = y_pred[:, [0::, 1]*5]

    #test = y_pred[[0,1,0,0], :]
    #print(r[:, [0,1], :][[0,1]])

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    loss_func = define_rotated_box_loss(
        params=dict(grid={"scale": 4.0},
                    size={"scale": 0.0055},
                    outside={"scale": 0.1}),
        tf_walls_h=walls_h, tf_walls_v=walls_v)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        print(sess.run(loss_func(y_true, y_pred)))
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()


def test_piecewise_loss():
    init = tf.global_variables_initializer()

    y_true = np.array([[4, 3], [9, 2], [3, 1], [2, 2], [7, 2]])

    y_pred = np.array(
        [[4, 3, 2, 3], [8, 0, 6, 3], [10, 2, 8, 4], [3, 1, 7, 3],
         [8, 1, 12, 4]])

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    params = {"center": "ML2", "size": {"threshold": 0.0, "scale": 1.0},
              "piecewise": {"scale": 1.0}}

    loss_func = define_bbox_loss_piecewise(params)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        print(sess.run(loss_func(y_true, y_pred)))
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()

def test_center_loss():

    init = tf.global_variables_initializer()

    y_true = np.array([[4, 3], [9, 2], [3, 1], [2, 2], [7, 2]])

    y_pred = np.array(
        [[4, 3, 2, 3], [8, 0, 6, 3], [10, 2, 8, 4], [3, 1, 7, 3],
         [8, 1, 12, 4]])

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    params = {"center": "ML2", "size": {"threshold": 0.0, "scale": 1.0}, "outside": {"scale": 1.0}}

    loss_func = define_bbox_loss(params)

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        print(sess.run(loss_func(y_true, y_pred)))
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()


def center_loss(y_true, y_pred):
    c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)
    size_loss = tf.abs(tf.reduce_sum(y_pred[:, 2:], axis=1))
    return c_loss + tf.multiply(size_loss, tf.constant(10.0))


def bbox_loss(y_true, y_pred):

    c_loss = tf.linalg.norm(tf.subtract(y_true, y_pred[:, :2]), axis=1)

    size_loss = tf.abs(tf.reduce_sum(y_pred[:, 2:], axis=1))

    outside_x = tf.subtract(tf.abs(tf.subtract(y_true[:, 0], y_pred[:, 0])),
                            tf.divide(y_pred[:, 2], tf.constant(2.0)))
    outside_y = tf.subtract(tf.abs(tf.subtract(y_true[:, 1], y_pred[:, 1])),
                            tf.divide(y_pred[:, 3], tf.constant(2.0)))

    outside_x_loss = tf.maximum(tf.zeros_like(outside_x), outside_x)
    outside_y_loss = tf.maximum(tf.zeros_like(outside_y), outside_y)

    outside_loss = outside_x_loss + outside_y_loss

    # only count size loss if greater than 50.0
    size_loss = tf.maximum(tf.zeros_like(size_loss), tf.subtract(size_loss, tf.multiply(tf.ones_like(size_loss), 30.0)))

    return c_loss + tf.multiply(outside_loss, tf.constant(5.0)) + tf.multiply(size_loss, tf.constant(0.5))

def bound_loss(y_true, y_pred):

    x_diff = tf.subtract(y_pred[:, 0], tf.divide(y_pred[:, 2], tf.constant(2.0)))
    x_left = tf.subtract(y_pred[:, 0], x_diff)
    x_right = tf.add(y_pred[:, 0], x_diff)

    y_diff = tf.subtract(y_pred[:, 1], tf.divide(y_pred[:, 2], tf.constant(2.0)))

    #x_loss_left = tf.subtract(y_true[:, 0], tf.subtract(y_pred[]))

def test_custom_loss():

    init = tf.global_variables_initializer()

    y_true = np.array([[4, 3], [9, 2], [3,1], [2, 2], [7,2]])

    y_pred = np.array(
        [[3, 2, 5, 4], [8, 0, 6, 3], [10, 2, 8, 4], [3,1, 7,3], [8,1,12,4]])

    custom_loss_python(y_true, y_pred)

    y_true = tf.constant(y_true, dtype=tf.float32)
    y_pred = tf.constant(y_pred, dtype=tf.float32)

    walls_h = np.array([[1, 0, 1, 5], [3, 0, 3, 5]])
    walls_v = np.array([[0, 4, 5, 4]])

    custom_loss = custom_loss_tf

    with tf.Session() as sess:
        # Run the initializer on `w`.
        sess.run(init)
        print(sess.run(custom_loss(y_true, y_pred)))
        writer = tf.summary.FileWriter("output", sess.graph)

        writer.close()

def custom_loss_tf(y_true, y_pred):
    # check whether true label in bbox

    # x-coord
    x_left = tf.less_equal(y_true[:, 0], tf.maximum(y_pred[:, 2], y_pred[:, 0]))
    x_right = tf.less_equal(tf.minimum(y_pred[:, 0], y_pred[:, 2]), y_true[:, 0])

    # y-coord
    y_bottom = tf.less_equal(y_true[:, 1], tf.maximum(y_pred[:, 3], y_pred[:, 1]))
    y_top = tf.less_equal(tf.minimum(y_pred[:, 1], y_pred[:, 3]), y_true[:, 1])

    x_inside = tf.logical_and(x_left, x_right)
    y_inside = tf.logical_and(y_bottom, y_top)

    inside_bbox = tf.logical_and(x_inside, y_inside)

    zeros = tf.zeros(tf.shape(y_true)[0])
    penalty = tf.fill([tf.shape(y_true)[0]], 100.0)
    outside_bbox_penalty = tf.where(inside_bbox, zeros, penalty)

    #
    # dist to center
    #

    bbox_center_x = tf.divide((tf.subtract(y_pred[:, 2], y_pred[:, 0])), tf.constant(2.0))
    bbox_center_y = tf.divide((tf.subtract(y_pred[:, 3], y_pred[:, 1])), tf.constant(2.0))

    dist_to_center_x = tf.math.abs(tf.subtract(bbox_center_x, np.subtract(y_true[:, 0], y_pred[:, 0])))
    dist_to_center_y = tf.math.abs(tf.subtract(bbox_center_y, np.subtract(y_true[:, 1], y_pred[:, 1])))

    dist_to_center = tf.sqrt(tf.add(tf.square(dist_to_center_x), tf.square(dist_to_center_y)))

    #dist_to_center = tf.add(dist_to_center_x, dist_to_center_y)

    #
    # calc size of bbox
    #

    x_length = tf.subtract(y_pred[:, 2], y_pred[:, 0])
    y_length = tf.subtract(y_pred[:, 3], y_pred[:, 1])
    bbox_size = tf.add(tf.math.abs(x_length), tf.math.abs(y_length))

    #
    # offset from BBox
    #

    r_off = tf.subtract(y_true[:, 0],
                        tf.math.maximum(y_pred[:, 0], y_pred[:, 2]))
    l_off = tf.subtract(tf.math.minimum(y_pred[:, 0], y_pred[:, 2]),
                        y_true[:, 0])
    t_off = tf.subtract(y_true[:, 1],
                        tf.math.maximum(y_pred[:, 1], y_pred[:, 3]))
    b_off = tf.subtract(tf.math.minimum(y_pred[:, 1], y_pred[:, 3]),
                        y_true[:, 1])

    # check if any x_off greater 0
    r_off = tf.math.maximum(r_off, tf.zeros_like(r_off))
    l_off = tf.math.maximum(l_off, tf.zeros_like(l_off))
    t_off = tf.math.maximum(t_off, tf.zeros_like(t_off))
    b_off = tf.math.maximum(b_off, tf.zeros_like(b_off))

    # scale offset
    scale_fac = tf.constant(10.0)
    r_off = tf.multiply(r_off, scale_fac)
    l_off = tf.multiply(l_off, scale_fac)
    t_off = tf.multiply(t_off, scale_fac)
    b_off = tf.multiply(b_off, scale_fac)

    bbox_off = tf.add_n([r_off, l_off, t_off, b_off])

    # bbox size loss
    mean_bbox_off = tf.reduce_mean(bbox_off)
    scale_factor = tf.divide(tf.constant(1.0), mean_bbox_off)
    bbox_size_loss = tf.multiply(bbox_size, tf.constant(0.2))

    #
    # final loss
    #

    loss = dist_to_center + bbox_off + bbox_size_loss# + outside_bbox_penalty

    return loss

def custom_loss_python(y_true, y_pred):
    # check if correct bbox
    x_correct = np.less(y_pred[:, 0], y_pred[:, 2])
    y_correct = np.less(y_pred[:, 1], y_pred[:, 3])

    correct_bbox = np.logical_and(x_correct, y_correct)

    # check whether true label in bbox

    # x-coord
    x_left = np.less_equal(y_true[:, 0], y_pred[:, 2])
    x_right = np.less_equal(y_pred[:, 0], y_true[:, 0])

    # y-coord
    y_bottom = np.less_equal(y_true[:, 1], y_pred[:, 3])
    y_top = np.less_equal(y_pred[:, 1], y_true[:, 1])

    x_inside = np.logical_and(x_left, x_right)
    y_inside = np.logical_and(y_bottom, y_top)

    inside_bbox = np.logical_and(x_inside, y_inside)

    # calc size of bbox
    x_length = np.subtract(y_pred[:, 2], y_pred[:, 0])
    y_length = np.subtract(y_pred[:, 3], y_pred[:, 1])
    bbox_size = np.multiply(x_length, y_length)

    # bbox loss
    wrong_bbox_penalty = np.full(len(y_pred), 100)
    wrong_bbox_penalty[np.where(correct_bbox)[0]] = 0

    # inside bbox loss
    inside_bbox_penalty = np.full(len(y_pred), 10)
    inside_bbox_penalty[np.where(inside_bbox)[0]] = 0

    # bbox size loss
    bbox_size_loss = bbox_size

    # final loss
    loss = bbox_size_loss + wrong_bbox_penalty + inside_bbox_penalty

    print(loss)

def test():



    #a = tf.Variable([1, 2, 3, 1])
    a = tf.constant(4, shape=[4])
    #a = tf.fill([4], 4)
    start_op = tf.global_variables_initializer()
    comparison = tf.equal(a, tf.constant(1))
    conditional_assignment_op = a.assign(
        tf.where(comparison, tf.zeros_like(a), a))

    with tf.Session() as session:
        # Equivalent to: a = np.array( [1, 2, 3, 1] )
        session.run(a)
        print(a.eval())

        session.run(start_op)
        print(a.eval())
        # Equivalent to: a[a==1] = 0
        session.run(conditional_assignment_op)
        print(a.eval())

    # Output is:
    # [1 2 3 1]
    # [0 2 3 0]


def get_grid_cell_values(y_true, y_pred):
    # grid cell classification loss
    grid_cell_pred = y_pred[:, 5::6]

    # scale from [-1,1] to [0,1]
    grid_cell_pred = tf.multiply(tf.add(grid_cell_pred, 1.0), 0.5)

    # grid cell true
    grid_cell_true = tf.one_hot(tf.cast(y_true[:, 2], dtype=tf.int32),
                                depth=tf.shape(grid_cell_pred)[1])

    return grid_cell_true, grid_cell_pred


def class_grid_cell_loss(y_true, y_pred, params):
    # grid cell classification loss

    grid_cell_true, grid_cell_pred = get_grid_cell_values(y_true, y_pred)

    class_loss = tf.reduce_sum(
        tf.squared_difference(grid_cell_pred, grid_cell_true), axis=1)

    if "scale" in params["grid"]:
        factor = params["grid"]["scale"]
        class_loss = tf.multiply(class_loss, tf.constant(factor))

    return class_loss


def compute_multi_origin_wall_encoding(gs, n_rows, n_cols, pad_ratio, walls_h, walls_v):
    gs = tf.constant(gs, dtype=tf.float32)
    gs_idx = tf.range(0, n_rows * n_cols)
    r_idx = tf.cast(tf.cast(gs_idx / n_cols, tf.int32), tf.float32)
    c_idx = tf.cast(tf.mod(gs_idx, n_cols), tf.float32)
    gs_centers_x = c_idx * gs
    gs_centers_y = r_idx * gs

    gs_centers = tf.concat(
        [tf.reshape(gs_centers_x, [-1, 1]), tf.reshape(gs_centers_y, [-1, 1])],
        axis=1)

    #omega_walls = walls_h[:, 4]

    w_h_1 = (walls_h[:, tf.newaxis, :2] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    w_h_2 = (walls_h[:, tf.newaxis, 2:4] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    walls_h = tf.concat([w_h_1, w_h_2], axis=2)

    w_v_1 = (walls_v[:, tf.newaxis, :2] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    w_v_2 = (walls_v[:, tf.newaxis, 2:4] - gs_centers) / (
            gs / 2.0 + gs * pad_ratio)
    walls_v = tf.concat([w_v_1, w_v_2], axis=2)

    #walls_all = tf.concat([walls_h, walls_v], axis=0)

    num_wh = tf.shape(walls_h)[0]

    return walls_h, walls_v, num_wh


def get_highest_confidence_prediction(y_true, y_pred, ):
    # pred subset
    g_idx = y_true[:, 2]

    grid_cell_true, grid_cell_pred = get_grid_cell_values(y_true, y_pred)

    # convert to 3-dim tensor
    y_pred_sa = tf.reshape(y_pred, (
        tf.shape(y_true)[0], tf.shape(grid_cell_pred)[1], 6))

    # gather along 2nd dimension (Grids)
    grid_sub = tf.gather(y_pred_sa, tf.cast(g_idx, tf.int32),
                         axis=1)  # [0,0,:]

    # Read dimensions
    # (see: https://stackoverflow.com/questions/57387169/
    # getting-the-diagonal-elements-of-only-part-of-a-tensor)

    s = tf.shape(grid_sub)
    # Make indices for gathering
    ii, jj = tf.meshgrid(tf.range(s[0]), tf.range(s[2]), indexing='ij')
    idx = tf.stack([ii, ii, jj], axis=-1)

    # Gather result
    y_pred_s = tf.gather_nd(grid_sub, idx)

    #
    # SCALE tanh box size output to [0,1]
    #

    zeros = tf.zeros([tf.shape(y_pred_s)[0], 2])
    ones = tf.ones([tf.shape(y_pred_s)[0], 2])
    scale_add = tf.concat(
        [zeros, ones, tf.zeros([tf.shape(y_pred_s)[0], 2])], axis=1)

    y_pred_s = tf.add(y_pred_s, scale_add)

    return y_pred_s


def get_walls_encoding_with_respect_to_y_true_grid_cell(walls, y_true):
    # 1) compute distance from centers to walls (dist_walls)

    #walls = walls_all[:, :, :]

    # repeat walls along first axis for batch-size times
    walls = tf.reshape(tf.tile(walls, [tf.shape(y_true)[0], 1, 1]),
                       [tf.shape(y_true)[0], tf.shape(walls)[0],
                        tf.shape(walls)[1], tf.shape(walls)[2]])
    # walls = tf.stack([walls] * tf.shape(y_true)[0], axis=0)

    walls = tf.transpose(walls, [0, 2, 1, 3])

    gs_selector = tf.cast(tf.reshape(y_true[:, 2], [-1, 1]), tf.int32)
    idx = tf.stack(
        [tf.reshape(tf.range(tf.shape(walls)[0]), (-1, 1)), gs_selector],
        axis=-1)
    walls = tf.gather_nd(walls, idx)[:, 0, :, :]

    return walls


def get_orthogonal_projection_of_points_to_wall(y_pred_s, walls, num_wh, horizontal_walls=True):
    #
    # compute orthogonal projection of center onto walls
    #

    # subtract first wall supporting point from wall coordinates such that
    # wall has first supporting point in center (vector projection)

    if horizontal_walls:
        walls = walls[:, :num_wh, :]
    else:
        walls = walls[:, num_wh:, :]

    s_h = tf.concat(
        [(walls[:, :, 2] - walls[:, :, 0])[:, :, tf.newaxis],
         (walls[:, :, 3] - walls[:, :, 1])[:, :, tf.newaxis]],
        axis=2)

    c_h = y_pred_s[:, tf.newaxis, :2] - walls[:, :, :2]

    proj = tf.einsum('ij,ijk->ijk',
                       tf.einsum('ijk,ijk->ij', c_h, s_h) /
                       tf.einsum('ijk,ijk->ij', s_h, s_h), s_h)

    # translate back
    proj = proj + walls[:, :, :2]

    return proj


def get_range_mask_of_walls(proj, walls, num_wh, horizontal_walls=True):
    if horizontal_walls:
        walls = walls[:, :num_wh, :]
        wall_idx = [0, 2]
        w_idx = 0
    else:
        walls = walls[:, num_wh:, :]
        wall_idx = [1, 3]
        w_idx = 1

    walls_x = tf.concat([(walls[:, :, wall_idx[0]])[:, :, tf.newaxis],
                           (walls[:, :, wall_idx[1]])[:, :, tf.newaxis]], axis=2)
    walls_x_max = tf.reduce_max(walls_x, axis=2)
    walls_x_min = tf.reduce_min(walls_x, axis=2)

    wall_x_max_range = tf.less_equal(proj[:, :, w_idx], walls_x_max)
    wall_x_min_range = tf.greater_equal(proj[:, :, w_idx], walls_x_min)

    wall_range = tf.logical_and(wall_x_max_range, wall_x_min_range)

    return wall_range


def _get_distance_between_wall_bound_and_projection_point(proj, y_pred_s, wall_range, MAX_DIST, horizontal_walls=True):

    if horizontal_walls:
        p_idx = 1
        w_idx = 3
    else:
        p_idx = 0
        w_idx = 2

    # new computation of distance between center and wall
    # using vector projection on wall
    dist_bound_wall = tf.abs(
        proj[:, :, p_idx] - tf.reshape(y_pred_s[:, p_idx], [-1, 1]))
    d = dist_bound_wall - tf.reshape(y_pred_s[:, w_idx] / 2, [-1, 1])

    d = tf.where(~wall_range, tf.ones_like(d) * MAX_DIST, d)
    d = tf.where(dist_bound_wall > 1.0, tf.ones_like(d) * MAX_DIST, d)

    pos_mask = tf.reshape(y_pred_s[:, p_idx], [-1, 1]) > proj[:, :, p_idx]

    d_pos_only = tf.where(pos_mask, tf.ones_like(d) * MAX_DIST, d)
    d_neg_only = tf.where(~pos_mask, tf.ones_like(d) * MAX_DIST, d)

    return d_pos_only, d_neg_only, d


def get_distance_between_wall_bound_and_projection_point(y_pred_s, walls, num_wh, MAX_DIST):
    # compute orthogonal projection of the box prediction points onto walls
    proj_h = get_orthogonal_projection_of_points_to_wall(
        y_pred_s, walls, num_wh, horizontal_walls=True)

    proj_v = get_orthogonal_projection_of_points_to_wall(
        y_pred_s, walls, num_wh, horizontal_walls=False)

    wall_h_range = get_range_mask_of_walls(proj_h, walls, num_wh,
                                           horizontal_walls=True)
    wall_v_range = get_range_mask_of_walls(proj_v, walls, num_wh,
                                           horizontal_walls=False)

    d_hor_pos_only, d_hor_neg_only, d_hor = _get_distance_between_wall_bound_and_projection_point(
        proj_h, y_pred_s, wall_h_range, MAX_DIST, horizontal_walls=True)

    d_ver_pos_only, d_ver_neg_only, d_ver = _get_distance_between_wall_bound_and_projection_point(
        proj_v, y_pred_s, wall_v_range, MAX_DIST, horizontal_walls=False)

    return d_hor_pos_only, d_hor_neg_only, d_hor, d_ver_pos_only, d_ver_neg_only, d_ver


def _get_wall_loss(d_pos_only, d_neg_only, MAX_DIST):
    wall_loss_pos = tf.reduce_min(tf.abs(d_pos_only), axis=1)
    wall_loss_neg = tf.reduce_min(tf.abs(d_neg_only), axis=1)
    wall_loss_pos = tf.where(wall_loss_pos == MAX_DIST,
                                 tf.zeros_like(wall_loss_pos),
                                 wall_loss_pos)
    wall_loss_neg = tf.where(wall_loss_neg == MAX_DIST,
                                 tf.zeros_like(wall_loss_neg),
                                 wall_loss_neg)
    wall_loss = wall_loss_pos + wall_loss_neg

    return wall_loss


def get_wall_loss(d_hor_pos_only, d_hor_neg_only, d_ver_pos_only,
                  d_ver_neg_only, grid_cell_true, grid_cell_pred, MAX_DIST, scale=0.1):
    wall_loss_hor = _get_wall_loss(d_hor_pos_only, d_hor_neg_only, MAX_DIST)
    wall_loss_ver = _get_wall_loss(d_ver_pos_only, d_ver_neg_only, MAX_DIST)

    wall_loss_sum = tf.reduce_sum(
        tf.multiply(grid_cell_pred, grid_cell_true), axis=1) * (
                            wall_loss_hor + wall_loss_ver)

    return wall_loss_sum * tf.constant(scale)


def get_angle_loss(d_hor, y_pred_s, omega_walls, grid_cell_true, grid_cell_pred, MAX_DIST):
    #
    # Angle diff to horizontal walls (inversely weighted by distance)
    #

    min_dist_wall = tf.reduce_min(np.abs(d_hor), axis=1)
    min_dist_wall = tf.where(min_dist_wall >= MAX_DIST,
                             tf.zeros_like(min_dist_wall), min_dist_wall)

    angle_diff_scale = tf.square((1.0 / d_hor) * min_dist_wall[:, tf.newaxis])

    angle_diff = tf.square(
        y_pred_s[:, 4, tf.newaxis] - omega_walls[tf.newaxis, :])

    angle_diff_loss = tf.reduce_sum(angle_diff_scale * angle_diff, axis=1)

    return tf.reduce_sum(tf.multiply(grid_cell_pred, grid_cell_true),
                          axis=1) * angle_diff_loss


def get_rotated_values(y_true, y_pred_s, tf_PI):
    #
    # following operations are computed with rotated local coordinate system
    #

    # rotate (c_x, c_y) and (t_x, t_y) by omega * PI around center of grid cell
    omega = y_pred_s[:, 4]

    rot_cx = tf.multiply(y_pred_s[:, 0],
                         tf.cos(tf_PI * omega)) - tf.multiply(
        y_pred_s[:, 1], tf.sin(tf_PI * omega))
    rot_cy = tf.multiply(y_pred_s[:, 1],
                         tf.cos(tf_PI * omega)) + tf.multiply(
        y_pred_s[:, 0], tf.sin(tf_PI * omega))

    rot_tx = tf.multiply(y_true[:, 0], tf.cos(tf_PI * omega)) - tf.multiply(
        y_true[:, 1], tf.sin(tf_PI * omega))
    rot_ty = tf.multiply(y_true[:, 1], tf.cos(tf_PI * omega)) + tf.multiply(
        y_true[:, 0], tf.sin(tf_PI * omega))

    y_true = tf.concat(
        [tf.reshape(rot_tx, [-1, 1]), tf.reshape(rot_ty, [-1, 1]),
         tf.reshape(y_true[:, 2], [-1, 1])], axis=1)

    y_pred_s = tf.concat(
        [tf.reshape(rot_cx, [-1, 1]), tf.reshape(rot_cy, [-1, 1]),
         y_pred_s[:, 2:]], axis=1)

    return y_true, y_pred_s


def get_box_loss(y_true, y_pred_s, grid_cell_true, grid_cell_pred, params):
    # box loss
    box_loss = tf.zeros_like(y_true[:, 0])
    box_loss_scale = tf.reduce_sum(
        tf.multiply(grid_cell_pred, grid_cell_true), axis=1)

    # center loss
    c_loss = tf.reduce_sum(
        tf.square(tf.subtract(y_true[:, :2], y_pred_s[:, :2])),
        axis=1)

    box_loss += c_loss

    if "outside" in params:

        delta = 20.0
        if "delta" in params['outside']:
            delta = params['outside']['delta']

        outside_x = tf.subtract(tf.subtract(
            tf.abs(tf.subtract(y_true[:, 0], y_pred_s[:, 0])),
            tf.divide(y_pred_s[:, 2], tf.constant(delta))),
            tf.constant(0.0))

        outside_y = tf.subtract(tf.subtract(
            tf.abs(tf.subtract(y_true[:, 1], y_pred_s[:, 1])),
            tf.divide(y_pred_s[:, 3], tf.constant(delta))),
            tf.constant(0.0))

        outside_x_loss = outside_x
        outside_y_loss = outside_y

        outside_loss = tf.square(outside_x_loss) + tf.square(outside_y_loss)

        if "scale" in params["outside"]:
            factor = params["outside"]["scale"]
            outside_loss = tf.multiply(outside_loss, tf.constant(factor))

        box_loss += outside_loss

    return box_loss * box_loss_scale



if __name__ == "__main__":

    #y_pred_s = np.array([[0,1], [1,2], [3,0]])
    #walls = np.array([[1,0,1,2],[2,1,1,3],[1,0,1,4],[2,5,1,4]])

    # test_res = np.matmul(
    #     np.reshape(y_pred_s[:, 0], [-1, 1]),
    #     np.reshape((walls[:, 3] - walls[:, 1]), [1, -1]))
    #
    test_yolo_loss()

    # test = np.array([[1,2,5,4,5,6], [1,2,5,4,5,6], [7,2,5,4,5,6], [1,2,5,4,7,6]])
    # y = np.array([[2, 1], [3, 0], [1, 1], [1, 1]])
    #
    # test = test.reshape(len(test), 2, 3)
    #
    # #print(test)
    #
    # idx = y[:, 1]
    #
    # sub = test[:, idx, :][[0,1,2,3], [0,1,2,3]]
    # #print(sub)
    # #test()
    # test_yolo_loss()
    # #y_true = np.array([[7, 3], [2,4], [1, 1], [3,4]])
    #
    # #y_pred = np.array([[6, 1, 7.5, 4], [1,3, 3, 4.5], [2, 0, 3, 1], [4,1,3,2]])
    #
    # #custom_loss_python(y_pred, y_true)