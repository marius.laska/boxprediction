from data.gia.gia_data_provider import PDdataProvider
from il_pipeline.models.dnn_model import DnnModel


class BboxDataProvider(PDdataProvider):

    def __init__(self, conn_str=None, map_id=None, split_ratio=None):
        super().__init__(conn_str, map_id, split_ratio)

