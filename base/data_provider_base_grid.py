import math

from data.data_provider_base import DataProviderBase
import numpy as np
from pkg_resources import resource_filename
from sklearn.model_selection import train_test_split

from base.floor_plan_plot import FloorPlanPlotRec


class DataProviderGridBase(DataProviderBase):

    def __init__(self):
        super().__init__()

        self.grid_labels = None
        self.grid_size = None
        self.grid_walls_h = None
        self.grid_walls_v = None

    def transform_to_hier_grid_encoding(self, start_grid_size=10, division=2, depth=3):
        coord_center = (0.5, 0.5)

        labels = self.transform_to_2dim_grid_encoding(grid_size=start_grid_size, center=coord_center)

        hier_labels = np.zeros((len(self.labels), 3, depth))

        for d in range(depth):
            if d > 0:
                labels = self.transform_to_2dim_grid_encoding(
                    grid_size=1/division, labels=labels[:, :2], width=1.0, height=1.0, initial_shift=coord_center, center=coord_center, set_grid_size=False)

            hier_labels[:, :, d] = labels

        self.grid_labels = hier_labels

    def transform_to_2dim_2layer_grid_encoding(self, grid_size=10.0, height=None, width=None, labels=None):

        if labels is None:
            labels = self.labels

        if height is None:
            height = self.floorplan_height

        if width is None:
            width = self.floorplan_width

        # Determine #row,col for 1st layer
        num_row_l1 = np.ceil(height / grid_size)
        num_col_l1 = np.ceil(width / grid_size)
        num_l1 = int(num_row_l1 * num_col_l1)

        num_row_l2 = num_row_l1 - 1
        num_col_l2 = num_col_l1 - 1
        num_l2 = int(num_row_l2 * num_col_l2)

        num = num_l1 + num_l2

        center = []
        origins = []

        # Determine centers & origins (lower-left)
        for idx in range(num):
            # check whether 1st or 2nd layer
            if idx < num_l1:
                # 1st layer
                r_idx = int(idx / num_col_l1)
                c_idx = idx % num_col_l1

                x = c_idx * grid_size
                y = r_idx * grid_size

                origins.append(np.array([x, y]))
                center.append(np.array([x + grid_size/2.0, y + grid_size/2.0]))
            else:
                # 2nd layer
                idx -= num_l1
                r_idx = int(idx / num_col_l2)
                c_idx = idx % num_col_l2

                x = c_idx * grid_size
                y = r_idx * grid_size

                origins.append(np.array([x + grid_size / 2.0, y + grid_size / 2.0]))
                center.append(np.array([x + grid_size, y + grid_size]))

        # Determine closest origin for labels (the corresponding cell is
        # responsible for encoding
        center = np.array(center)
        origins = np.array(origins)

        dist_to_center = np.full((len(labels), len(center)), np.inf)
        for o_idx, o in enumerate(center):
            d = np.linalg.norm(labels - o, axis=1)
            dist_to_center[:, o_idx] = d

        resp_grid_idx = np.argmin(dist_to_center, axis=1)
        resp_origins = origins[resp_grid_idx, :]

        # determine final encoding
        grid_encoding = np.zeros((len(labels), 3))
        grid_encoding[:, 2] = resp_grid_idx
        grid_encoding[:, :2] = (labels - resp_origins) / grid_size

        self.grid_size = grid_size
        self.grid_labels = grid_encoding

    def transform_to_2dim_overlapping_grid_encoding(self, grid_size=10.0, padding_ratio=0.1, height=None, width=None, labels=None):

        img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
        linewidth = 2
        #fp = FloorPlanPlotRec((200, 80), 2, floorplan_bg_img=img)

        if labels is None:
            labels = self.labels

        if height is None:
            height = self.floorplan_height

        if width is None:
            width = self.floorplan_width

        # width = 160

        # Determine #row,col for 1st layer
        num_row_l1 = np.ceil(height / grid_size) + 1
        num_col_l1 = np.ceil(width / grid_size) + 1
        num_l1 = int(num_row_l1 * num_col_l1)

        num = num_l1

        origins = []

        # Determine centers & origins (lower-left)
        for idx in range(num):

            # 1st layer
            r_idx = int(idx / num_col_l1)
            c_idx = idx % num_col_l1

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(np.array([x, y]))
            #fp.axis.text(x+1, y-2, str(idx))

        # Determine closest origin for labels (the corresponding cell is
        # responsible for encoding
        o_plt = np.array([[o[0], o[1], 40, 40] for o in origins])
        o_plt_pad = np.array([[o[0], o[1], 48, 48] for o in origins])

        origins = np.array(origins)

        #fp.draw_rectangles_new(o_plt, color="black")
        #fp.draw_rectangles_new(o_plt_pad, color="grey")

        #fp.draw_rectangles_new(o_plt[7], color="green")
        #fp.draw_rectangles_new(o_plt_pad[[1,2,6]], color="blue")

        #fp.draw_points(origins[:, 0], origins[:, 1], color="black")
        #fp.draw_points([62], [21], color="green")

        o_vals = np.array([7,1,2,6])
        test = (np.array([[62, 21]]) - origins[o_vals, :]) / (grid_size / 2.0 + grid_size * padding_ratio)
        print(np.concatenate((test, o_vals.reshape(-1, 1)), axis=1))

        #fp.show_plot()

        dist_to_center = np.full((len(labels), len(origins)), np.inf)
        dist_to_center_cmp = np.full((len(labels), len(origins), 2), np.inf)
        for o_idx, o in enumerate(origins):
            d = np.linalg.norm(labels - o, axis=1)
            dist_to_center[:, o_idx] = d
            dist_to_center_cmp[:, o_idx, :] = np.abs(labels - o)

        resp_grid_idx = np.argmin(dist_to_center, axis=1)
        resp_origins = origins[resp_grid_idx, :]

        # find alternative origins for augmentation
        test = np.array(np.where(np.all(dist_to_center_cmp[:, :, :] < (grid_size / 2 + grid_size * padding_ratio), axis=2)))

        aug_encoding = {}
        for l_idx in range(len(labels)):
            mask = np.where(test[0, :] == l_idx)[0]
            aug_origins = test[1, mask]
            aug_idx = aug_origins[np.where(aug_origins != resp_grid_idx[l_idx])[0]]
            if len(aug_idx) > 0:
                aug_enc = np.zeros((len(aug_idx), 3))
                aug_enc[:, 2] = aug_idx
                aug_enc[:, :2] = (labels[l_idx] - origins[aug_idx, :]) / (
                            grid_size / 2.0 + grid_size * padding_ratio)

                aug_encoding[l_idx] = aug_enc

        # determine final encoding
        grid_encoding = np.zeros((len(labels), 3))
        grid_encoding[:, 2] = resp_grid_idx
        grid_encoding[:, :2] = (labels - resp_origins) / (grid_size / 2.0 + grid_size * padding_ratio)

        self.grid_size = grid_size
        self.grid_labels = grid_encoding
        self.aug_encoding = aug_encoding

    def encode_walls_to_2dim_overlapping_grid_encoding(self, walls, horizontal=True, overlap_strategy="ignore", angle_only=False, grid_size=10.0, padding_ratio=0.1, height=None, width=None):

        img = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
        #fp = FloorPlanPlotRec((200, 80), 2, floorplan_bg_img=img)

        if height is None:
            height = self.floorplan_height

        if width is None:
            width = self.floorplan_width

        # Determine #row,col for 1st layer
        num_row_l1 = np.ceil(height / grid_size) + 1
        num_col_l1 = np.ceil(width / grid_size) + 1
        num_l1 = int(num_row_l1 * num_col_l1)

        num = num_l1

        origins = []

        # Determine centers & origins (lower-left)
        for idx in range(num):

            # 1st layer
            r_idx = int(idx / num_col_l1)
            c_idx = idx % num_col_l1

            x = c_idx * grid_size
            y = r_idx * grid_size

            origins.append(np.array([x, y]))
            #fp.axis.text(x+1, y-2, str(idx))

        # Determine closest origin for labels (the corresponding cell is
        # responsible for encoding
        o_plt = np.array([[o[0], o[1], 40, 40] for o in origins])
        o_plt_pad = np.array([[o[0], o[1], 48, 48] for o in origins])

        origins = np.array(origins)

        # fp.draw_rectangles_new(o_plt, color="black")
        # fp.draw_rectangles_new(o_plt_pad, color="grey")
        #
        # fp.draw_points(origins[:, 0], origins[:, 1], color="black")

        dist_line_center = np.full((len(walls), len(origins)), np.inf)
        dist_to_center = np.full((len(walls), len(origins), 2), np.inf)
        dist_to_center_cmp = np.full((len(walls), len(origins), 2, 2), np.inf)
        for o_idx, o in enumerate(origins):
            d_1 = np.linalg.norm(walls[:, :2] - o, axis=1)
            d_2 = np.linalg.norm(walls[:, 2:4] - o, axis=1)
            d_ceter_line = np.linalg.norm(np.mean(np.stack(
                [walls[:, [0, 2]],
                 walls[:, [1, 3]]], axis=2), axis=1) - o, axis=1)

            dist_to_center[:, o_idx, 0] = d_1
            dist_to_center[:, o_idx, 1] = d_2
            dist_line_center[:, o_idx] = d_ceter_line

            dist_to_center_cmp[:, o_idx, 0, :] = np.abs(walls[:, :2] - o)
            dist_to_center_cmp[:, o_idx, 1, :] = np.abs(walls[:, 2:4] - o)

        resp_grid_center_line_idx = np.argmin(dist_line_center, axis=1)
        resp_grid_center_origins = origins[resp_grid_center_line_idx, :]

        # resp_grid_idx = np.argmin(dist_to_center, axis=1)
        # resp_origins = origins[resp_grid_idx, :]

        # compute dist to respective center
        p1_dist = walls[:, :2] - resp_grid_center_origins
        p1_inside = np.all(p1_dist < grid_size + grid_size * padding_ratio, axis=1)
        p2_dist = walls[:, 2:4] - resp_grid_center_origins
        p2_inside = np.all(p2_dist < grid_size + grid_size * padding_ratio, axis=1)

        # idx of walls that cannot be encoded within single grid_cell
        mask = np.logical_and(p1_inside, p2_inside)
        split_required = np.where(~mask)[0]
        no_split_required = np.where(mask)[0]
        if overlap_strategy == "ignore":
            walls = walls[no_split_required, :]
            resp_grid_center_line_idx = resp_grid_center_line_idx[no_split_required]
            resp_grid_center_origins = resp_grid_center_origins[no_split_required, :]
        elif overlap_strategy == "keep":
            print("keep walls between grid_idx")

        if horizontal:
            grid_encoding = np.zeros((len(walls[:, :2]), 6))
            grid_encoding[:, 5] = resp_grid_center_line_idx

            # calculate angle
            omega = -np.arctan(
                (walls[:, 3] - walls[:, 1]) / (walls[:, 2] - walls[:, 0])) / math.pi
            grid_encoding[:, 4] = omega
        else:
            grid_encoding = np.zeros((len(walls[:, :2]), 5))
            grid_encoding[:, 4] = resp_grid_center_line_idx

        # determine final encoding
        # grid_encoding = np.zeros((len(walls[:, :2]), 5))
        # grid_encoding[:, 4] = resp_grid_center_line_idx
        if not angle_only:
            grid_encoding[:, :2] = (walls[:, :2] - resp_grid_center_origins) / (grid_size / 2.0 + grid_size * padding_ratio)
            grid_encoding[:, 2:4] = (walls[:, 2:4] - resp_grid_center_origins) / (grid_size / 2.0 + grid_size * padding_ratio)
        else:
            grid_encoding[:, :4] = walls

        # calc angles

        if horizontal:
            self.grid_walls_h = grid_encoding
        else:
            self.grid_walls_v = grid_encoding

    def transform_to_2dim_3layer_grid_encoding(self, grid_size=10.0, height=None, width=None, labels=None):

        if labels is None:
            labels = self.labels

        if height is None:
            height = self.floorplan_height

        if width is None:
            width = self.floorplan_width

        # Determine #row,col for 1st layer
        num_row_l1 = np.ceil(height / grid_size)
        num_col_l1 = np.ceil(width / grid_size)
        num_l1 = int(num_row_l1 * num_col_l1)

        num_row_l2 = num_row_l1 + 1
        num_col_l2 = num_col_l1
        num_l2 = int(num_row_l2 * num_col_l2)

        num_row_l3 = num_row_l1 - 1
        num_col_l3 = num_col_l1
        num_l3 = int(num_row_l3 * num_col_l3)

        num = num_l1 + num_l2 + num_l3

        center = []
        origins = []

        # Determine centers & origins (lower-left)
        for idx in range(num):
            # check whether 1st or 2nd layer
            if idx < num_l1:
                # 1st layer
                r_idx = int(idx / num_col_l1)
                c_idx = idx % num_col_l1

                x = c_idx * grid_size
                y = r_idx * grid_size

                origins.append(np.array([x, y]))
                center.append(np.array([x + grid_size/2.0, y + grid_size/2.0]))
            elif idx < (num_l1 + num_l2):
                # 2nd layer
                idx -= num_l1
                r_idx = int(idx / num_col_l2)
                c_idx = idx % num_col_l2

                x = c_idx * grid_size
                y = r_idx * grid_size

                origins.append(np.array([x - grid_size / 2.0, y - grid_size / 2.0]))
                center.append(np.array([x, y]))
            else:
                # 3rd layer
                idx -= (num_l1 + num_l2)
                r_idx = int(idx / num_col_l3)
                c_idx = idx % num_col_l3

                x = c_idx * grid_size
                y = r_idx * grid_size

                origins.append(
                    np.array([x, y + grid_size / 2.0]))
                center.append(np.array([x + grid_size / 2.0, y + grid_size]))

        # Determine closest origin for labels (the corresponding cell is
        # responsible for encoding
        center = np.array(center)
        origins = np.array(origins)

        dist_to_center = np.full((len(labels), len(center)), np.inf)
        for o_idx, o in enumerate(center):
            d = np.linalg.norm(labels - o, axis=1)
            dist_to_center[:, o_idx] = d

        resp_grid_idx = np.argmin(dist_to_center, axis=1)
        resp_origins = origins[resp_grid_idx, :]

        # determine final encoding
        grid_encoding = np.zeros((len(labels), 3))
        grid_encoding[:, 2] = resp_grid_idx
        grid_encoding[:, :2] = (labels - resp_origins) / grid_size

        self.grid_size = grid_size
        self.grid_labels = grid_encoding


    def transform_to_2dim_grid_encoding(self, grid_size=10.0, height=None, width=None, labels=None, center=(0, 0), initial_shift=None, set_grid_size=True):

        if labels is None:
            labels = self.labels

        if height is None:
            height = self.floorplan_height

        if width is None:
            width = self.floorplan_width

        if initial_shift:
            labels[:, :2] += initial_shift

        grid_labels = np.zeros((len(labels), 3))

        if set_grid_size:
            self.grid_size = grid_size

        n_rows = math.ceil(height/grid_size)
        n_cols = math.ceil(width/grid_size)

        r_id = (labels[:, 1] / grid_size).astype(int)
        c_id = (labels[:, 0] / grid_size).astype(int)

        g_idx = r_id * n_cols + c_id

        # fix outliers
        g_idx[np.where(g_idx > (n_rows * n_cols - 1))] = (n_rows * n_cols - 1)

        grid_labels[:, 2] = g_idx

        grid_labels[:, :2] = (labels % grid_size) / grid_size

        if center != (0,0):
            grid_labels[:, :2] -= np.array(center)

        self.grid_labels = grid_labels

        return grid_labels

    def transform_to_grid_encoding(self):
        self.grid_labels = np.zeros((len(self.labels), 3))

        dims = np.array([self.floorplan_width, self.floorplan_height])

        min_len = np.min(dims)
        arg_max = np.argmax(dims)

        self.grid_labels[:, :2] = (self.labels % min_len) / min_len
        self.grid_labels[:, 2] = (self.labels[:, arg_max] / min_len).astype(int)

    def get_augmented_train_data(self):
        # get data and labels according to generator mode and k-fold split idx
        split_indices = self.splits_indices
        split_idx = self.current_split_idx
        data = self.data_tensor
        labels = self.grid_labels

        train_val_idx, test_idx = split_indices[split_idx]

        # obtain separate validation data
        train_idx, val_idx, _, _ = train_test_split(
            train_val_idx, train_val_idx, test_size=self.split_ratio['val'],
            random_state=self.random_state)

        x_train = data[train_idx]
        y_train = labels[train_idx]

        for k, v in self.aug_encoding.items():
            if k in train_idx:
                r_data = data[k, :].reshape(1, -1)
                x_train = np.concatenate((x_train, np.repeat(r_data, len(v), axis=0)), axis=0)
                y_train = np.concatenate((y_train, v), axis=0)

        return x_train, y_train

    def get_num_grid_cells(self):
        return None

    def in_mem_data_generator(self, split_idx=None, mode='train', area_labels=True, augmentation=None, autoencoder=False, noise_percentage=None, noisy_labels=False, grid_labels=True, model_type="classification", batch_size=32):
        if split_idx is None:
            split_idx = self.current_split_idx

        if mode == 'train':
            data, labels = self.get_train_data(split_idx=split_idx, area_labels=area_labels, noisy_labels=noisy_labels, labels=self.grid_labels)
        if mode == 'val':
            data, labels = self.get_val_data(split_idx=split_idx, area_labels=area_labels, noisy_labels=noisy_labels, labels=self.grid_labels)
        if mode == 'test':
            data, labels = self.get_test_data(split_idx=split_idx, area_labels=area_labels, noisy_labels=noisy_labels, labels=self.grid_labels)

        if augmentation is not None and augmentation > 0 and mode == 'train':
            #data, labels = self.augmentate_data(num_augs=augmentation, missing_val=0)
            # augment only training data
            data, labels = self.get_augmented_train_data()

        if model_type == "CNN" or "2D-CNN" in model_type:
            data = self.transform_batch_to_image(data, pearsoncorr=False)
        elif model_type == "CNN-PSSD":
            data = self.transform_batch_to_pairwise_diff_image(data)

        labels = self.add_class_label_noise(labels, noise_percentage)

        num_batches = math.ceil(len(data) / batch_size)
        while True:

            for run_id in range(num_batches):
                b_data = data[run_id * batch_size:(run_id + 1) * batch_size, :]
                b_labels = labels[run_id * batch_size:(run_id + 1) * batch_size, :]

                if autoencoder:
                    yield (b_data, b_data)
                else:
                    yield (b_data, b_labels)