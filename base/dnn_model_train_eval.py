import logging
import math
import os

from il_pipeline.models.dnn_model import DnnModel
from data.data_provider_base import DataProviderBase
from base.KFoldTrainTestSummary import KFoldTrainTestSummary

log = logging.getLogger(__name__)


class DnnModelTrainEval(DnnModel):
    def __init__(self, type="classification", summary: KFoldTrainTestSummary=None,
                 data_provider: DataProviderBase=None,
                 params=None, output_dir=None, filename=None):

        super().__init__(self, type, summary,
                 data_provider, params, output_dir, filename)

        self.summary: KFoldTrainTestSummary = summary

    def evaluate_model(self, test_bs):
        """
        Evaluates the model on separate test data (supplied by data generator)
        :param test_bs: batch size of test data
        """

        data_provider = self.data_provider
        summary = self.summary
        classifier = self.classifier

        path = self.output_dir + "{}.hdf5".format(self.filename)
        if os.path.exists(path):
            classifier.load_weights(path)

        # calculate the amount of steps on the test data before one batch
        # is completed (required by generator)
        _, _, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")
        test_steps_per_epoch = math.ceil(num_test / test_bs)

        area_labels = self.type == "classification" or self.type == "cnn"
        y_pred = classifier.predict_generator(
            data_provider.in_mem_data_generator(
                mode='test', model_type=self.type,
                area_labels=area_labels, batch_size=test_bs),
            steps=test_steps_per_epoch,
            verbose=0)

        # evaluate on train data

        num_train, num_val, num_test = data_provider.get_train_val_test_num(
            area_labels=self.type != "regression")

        train_bs = min(num_train, self.params['batch_size'])
        train_steps_per_epoch = math.ceil(num_train / train_bs)

        y_pred_train = classifier.predict_generator(
            data_provider.in_mem_data_generator(
                mode='train', model_type=self.type,
                area_labels=area_labels, batch_size=train_bs),
            steps=train_steps_per_epoch,
            verbose=0
            )

        #if self.type != "regression":
        if data_provider.area_labels is not None:
            _, y_true = data_provider.get_test_data()
            summary.y_true.append(y_true)

        _, y_true_labels = data_provider.get_test_data(area_labels=False)
        summary.y_pred_train = y_pred_train
        summary.y_pred.append(y_pred)
        summary.y_true_labels.append(y_true_labels)

        summary.num_folds += 1
