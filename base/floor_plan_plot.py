from il_pipeline.plotting.pipeline_plots import FloorplanPlot
import matplotlib.patches as patches
import numpy as np


class FloorPlanPlotRec(FloorplanPlot):

    def __init__(self, floorplan_dimensions, grid_size, draw_grid=False, floorplan_bg_img="",
                 filename=None, sample_points_file=None, add_points=False,
                 walls_file=None, add_walls=False, xtick_freq=None, artificial_labels=False,
                 correct_walls=False, title=None, walls=None):
        super().__init__(floorplan_dimensions, grid_size, draw_grid, floorplan_bg_img,
                         filename, sample_points_file, add_points, walls_file, add_walls,
                         xtick_freq, artificial_labels, correct_walls)

        if title is not None:
            self.set_title(title)
        if walls is not None:
            self.new_walls = walls
            self.draw_initial_walls()

    def set_title(self, title="title"):
        self.axis.set_title(title)

    def draw_circles(self, centers, radius, color='r'):
        if centers.ndim == 1:
            centers = np.array([centers])
            radius = np.array([radius])

        for center, rad in zip(centers, radius):
            circle = patches.Circle(center, rad, fill=False, color=color)
            self.axis.add_patch(circle)

    def draw_ellipse(self, center, width, height, color="r"):
        el = patches.Ellipse(center, width, height, fill=False, color=color)
        self.axis.add_patch(el)

    def draw_rectangles_new(self, anchors, color="r"):
        if anchors.ndim == 1:
            anchors = np.array([anchors])
        for anchor in anchors:
            min_x = anchor[0] - anchor[2] / 2
            min_y = anchor[1] - anchor[3] / 2

            width = anchor[2]
            height = anchor[3]

            rect = patches.Rectangle((min_x, min_y), width, height, fill=False,
                                 color=color)
            self.axis.add_patch(rect)

    def draw_rectangles(self, anchors, color="r", fill=False):
        if anchors.ndim == 1:
            anchors = np.array([anchors])
        for anchor in anchors:
            min_x = min(anchor[0], anchor[2])
            min_y = min(anchor[1], anchor[3])
            max_x = max(anchor[0], anchor[2])
            max_y = max(anchor[1], anchor[3])

            width = max_x - min_x
            height = max_y - min_y

            rect = patches.Rectangle((min_x, min_y), width, height, fill=fill,
                                 color=color)
            self.axis.add_patch(rect)

    def draw_error_scatter(self, points, error, color="r", norm=None):

        self.axis.scatter(points[:, 0], points[:, 1],
                          s=error, color=color, alpha=1)