from data.gia.gia_data_provider import PDdataProvider
from base.data_provider_base_grid import DataProviderGridBase
import pandas as pd
from sqlalchemy import create_engine


class GiaDataProviderGrid(PDdataProvider, DataProviderGridBase):
    pass

    def get_mac_df_multiple_maps(self):
        # query for measurements dataframe
        q_str = """
                SELECT m."macAddress" FROM measurementprocessresult as mp JOIN measurements ON 
                mp.id=measurements."processResult" 
                JOIN macaddress AS m on m.id = measurements."macAddress"
                WHERE "mapId" IN (32,21,18,1)
                """

        # initiate database connection
        engine = create_engine(self.conn_str)

        measurements_df = pd.read_sql_query(q_str, con=engine)

        self.mac_addresses_df = measurements_df.drop_duplicates(
            subset="macAddress").loc[:, ["macAddress"]].sort_values(
            "macAddress")

    @classmethod
    def setup_data_provider(self, data_params, pre_params=None, full_mac=False):
        """
        Factory method for data provider that uses filters to obtain subset
        of data.
        1) The labels are transformed from grid dimension with origin at upper left
        (android drawing) to lower left with meter dimension.
        2) Filters are applied to obtain base set of data
        3) Data is transformed into tensor and standardized (0-std columns (APs)
             are discarded)
        4) Data is split into several folds, such that the specific data
            (train/val/test) can be accessed for the current split index of the
            class
        :param data_params: Data parameters dict that holds connection and map_id
        :param filters: The filters parsed from yaml (via ConfigReader)
        :return:
        """
        connection_str = data_params['connection']
        map_id = data_params['map_id']
        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        dataset_provider = GiaDataProviderGrid(connection_str, map_id, split_ratio)

        # read SQL dataset from Backend

        if type(map_id) is list:
            dataset_provider.get_building_dataset()
            dataset_provider.test_transform_into_tensor()
        else:
            dataset_provider.get_dataset()

            if full_mac:
                dataset_provider.get_mac_df_multiple_maps()

            # convert to meters
            dataset_provider.transform_labels_from_grid_to_meter()
            dataset_provider.transform_labels_origin(origin="ll")

            # set up all filters that are sequentially applied to generate
            # the desired subset of data
            data_filters = dataset_provider.setup_filters(pre_params=pre_params)
            dataset_provider.apply_filters(data_filters)

            dataset_provider.transform_into_tensor()

        # add label noise
        if 'label_noise' in pre_params:
            param = pre_params['label_noise']
            if type(param) is list:
                dataset_provider.add_label_noise(*pre_params['label_noise'])
            else:
                dataset_provider.add_noise_within_walls(walls_file=param)

        # if we replace missing values by area mean, we cannot standardize yet
        if 'missing_values' not in pre_params and (
                'standardize' not in pre_params or pre_params['standardize']):
            dataset_provider.standardize_data()

        # remove temporary APs
        if "filter_tmp_AP" in pre_params and pre_params[
            "filter_tmp_AP"] is not None:
            dataset_provider.filter_temporary_access_points(
                temp_threshold=pre_params["filter_tmp_AP"])

        if dataset_provider.splits_indices is None:
            dataset_provider.generate_split_indices(pre_params)

        return dataset_provider