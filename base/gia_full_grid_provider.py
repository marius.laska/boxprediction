import pandas as pd
import numpy as np
from il_pipeline.pipeline import Pipeline
from sqlalchemy import create_engine

from base.gia_data_provider_grid import GiaDataProviderGrid


class GiaProviderFullGrid(GiaDataProviderGrid):

    def __init__(self):
        super().__init__()
        self.dps = {}

    def generate_split_indices(self, pre_params=None):
        pass

    def setup_dps(self, data_params, pre_params):

        map_ids = data_params['map_id']

        idx = 0
        for index, map_id in enumerate(map_ids):
        #for index, comb in self.get_bld_floor_combinations().iterrows():
            idx += 1
            #if idx > 3:
            #    break

            data_params["map_id"] = map_id
            #data_params["floor"] = comb[1]

            #data_params = {"building": comb[0], "floor": comb[1]}
            pre_params["standardize"] = False
            dp = GiaProviderFullGrid.setup_data_provider(data_params=data_params, pre_params=pre_params, full_mac=True)
            self.split_ratio = dp.split_ratio

            dp.transform_to_2dim_overlapping_grid_encoding(
                grid_size=pre_params['grid_size'])

            self.dps[(0, index)] = dp

        self.merge_dps()

    def merge_dps(self):

        data = []
        aug_dicts = []
        labels = []
        grid_labels = []

        train_idx = []
        test_idx = []
        last_max_idx = 0
        last_grid_idx = 0
        sum_max_idx = 0
        sum_grid_idx = 0
        mapping = {}
        last_len_idx = 0

        for key, dp in self.dps.items():
            data.append(dp.data_tensor)
            labels.append(dp.labels)

            # aug encoding
            aug_dict = {k+last_len_idx: v for (k, v) in dp.aug_encoding.items()}
            aug_dicts.append(aug_dict)
            last_len_idx += len(dp.data_tensor)

            # num grid cells
            num_row_l1 = np.ceil(dp.floorplan_height / dp.grid_size) + 1
            num_col_l1 = np.ceil(dp.floorplan_width / dp.grid_size) + 1
            max_grid_idx = int(num_row_l1 * num_col_l1)
            max_idx = len(np.concatenate(dp.splits_indices[0], axis=0))

            sum_max_idx += max_idx
            sum_grid_idx += max_grid_idx

            dp.grid_labels[:, 2] += last_grid_idx
            grid_labels.append(dp.grid_labels)

            mapping[key] = (last_max_idx, sum_max_idx, last_grid_idx, sum_grid_idx)

            train_idx.append(dp.splits_indices[0][0] + last_max_idx)
            test_idx.append(dp.splits_indices[0][1] + last_max_idx)

            last_max_idx = sum_max_idx
            last_grid_idx = sum_grid_idx

        # merge aug dicts
        full_aug_dict = {}
        for a_d in aug_dicts:
            for (k, v) in a_d.items():
                full_aug_dict[k] = v

        self.aug_encoding = full_aug_dict
        self.data_tensor = np.concatenate(data, axis=0)
        self.labels = np.concatenate(labels, axis=0)
        self.grid_labels = np.concatenate(grid_labels, axis=0)
        self.dp_mapping = mapping

        # concat and shuffle indices
        train = np.concatenate(train_idx, axis=0)
        test = np.concatenate(test_idx, axis=0)
        np.random.shuffle(train)
        np.random.shuffle(test)

        self.splits_indices = [(train, test)]
        self.num_splits = 1

        self.standardize_data()

    def _decode_grid_labels(self, grid_labels, y_true_labels, y_true):
        sub = grid_labels[:, 4::5]
        chosen = np.argmax(sub, axis=1)

        decoded_preds = np.zeros((len(grid_labels), 7))
        decoded_true = np.zeros((len(grid_labels), 5))

        decoded_preds[:, 6] = chosen
        decoded_true[:, 4] = y_true[:, 2]

        # find corresponding dp
        for key, mapping in self.dp_mapping.items():
            # y_pred
            c_range = np.logical_and(chosen >= mapping[2], chosen < mapping[3])
            dp_idx = np.where(c_range)[0]
            dp = self.dps[key]

            # y_true
            t_range = np.logical_and(y_true[:, 2] >= mapping[2], y_true[:, 2] < mapping[3])
            t_idx = np.where(t_range)[0]
            decoded_true[t_idx, 2:4] = np.array(key)

            decoded_labels = convert_from_2dim_overlapping_grid(
                grid_labels[dp_idx, :], offset=mapping[2],
                grid_size=dp.grid_size, padding_ratio=0.1,
                height=dp.floorplan_height, width=dp.floorplan_width)

            decoded_preds[dp_idx, :4] = decoded_labels
            decoded_preds[dp_idx, 4:6] = np.array(key)

            decoded_true[dp_idx, :2] = y_true_labels[dp_idx, :2]

            # print("Enc->DEC: {}".format(
            #     np.array_equal(y_true_labels[dp_idx, :], decoded_labels[:, :2])))

        return decoded_preds, decoded_true

    def decode_pipe_grid_labels(self, p: Pipeline):
        y_pred_list = p.summary.y_pred
        y_true_labels_list = p.summary.y_true_labels
        y_true_list = p.summary.y_true
        y_pred_list_new = []
        y_true_list_new = []
        for y_pred, y_true_labels, y_true in zip(y_pred_list, y_true_labels_list, y_true_list):
            y_p, y_t = self._decode_grid_labels(y_pred, y_true_labels, y_true)
            y_pred_list_new.append(y_p)
            y_true_list_new.append(y_t)

        p.summary.y_pred = y_pred_list_new
        p.summary.y_true = y_true_list_new

    def get_bld_floor_combinations(self):

        data = pd.read_csv("UJIndoorLoc/trainingData.csv")
        bld_id = data[["BUILDINGID", "FLOOR"]].drop_duplicates().sort_values(by=["BUILDINGID", "FLOOR"])

        return bld_id

    def get_num_grid_cells(self):
        max_key = sorted(self.dp_mapping.keys())[-1]

        return self.dp_mapping[max_key][3]


def convert_from_2dim_overlapping_grid(pred_vals, width, height, offset=0, grid_size=40, padding_ratio=0.1):

    # Determine #row,col for 1st layer
    num_row_l1 = np.ceil(height / grid_size) + 1
    num_col_l1 = np.ceil(width / grid_size) + 1
    num_l1 = int(num_row_l1 * num_col_l1)

    num = num_l1

    origins = []

    # Determine centers & origins (lower-left)
    for idx in range(num):
        # 1st layer
        r_idx = int(idx / num_col_l1)
        c_idx = idx % num_col_l1

        x = c_idx * grid_size
        y = r_idx * grid_size

        origins.append(np.array([x, y]))

    # Determine closest origin for labels (the corresponding cell is
    # responsible for encoding
    origins = np.array(origins)

    sub = pred_vals[:, 4::5]
    chosen = np.argmax(sub, axis=1)

    gs_choice = chosen - offset

    pred_fold = np.zeros((len(pred_vals), 4))

    for idx in range(len(pred_vals)):
        local_box = pred_vals[idx,
                    chosen[idx] * 5:(chosen[idx] + 1) * 5 - 1]

        local_box[:2] = origins[gs_choice[idx], :] + local_box[:2] * (grid_size / 2.0 + grid_size * padding_ratio)

        # 3rd and 4th dimension contain width and height
        # convert from [-1,1] to [0,1]
        local_box[2:] = (local_box[2:] + 1) / 2.0
        local_box[2:] *= grid_size

        pred_fold[idx, :] = local_box

    return pred_fold


def test_encoding_decoding():
    dp = GiaProviderFullGrid()
    dp.setup_dps(data_params={}, pre_params={})

    # test prediction that chooses correct grid cell and perfect box center
    pred_vals = np.zeros((len(dp.grid_labels), 267 * 5))

    for gl_idx, gl in enumerate(dp.grid_labels):
        pred_vals[gl_idx, int(gl[2] * 5 + 4)] = 1.0
        pred_vals[gl_idx, int(gl[2] * 5)] = gl[0]
        pred_vals[gl_idx, int(gl[2] * 5 + 1)] = gl[1]

    dp._decode_grid_labels(pred_vals)