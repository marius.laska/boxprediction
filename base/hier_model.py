import math
import os
import numpy as np

from data.data_provider_base import DataProviderBase
from il_pipeline.models.dnn_model import DnnModel
from il_pipeline.summary.summary import KFoldClassSummary
from il_pipeline.models.tf_model_definition import classification_model_for_generator, cnn_model_for_generator
from tensorflow.keras.activations import softmax, linear, sigmoid
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras.backend import categorical_crossentropy, relu, softmax
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.optimizers import Adam

from base.bbox_model_definition import bbox_model_for_generator
from base.hier_model_definition import hier_model_for_generator
from base.custom_loss import bbox_loss, define_bbox_loss, define_circle_loss, define_quantile_loss, \
    define_single_quantile_loss, define_yolo_loss


class HierModel(DnnModel):

    def __init__(self, type="classification", summary: KFoldClassSummary=None,
                 data_provider: DataProviderBase=None,
                 params=None, output_dir=None, filename=None):
        super().__init__(type, summary, data_provider, params, output_dir, filename)

    def load_weights(self, file=None):
        if file is None:
            file = self.output_dir + self.filename + ".hdf5"
        self.classifier.load_weights(file)

    def setup_params(self):
        params = self.params

        # update parameters with standard values for classification model
        params.update({
            'shape': 'brick',
            'weight_regulizer': None,
            'kernel_initializer': 'he_normal',
            'optimizer': Adam,
            'activation': relu,
            'last_activation': softmax})

    def setup_model(self, setup_params=False):
        # use instance variables if no optional variables are supplied
        output_dir = self.output_dir
        dp = self.data_provider
        params = self.params

        if setup_params:
            self.setup_params()

        x_cols, y_cols = dp.get_data_dims("regression")  # was regression

        # obtain Keras classifier (Sequential) for specified parameters
        if self.type == "HIER_GRID":
            start_gs = math.ceil(
                    dp.floorplan_height / dp.grid_size) * math.ceil(
                    dp.floorplan_width / dp.grid_size)
            y_cols = [start_gs, 9]

        if "HIER_GRID" in self.type:
            classifier_template = hier_model_for_generator(metrics=[])
            self.classifier = classifier_template(X_cols=x_cols, Y_cols=y_cols, params=params)

        self.type = "regression"

    def evaluate_model(self, test_bs):
        data_provider = self.data_provider
        summary = self.summary

        x_test, y_test = data_provider.get_test_data(area_labels=False,
                                                     labels=data_provider.grid_labels)

        y_pred_list = []
        for idx in range(100):
            y_pred = self.classifier.predict(x={'AP': x_test}, batch_size=32)
            y_pred_list.append(y_pred)

        _, y_true_labels = data_provider.get_test_data(area_labels=False)
        summary.y_pred.append(y_pred_list)
        summary.y_true_labels.append(y_true_labels)

        summary.num_folds += 1

    def _train_model(self, save_weights_only=False, evaluate=True):
        """
        Trains a DNN model with the specified parameters using Keras
        fit_generator function. Model is evaluated on separate test data
        """
        output_dir = self.output_dir
        data_provider = self.data_provider
        params = self.params

        # self.setup_model()

        # calculate batch sizes (might be smaller than specified batch size if
        # not enough data supplied)
        num_train, num_val, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")

        train_bs = min(num_train, params['batch_size'])
        val_bs = min(num_val, train_bs)
        test_bs = min(num_test, train_bs)

        # calculate the steps per epoch (used by generator) to determine
        # after how many steps a new epoch starts (model has seen all data)
        val_steps_per_epoch = math.ceil(num_val / val_bs)
        num_augs = 0
        if 'augmentation' in params and params['augmentation'] is not None:
            num_augs = params['augmentation']

        train_steps_per_epoch = math.ceil(num_train * (num_augs + 1) / train_bs)

        # setup callbacks
        checkpoint_file_name = output_dir + "{}.hdf5".format(self.filename)

        # save best performing model parameters
        checkpoint = ModelCheckpoint(checkpoint_file_name, verbose=0,
                                     monitor='val_loss',
                                     save_best_only=True, mode='auto',
                                     save_weights_only=save_weights_only)

        earlyStopping = EarlyStopping(monitor='val_loss',
                                      patience=500,
                                      verbose=0,
                                      mode='auto')

        x_train, y_train = data_provider.get_train_data(area_labels=False, labels=data_provider.grid_labels)
        x_val, y_val = data_provider.get_val_data(area_labels=False, labels=data_provider.grid_labels)

        self.classifier.fit(x={'AP': x_train}, y={'first': y_train[:, :, 0], "second": y_train[:, :, 1]},
                            validation_data=[{'AP': x_val}, {'first': y_val[:, :, 0], 'second': y_val[:, :, 1]}],
                            batch_size=32, epochs=200, verbose=0, callbacks=[checkpoint, earlyStopping])

        # evaluate model and store results in summary file
        if evaluate:
            self.evaluate_model(test_bs)


def get_accuracy_hier_layerwise(y_true, y_pred):
    num_layers = len(y_pred)

    accs = []

    eq_mask = np.full((len(y_true)), True)

    for l_idx in range(num_layers):
        l_true = y_true[:, 2, l_idx]
        l_pred = np.argmax(y_pred[l_idx], axis=1)

        eq = l_true == l_pred

        eq_mask = np.logical_and(eq, eq_mask)

        acc = len(np.where(eq_mask)[0]) / len(l_true)

        accs.append(acc)

    return accs
