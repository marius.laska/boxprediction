from il_pipeline.models.layer_generator import hidden_layers
from il_pipeline.models.lr_normalizer import lr_normalizer
from tensorflow import keras
from tensorflow.keras import regularizers
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.models import Sequential
from base.custom_loss import hier_loss


def hier_model_for_generator(metrics):

    def define_hier_model_for_generator(X_cols, Y_cols, params):

        ap_input = keras.Input(shape=(X_cols, ), name="AP")

        first = keras.layers.Dense(params['first_neuron'], input_dim=X_cols,
                        activation=params['activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])
                        )(ap_input)

        first = keras.layers.Dropout(0.5)(first, training=True)

        first_output = keras.layers.Dense(Y_cols[0], name="first", activation=params['last_activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty']))(first)

        sec_input = keras.layers.concatenate([ap_input, first_output])

        second = keras.layers.Dense(params['first_neuron'],
                        activation=params['activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])
                        )(sec_input)

        second = keras.layers.Dropout(0.5)(second, training=True)

        second_output = keras.layers.Dense(Y_cols[1], name="second",
                                          activation=params['last_activation'],
                                          kernel_regularizer=regularizers.l2(
                                              params['regularization_penalty']))(second)

        model = keras.Model(inputs=[ap_input], outputs=[first_output, second_output])

        # # compile the model
        model.compile(loss={"first": hier_loss, "second": hier_loss},
                      loss_weights=[1, 0.5],
                      metrics=[*metrics],
                      optimizer=params['optimizer'](
                          lr=lr_normalizer(params['lr'], params['optimizer'])))

        return model

    return define_hier_model_for_generator


if __name__ == "__main__":
    test = hier_model_for_generator(None)
    params = {"first_neuron": 512,
              "activation": "relu",
              "regularization_penalty": 0.0,
              "last_activation": "linear"}

    test(5, 2, params)