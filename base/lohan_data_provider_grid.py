from data.lohan.lohan_data_provider import LohanDSprovider
from base.data_provider_base_grid import DataProviderGridBase


class LohanDataProviderGrid(LohanDSprovider, DataProviderGridBase):

    @classmethod
    def setup_data_provider(cls, data_params, pre_params):
        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        if 'num_epochs' in pre_params:
            num_epochs = pre_params['num_epochs']
            if 'epoch' in pre_params:
                epoch = pre_params['epoch']
            else:
                epoch = 1
        else:
            num_epochs = 1
            epoch = 1

        if 'floor' not in pre_params:
            #log.error("'floor' parameter must be supplied. Set to 0...")
            floor = 0
        else:
            floor = pre_params['floor']

        dp = LohanDataProviderGrid(random_state=1,
                             split_ratio=split_ratio,
                             num_epochs=num_epochs)

        dp.load_dataset(floor=floor, epoch=epoch)
        dp.replace_missing_values()

        if 'standardize' not in pre_params or pre_params['standardize']:
            dp.standardize_data()

        # set up all filters that are sequentially applied to generate
        # the desired subset of data
        data_filters = dp.setup_filters(pre_params)
        if len(data_filters) > 0:
            dp.convert_to_dataframe()
            dp.apply_filters(data_filters)
            dp.transform_into_tensor(missing_val=0)

        # remove temporary APs
        if "filter_tmp_AP" in pre_params and pre_params[
            "filter_tmp_AP"] is not None:
            dp.filter_temporary_access_points(
                temp_threshold=pre_params["filter_tmp_AP"])

        if dp.splits_indices is None:
            dp.generate_split_indices(pre_params)

        return dp

