from base.data_provider_base_grid import DataProviderGridBase
import numpy as np


class SimulationDataProvider(DataProviderGridBase):

    def __init__(self, random_state, split_ratio):
        super().__init__()
        self.random_state = random_state
        self.split_ratio = split_ratio
        np.random.seed(random_state)

    @classmethod
    def setup_data_provider(cls, data_params, pre_params):
        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        dp = SimulationDataProvider(
            random_state=1, split_ratio=split_ratio)

        dp.load_dataset(num_ap=80, ap_position="random", num_train=5000)#, train_positions="real-td-pos")
        #dp.replace_missing_values()

        if 'standardize' not in pre_params or pre_params['standardize']:
            dp.standardize_data()

        if dp.splits_indices is None:
            dp.generate_split_indices(pre_params)

        return dp

    def get_rssi_value(self, d, p_tx=20, l_0=40.22, gamma=1.64, l_c=53.73, k=2, l_w=4.51, l_noise=0.5):

        return p_tx - (l_0 + 10 * gamma * np.log10(d) + l_c + k * l_w) - np.random.exponential(l_noise, len(d))

    def load_dataset(self, dims=(180, 60), ap_position="corner", num_ap=None, num_train=5000, train_positions="random", test_positions="random"):

        self.floorplan_width = 200
        self.floorplan_height = 80

        rg = np.random.default_rng(seed=self.random_state)

        # generate ap locations
        if ap_position == "corner":
            ap_pos = np.array([[0, 0], [dims[0], 0], [0, dims[1]], [dims[0], dims[1]]])

        elif ap_position == "random":
            ap_pos = np.zeros((num_ap, 2))
            ap_pos[:, 0] = rg.uniform(0, dims[0], num_ap)
            ap_pos[:, 1] = rg.uniform(0, dims[1], num_ap)

        # generate training locations
        y_train = np.zeros((num_train, 2))
        if train_positions == "random":
            y_train[:, 0] = rg.uniform(0, dims[0], num_train)
            y_train[:, 1] = rg.uniform(0, dims[1], num_train)
        elif train_positions == "real-td-pos":
            from il_pipeline.utility.storable import Storable
            from il_pipeline.pipeline import Pipeline
            real_p: Pipeline = Storable.load("/Users/mariuslaska/PycharmProjects/boxprediction/evaluation/lohan/evaluation/grid_new_loss/output/BBOX_DEEP")
            y_train = real_p.data_provider.labels
            num_train = len(y_train)

        # generate RSS data
        data = np.zeros((num_train, len(ap_pos)))

        for ap_idx, ap in enumerate(ap_pos):
            dist_to_ap = np.linalg.norm(y_train - ap, axis=1)
            data[:, ap_idx] = self.get_rssi_value(dist_to_ap)

        self.data_tensor = data
        self.labels = y_train


if __name__ == "__main__":

    dp = SimulationDataProvider.setup_data_provider({"split_ratio": [0.7, 0.1, 0.2]}, {})

    print(dp.labels)
