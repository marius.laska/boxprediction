from data.data_provider_base import DataProviderBase
import pandas as pd
import numpy as np
import yaml
from il_pipeline.utility.config_reader import ConfigReader


class UJIndoorLocProvider(DataProviderBase):

    def __init__(self):
        super().__init__()
        self.num_train = 0
        self.num_test = 0

    def generate_split_indices(self, pre_params=None):
        self.splits_indices = [(np.arange(self.num_train), np.arange(start=self.num_train, stop=self.num_train + self.num_test))]
        self.num_splits = 1

    def get_dataset(self, building_id=1, floor_id=1):
        """
        Reads the dataset from the SQL database and sets up the initial
        pandas dataframe structures
        :return:
        """

        train = pd.read_csv("UJIndoorLoc/trainingData.csv")
        train = train[train.BUILDINGID.eq(building_id)]
        train = train[train.FLOOR.eq(floor_id)]

        test = pd.read_csv("UJIndoorLoc/validationData.csv")
        test = test[test.BUILDINGID.eq(building_id)]
        test = test[test.FLOOR.eq(floor_id)]

        self.num_train = train.shape[0]
        self.num_test = test.shape[0]

        data = pd.concat((train, test), axis=0)

        self.uids = data['PHONEID'].to_numpy()
        self.labels = data[['LONGITUDE', 'LATITUDE']].to_numpy()
        self.data_tensor = data.iloc[:, :520].to_numpy()
        self.timestamps = data['TIMESTAMP'].to_numpy()

        self.floorplan_width = np.max(self.labels[:, 0]) - np.min(self.labels[:, 0]) # 200.0 #
        self.floorplan_height = np.max(self.labels[:, 1]) - np.min(self.labels[:, 1]) # 180.0#np.max(self.labels[:, 1]) - np.min(self.labels[:, 1])

    def set_missing_AP_val(self, before=100, after=-110.0):
        mask = np.where(self.data_tensor == before)
        self.data_tensor[mask] = after

    def convert_labels_to_meter(self):
        min_vals = np.min(self.labels, axis=0)

        self.labels -= min_vals

    @classmethod
    def setup_data_provider(self, data_params, pre_params=None):
        """
        Factory method for data provider that uses filters to obtain subset
        of data.
        1) The labels are transformed from grid dimension with origin at upper left
        (android drawing) to lower left with meter dimension.
        2) Filters are applied to obtain base set of data
        3) Data is transformed into tensor and standardized (0-std columns (APs)
             are discarded)
        4) Data is split into several folds, such that the specific data
            (train/val/test) can be accessed for the current split index of the
            class
        :param data_params: Data parameters dict that holds connection and map_id
        :param filters: The filters parsed from yaml (via ConfigReader)
        :return:
        """

        floor_id = data_params['floor']
        building_id = data_params['building']

        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        dataset_provider = UJIndoorLocProvider()

        dataset_provider.split_ratio = split_ratio
        # read SQL dataset from Backend

        dataset_provider.get_dataset(building_id, floor_id)

        dataset_provider.convert_labels_to_meter()
        dataset_provider.set_missing_AP_val()

        # if we replace missing values by area mean, we cannot standardize yet
        if 'missing_values' not in pre_params and (
                'standardize' not in pre_params or pre_params['standardize']):
            dataset_provider.standardize_data()

        # remove temporary APs
        if "filter_tmp_AP" in pre_params and pre_params[
            "filter_tmp_AP"] is not None:
            dataset_provider.filter_temporary_access_points(
                temp_threshold=pre_params["filter_tmp_AP"])

        if dataset_provider.splits_indices is None:
            dataset_provider.generate_split_indices(pre_params)

        return dataset_provider


if __name__ == "__main__":

    conf = ConfigReader("ujiindoor_data.yml")

    dp = UJIndoorLocProvider.setup_data_provider(conf.data_params, conf.pipelines[0]['preprocessing'])