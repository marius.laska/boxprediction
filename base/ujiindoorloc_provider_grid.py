from base.data_provider_base_grid import DataProviderGridBase
from base.ujiindoorloc_provider import UJIndoorLocProvider


class UJIndoorLocProviderGrid(UJIndoorLocProvider, DataProviderGridBase):

    @classmethod
    def setup_data_provider(self, data_params, pre_params=None):
        """
        Factory method for data provider that uses filters to obtain subset
        of data.
        1) The labels are transformed from grid dimension with origin at upper left
        (android drawing) to lower left with meter dimension.
        2) Filters are applied to obtain base set of data
        3) Data is transformed into tensor and standardized (0-std columns (APs)
             are discarded)
        4) Data is split into several folds, such that the specific data
            (train/val/test) can be accessed for the current split index of the
            class
        :param data_params: Data parameters dict that holds connection and map_id
        :param filters: The filters parsed from yaml (via ConfigReader)
        :return:
        """

        floor_id = data_params['floor']
        building_id = data_params['building']

        split_ratio = None
        if 'split_ratio' in data_params:
            split_tuple = data_params['split_ratio']
            split_ratio = {'train': split_tuple[0],
                           'val': split_tuple[1],
                           'test': split_tuple[2]}

        dataset_provider = UJIndoorLocProviderGrid()

        dataset_provider.split_ratio = split_ratio
        # read SQL dataset from Backend

        dataset_provider.get_dataset(building_id, floor_id)

        dataset_provider.convert_labels_to_meter()
        dataset_provider.set_missing_AP_val()

        # if we replace missing values by area mean, we cannot standardize yet
        if 'missing_values' not in pre_params and (
                'standardize' not in pre_params or pre_params['standardize']):
            dataset_provider.standardize_data()

        # remove temporary APs
        if "filter_tmp_AP" in pre_params and pre_params[
            "filter_tmp_AP"] is not None:
            dataset_provider.filter_temporary_access_points(
                temp_threshold=pre_params["filter_tmp_AP"])

        if dataset_provider.splits_indices is None:
            dataset_provider.generate_split_indices(pre_params)

        return dataset_provider