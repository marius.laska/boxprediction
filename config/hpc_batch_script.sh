#!/usr/local_rwth/bin/zsh

### name the job
#SBATCH --job-name=LOC_GPU

#SBATCH --output=batch/output/uji_GPU.%J.txt

#SBATCH --time=0-01:00:00

#SBATCH --mem-per-cpu=8G
#SBATCH --gres=gpu:1

### change directory
cd $HOME/boxprediction/

### Load modules for tensorflow GPU
module load cuda/100
module load cudnn/7.4

# set environment variables such that rtree can locate libspatialindex
export SPATIALINDEX_LIBRARY=~/lib/libspatialindex.so
export SPATIALINDEX_C_LIBRARY=~/lib/libspatialindex_c.so

### activate python virtual environment
source ../test/venv/bin/activate

### execute python script
python main.py -c config/GPU/uji/cnn_config.yml