import argparse
import copy
import logging

from debug_tools.logger import getLogger
from il_pipeline.utility.config_reader import ConfigReader
from il_pipeline.utility.storable import Storable
from ldce.base import ClusterBase

from base.ujiindoorloc_fullgrid_provider import test_encoding_decoding

log = getLogger(level=logging.INFO)
from base.bbox_pipeline import BboxPipeline
import numpy as np
import pandas as pd

from numpy.random import seed
from tensorflow import set_random_seed
import random

import tensorflow as tf
import os
import tensorflow.keras.backend as K
os.environ['PYTHONHASHSEED'] = '0'


def set_seeds(seed_val=1):
    # set random seeds
    seed(seed_val)
    random.seed(seed_val)

    config = tf.ConfigProto(intra_op_parallelism_threads=1,
                            inter_op_parallelism_threads=1)
    tf.set_random_seed(seed_val)
    sess = tf.Session(graph=tf.get_default_graph(), config=config)

    K.set_session(sess)


def execute(conf_file):
    area_classification = True
    logging.basicConfig(level="INFO")

    conf = ConfigReader(conf_file)

    conf.setup_directories()

    # only PD
    conf.download_floor_plan()

    # obtain pipeline config variations
    pipeline_configs = conf.get_pipeline_variation(conf.pipelines)

    # sequentially execute all training pipelines
    for p_idx, pipeline_params in enumerate(pipeline_configs):

        # set random seeds
        # if 'ns' in pipeline_params:
        #     seed(pipeline_params['ns'])
        #     random.seed(pipeline_params['ns'])
        # if 'ts' in pipeline_params:
        #     config = tf.ConfigProto(intra_op_parallelism_threads=1,
        #                             inter_op_parallelism_threads=1)
        #     tf.set_random_seed(pipeline_params['ts'])
        #     sess = tf.Session(graph=tf.get_default_graph(), config=config)
        #
        #     K.set_session(sess)

        num_iterations = conf.get_params('repetitions', pipeline_params)
        log.info('Train and evaluate il_pipeline "{}"... ({}/{}))'.format(
            pipeline_params['name'], p_idx + 1, len(pipeline_configs)))
        log.info('Repeat {} time(s) and build average...'.format(num_iterations))

        # read pipeline parameters
        pre_params = conf.get_params('preprocessing', pipeline_params, merge_level=1)
        model_params = conf.get_params('model_params', pipeline_params, merge_level=1)
        fp_params = conf.get_params('floor_plan', pipeline_params, merge_level=2)
        assign_closest = conf.get_params('assign_closest', pre_params, 'preprocessing')
        area_mode = conf.get_params('area_assignment', pre_params, 'preprocessing')
        data_params = conf.get_params('data', pipeline_params, global_params=conf.cfg, merge_level=1)

        # get data provider
        base_data_provider = BboxPipeline.get_data_provider(data_params, pre_params)

        # for storing pipeline iteration names
        p_names = []

        for run in range(num_iterations):

            # set seeds
            set_seeds(run+1)

            pipe_params = copy.deepcopy(pipeline_params)

            p_name = pipe_params['name']# + "[{}]".format(run + 1)
            pipe_params['name'] = p_name
            p_names.append(p_name)

            data_provider = copy.deepcopy(base_data_provider)

            clusterer: ClusterBase = BboxPipeline.get_floor_plan_segmentation(
                data_provider, fp_params)

            # either use segmented floor plan or apply regression
            if fp_params['type'] == 'segmentation':

                area_labels, _, coverage, mask = clusterer.get_cluster_labels_for_areas(
                    data_provider.labels, area_mode=area_mode,
                    assign_closest=assign_closest)

                data_provider.set_area_labels(area_labels,
                                              delete_uncovered=True,
                                              pre_params=pre_params)

                data_provider.remove_APs_with_low_correlation_to_areas(
                    pre_params)

                if data_provider.get_data_dims(
                        model_type="classification")[1] < 2:
                    log.info("Skipping segmentation (only single class)")
                    continue

            elif fp_params['type'] == 'regression':
                pass
                #data_provider.generate_random_splits_from_clusters(
                #   clusterer.base_cluster_mappings)

            elif fp_params['type'] == 'floor_classification':
                data_provider.set_area_labels(data_provider.labels,
                                             delete_uncovered=True,
                                             pre_params=pre_params)

            # if model is meant for upload use all data for training
            if 'upload_split' in pre_params and pre_params['upload_split']:
                data_provider.reset_split_indices_into_single_split_no_test_data()

            # compute grid encoding
            if "GRID_2L" in model_params['type']:
                if "grid_size" in pre_params:
                    data_provider.transform_to_2dim_2layer_grid_encoding(
                        grid_size=pre_params['grid_size'])
            elif "GRID_3L" in model_params['type']:
                if "grid_size" in pre_params:
                    data_provider.transform_to_2dim_3layer_grid_encoding(
                        grid_size=pre_params['grid_size'])
            elif ("GRID_OVERLAP" in model_params['type'] or "GRID_ROT" in model_params['type']) \
                    and data_params['provider'] != "UJIndoorLocFullProvider":
                if "grid_size" in pre_params:
                    data_provider.transform_to_2dim_overlapping_grid_encoding(
                        grid_size=pre_params['grid_size'])

                    # walls_h = np.load("/Users/mariuslaska/PycharmProjects/boxprediction/analysis/hor_walls.npy")
                    # walls_v = np.load("/Users/mariuslaska/PycharmProjects/boxprediction/analysis/ver_walls.npy")
                    #
                    # data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
                    #     grid_size=pre_params['grid_size'], horizontal=True,
                    #     angle_only=True, overlap_strategy='keep',
                    #     walls=walls_h)
                    #
                    # data_provider.encode_walls_to_2dim_overlapping_grid_encoding(
                    #     grid_size=pre_params['grid_size'], horizontal=False,
                    #     angle_only=True, overlap_strategy='keep',
                    #     walls=walls_v)

            elif "GRID" in model_params['type'] and data_params['provider'] != "UJIndoorLocFullProvider":
                if "grid_size" in pre_params:
                    data_provider.transform_to_2dim_grid_encoding(
                        grid_size=pre_params['grid_size'])
                else:
                    data_provider.transform_to_grid_encoding()

            #x, y = data_provider.get_augmented_train_data()

            #x2, y2 = data_provider.get_train_data(labels=data_provider.grid_labels)
            # set seed value for dropout etc
            model_params['seed'] = run + 1

            pipeline = BboxPipeline(data_provider,
                                clusterer,
                                conf,
                                model_params,
                                pipe_params['name'] + "_{}".format(run+1))

            if model_params is not None:
                pipeline.k_fold_validation()

            pipeline.store()

        #if model_params is not None:
        #    pipe_files = [conf.output_dir + p_name for p_name in p_names]
        #    BboxPipeline.merge_summaries(pipe_files, pipeline_params['name'])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", help="Path to yml config file")

    args = parser.parse_args()

    execute(args.c)


def center_diff(y_true, y_pred):
    return np.mean(np.linalg.norm(y_pred[:, :2] - y_true, axis=1))


def calc_acc_elipse(y_true, y_pred):
    s = np.square(y_pred[:, :2] - y_true)
    l = s[:, 0] / np.square(y_pred[:, 2] / 2.0)
    r = s[:, 1] / np.square(y_pred[:, 3] / 2.0)

    diff = l + r
    inside = len(np.where(diff <= 1)[0])

    return inside/len(y_true)


def calc_acc_c(y_true, y_pred):
    max_x = y_pred[:, 0] + y_pred[:, 2] / 2
    min_x = y_pred[:, 0] - y_pred[:, 2] / 2
    max_y = y_pred[:, 1] + y_pred[:, 3] / 2
    min_y = y_pred[:, 1] - y_pred[:, 3] / 2

    x_max = y_true[:, 0] <= max_x
    x_min = y_true[:, 0] >= min_x
    x_both = np.logical_and(x_max, x_min)

    y_max = y_true[:, 1] <= max_y
    y_min = y_true[:, 1] >= min_y
    y_both = np.logical_and(y_min, y_max)

    correct = np.where(np.logical_and(x_both, y_both))[0]
    num_correct = len(correct)

    wrong_idx = np.where(~np.logical_and(x_both, y_both))[0]
    correct_idx = np.where(np.logical_and(x_both, y_both))[0]
    #print("ACC: {}".format(num_correct / len(y_true)))

    return num_correct / len(y_true), wrong_idx, correct_idx


def calc_acc(y_true, y_pred):
    max_x = np.max(y_pred[:, [0,2]], axis=1)
    min_x = np.min(y_pred[:, [0,2]], axis=1)
    max_y = np.max(y_pred[:, [1, 3]], axis=1)
    min_y = np.min(y_pred[:, [1, 3]], axis=1)

    x_max = y_true[:, 0] <= max_x
    x_min = y_true[:, 0] >= min_x
    x_both = np.logical_and(x_max, x_min)

    y_max = y_true[:, 1] <= max_y
    y_min = y_true[:, 1] >= min_y
    y_both = np.logical_and(y_min, y_max)

    num_correct = len(np.where(np.logical_and(x_both, y_both))[0])

    wrong_idx = np.where(~np.logical_and(x_both, y_both))[0]
    print("ACC: {}".format(num_correct/len(y_true)))


def calc_vals(file, num_rep=1):
    bld_acc_sum = 0
    floor_acc_sum = 0
    pos_error_sum = 0

    for idx in range(num_rep):

        p: BboxPipeline = Storable.load(file.format(idx + 1))
        p.data_provider.decode_pipe_grid_labels(p)

        y_pred = np.concatenate(p.summary.y_pred, axis=0)
        y_true = np.concatenate(p.summary.y_true, axis=0)
        bld_pred = y_pred[:, 4]
        bld_true = y_true[:, 2]
        bld_acc = np.where(bld_pred == bld_true)[0]

        floor_pred = y_pred[:, 5]
        floor_true = y_true[:, 3]
        floor_acc = np.where(floor_pred == floor_true)[0]

        pos_error = np.mean(np.linalg.norm(y_pred[:, :2] - y_true[:, :2], axis=1))
        # pos error correct
        mask = np.where(np.logical_and(bld_pred == bld_true, floor_pred == floor_true))[0]
        pos_error_correct = np.mean(np.linalg.norm(y_pred[mask, :2] - y_true[mask, :2], axis=1))

        bld_acc_sum += len(bld_acc) / len(bld_true)
        floor_acc_sum += len(floor_acc) / len(floor_true)
        pos_error_sum += pos_error

        # print("bld_acc: {}".format(len(bld_acc)/len(bld_true)))
        # print("floor_acc: {}".format(len(floor_acc) / len(floor_true)))
        #
        # print("MSE: {}".format(pos_error))
        # print("MSE (only correct): {}".format(pos_error_correct))
        # print("--------------")

    return bld_acc_sum / num_rep, floor_acc_sum / num_rep, pos_error_sum / num_rep


def main_uji():

    pd_dict = {"HL": [], "HU": [], "Dropout": [], "bld": [], "floor": [], "mse": []}

    folder = "evaluation/uji/gpu/full_grid/hyper/output/"
    file_name = "DNN-DLB_first_neuron_{}_hidden_layers_{}_dropout_{}"

    for hl in [1,2,3]:
        for hu in [128]:
            for dropout in [0.0, 0.1,0.2,0.3,0.4,0.5,0.6,0.7]:
                bld_acc, fl_acc, mse = calc_vals(folder + file_name.format(hl, hu, dropout) + "_{}", num_rep=5)

                pd_dict["HL"].append(hl)
                pd_dict["HU"].append(hu)
                pd_dict["Dropout"].append(dropout)
                pd_dict["bld"].append(bld_acc)
                pd_dict["floor"].append(fl_acc)
                pd_dict["mse"].append(mse)

    df = pd.DataFrame(pd_dict)
    print(df)


if __name__ == "__main__":
    main_uji()
    # main()
