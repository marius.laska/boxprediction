import copy
import logging
import numpy as np
import argparse

from debug_tools.logger import getLogger
from il_pipeline.models.dnn_model import DnnModel
from il_pipeline.utility.config_reader import ConfigReader
from ldce.base import ClusterBase
from ldce.plotting.floorplan_plot import FloorplanPlot

from base.BboxModel import BboxModel
from base.bbox_model_definition import bbox_model_for_talos, reg_model_for_talos

log = getLogger(level=logging.INFO)
from base.bbox_pipeline import BboxPipeline

import talos as ta

def execute(conf_file, exp_number, downsample):
    area_classification = True
    logging.basicConfig(level="INFO")

    conf = ConfigReader(conf_file)

    conf.setup_directories()

    # only PD
    conf.download_floor_plan()

    # sequentially execute all training pipelines
    for p_idx, pipeline_params in enumerate(conf.pipelines):
        num_iterations = conf.get_params('repetitions', pipeline_params)
        log.info('Train and evaluate il_pipeline "{}"... ({}/{}))'.format(
            pipeline_params['name'], p_idx + 1, len(conf.pipelines)))
        log.info('Repeat {} time(s) and build average...'.format(num_iterations))

        # read pipeline parameters
        pre_params = conf.get_params('preprocessing', pipeline_params, merge_level=1)
        model_params = conf.get_params('model_params', pipeline_params, merge_level=1)
        fp_params = conf.get_params('floor_plan', pipeline_params, merge_level=1)
        assign_closest = conf.get_params('assign_closest', pre_params, 'preprocessing')
        area_mode = conf.get_params('area_assignment', pre_params, 'preprocessing')

        # get data provider
        base_data_provider = BboxPipeline.get_data_provider(conf.data_params, pre_params)

        # for storing pipeline iteration names
        p_names = []

        for run in range(num_iterations):
            pipe_params = copy.deepcopy(pipeline_params)

            p_name = pipe_params['name']# + "[{}]".format(run + 1)
            pipe_params['name'] = p_name
            p_names.append(p_name)

            data_provider = copy.deepcopy(base_data_provider)

            clusterer: ClusterBase = BboxPipeline.get_floor_plan_segmentation(
                data_provider, fp_params)

            # either use segmented floor plan or apply regression
            if fp_params['type'] == 'segmentation':

                area_labels, _, coverage, mask = clusterer.get_cluster_labels_for_areas(
                    data_provider.labels, area_mode=area_mode,
                    assign_closest=assign_closest)

                data_provider.set_area_labels(area_labels,
                                              delete_uncovered=True,
                                              pre_params=pre_params)

                data_provider.remove_APs_with_low_correlation_to_areas(
                    pre_params)

                if data_provider.get_data_dims(
                        model_type="classification")[1] < 2:
                    log.info("Skipping segmentation (only single class)")
                    continue

            elif fp_params['type'] == 'regression':
                data_provider.generate_random_splits_from_clusters(clusterer.base_cluster_mappings)

            elif fp_params['type'] == 'floor_classification':
                data_provider.area_labels = data_provider.labels

            # compute grid encoding
            if "GRID" in model_params['type']:
                data_provider.transform_to_grid_encoding()
            #data_provider.transform_to_2dim_grid_encoding(grid_size=20)

            pipeline = BboxPipeline(data_provider,
                                clusterer,
                                conf,
                                model_params,
                                pipe_params['name'])

            if model_params is not None:
                train_model(copy.deepcopy(model_params), pipeline, exp_number, downsample)


def train_model(params, pipe: BboxPipeline, exp_number, downsample):

    dp = pipe.data_provider

    sup_labels = None
    if "GRID" in params['type']:
        sup_labels = dp.grid_labels

    x_train, y_train = dp.get_train_data(labels=sup_labels, split_idx=0, area_labels=False)
    x_test, y_test = dp.get_test_data(labels=sup_labels, split_idx=0, area_labels=False)
    x_val, y_val = pipe.data_provider.get_val_data(labels=sup_labels, split_idx=0, area_labels=False)

    x_train_val = np.concatenate((x_train, x_val), axis=0)
    y_train_val = np.concatenate((y_train, y_val), axis=0)

    m_type = params['type']

    model_func = None

    if "BBOX" in m_type:
        model = BboxModel(params['type'], pipe.summary, pipe.data_provider, params,
                          pipe.config.output_dir, pipe.filename)
        model.setup_params()
        model.type = m_type

        m_params = model.params
        del m_params['loss']
        del m_params['type']
        del m_params['augmentation']

        model_func = bbox_model_for_talos

    elif "DNN" in m_type:
        m_type = params['pred'] if 'pred' in params else "classification"
        model = DnnModel(m_type, pipe.summary, pipe.data_provider, params,
                         pipe.config.output_dir, pipe.filename)
        model.setup_params()
        m_params = model.params
        del m_params['loss']
        del m_params['type']
        del m_params['pred']
        del m_params['augmentation']

        model_func = reg_model_for_talos

    # put every element in list
    m_params = {k: [v] if type(v) not in [list, tuple] else v for (k, v) in m_params.items()}

    t = ta.Scan(x=x_train_val,
                y=y_train_val,
                x_val=x_test,
                y_val=y_test,
                model=model_func,
                grid_downsample=downsample,
                params=m_params,
                dataset_name=m_type,
                experiment_no=exp_number)


def report():
    r = ta.Reporting("/Users/mariuslaska/Dropbox/PhD/BboxPaper/files/regression_1.csv")
    data = r.data

    test = data.sort_values(by=['val_loss'], ascending=True).head(5)

    r.plot_corr(metric="val_loss")
    print(test)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", help="Path to yml config file")
    parser.add_argument("-n", help="Exp number")
    parser.add_argument("-d", help="Downsample ratio")

    args = parser.parse_args()

    downsample = None
    if args.d:
        downsample = float(args.d)

    execute(args.c, exp_number=args.n, downsample=downsample)


if __name__ == "__main__":
    report()
    #main()