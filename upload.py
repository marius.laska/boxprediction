from il_pipeline.upload.upload_new import ModelUploader

conf_file = "config/upload/upload_gia_floors.yml"

up = ModelUploader(conf_file)

up.create_backend_manager()
up.upload_models()